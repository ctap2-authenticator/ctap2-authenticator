# Tools

This directory contains a bunch of tools useful
for developing, testing and deploying Ctap2 authencticators.


## DudeCT

This tool implements a fuzzing based test tool
which allows to check the authenticators cryptography for
timing leaks.

The work is based on this [paper](https://eprint.iacr.org/2016/1123.pdf).

In order to install and run the tool, the following steps are necessary
(tested on Ubuntu 18.04). From the base directory of this project run:

1. Create a virtual environment
        
        python3 -m venv venv
2. Switch to the environment and pull the dependencies:

        source venv/bin/activate
        pip install -r tools/requirements.txt

3. Set the `PYTHONPATH` accordingly:

        export PYTHONPATH=$(pwd)

4. Run the program

        python3 ./tools/dudect/main.py [cmd]


