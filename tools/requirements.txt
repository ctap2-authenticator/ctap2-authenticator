fido2 >= 0.8.0
solo-python >=0.0.12
tabulate >=0.8.7
matplotlib >= 3.2.1
hexdump >= 3.3
scipy >= 1.5.2