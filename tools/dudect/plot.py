import matplotlib.pyplot as plt

from tools.dudect.ttest import TStats


def plot_tstat(db, name, n_measurements=500, plot_until=None):
    tests = db.load_test(name)

    # We need to load the tests once before in order to get the percentiles right
    exec_times = []
    for test in tests:
        exec_times.append(test["output"])

    tests = db.load_test(name)
    stats = TStats(exec_times)

    # Prepare the Plot data array in case they are needed
    first_order_plot = []
    second_order_plot = []

    # Rebuild t-tests
    for test_num, test in enumerate(tests, 0):
        left = True if test["class"] == "left" else False
        stats.update_ttests(left, test["output"])

        if test_num % (2 * n_measurements) == 0:
            print("Rebuilt " + str(test_num) + " tests")

            first_order_plot.append(stats.first_order.test())
            second_order_plot.append(stats.second_order.test())

        if plot_until and test_num > plot_until:
            break

    plt.rc('font', size=24)
    fig = plt.figure()
    p = fig.add_subplot()

    p.set_xlabel("Number of test sets [n]")
    p.set_ylabel("t-statistic(n)")

    p.plot(first_order_plot, '-', label="t-statistic", color="#647498")
    p.plot(second_order_plot, '--', label="second order t-statistic", color="#ce782a")

    p.legend()


def plot_cdf(db, name, plot_until=None):
    tests = db.load_test(name)

    left = []
    right = []

    for test_num, test in enumerate(tests, 0):
        exec_time = test["output"]

        if test["class"] == "left":
            left.append(exec_time)
        elif test["class"] == "right":
            right.append(exec_time)

        if plot_until and test_num > plot_until:
            break

    left = to_cdf(left)
    right = to_cdf(right)

    # Add an point do the beginning and end respectively
    start = min(left[0][0], right[0][0])
    left.insert(0, (start, 0.0))
    right.insert(0, (start, 0.0))

    end = max(left[-1][0], right[-1][0])
    left.insert(-1, (end, 1.0))
    right.insert(-1, (end, 1.0))

    print(str(start))

    plt.rc('font', size=24)
    fig = plt.figure()
    p = fig.add_subplot()

    p.set_xlabel("Cycles per signature [i]")
    p.set_ylabel("empirical CDF(i)")

    # TODO: Make the names settable in accordance to the generator
    p.plot(*zip(*left), '-', label="varkey", color="#647498")
    p.plot(*zip(*right), '-', label="fixkey", color="#ce782a")

    p.legend()


def to_cdf(list):
    list = sorted(list)
    current_val = list[0]
    current_count = 0
    result = []

    for val in list:
        current_count += 1
        if val != current_val:
            result.append((current_val, current_count))
            current_val = val

    normalized_result = []
    for val in result:
        normalized_val = float(val[1]) / current_count
        normalized_result.append((val[0], normalized_val))

    return normalized_result
