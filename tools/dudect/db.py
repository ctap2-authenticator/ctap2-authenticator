import os
import sqlite3
import hexdump
import scipy.stats

from tabulate import tabulate


class Database:

    def __init__(self, db_path):

        create = False
        if not os.path.isfile(db_path):
            create = True

        self.db = sqlite3.connect(db_path)
        if create:
            print("Database does not exist. Creating a new database")
            self.gen_schema()

    def gen_schema(self):
        c = self.db.cursor()

        c.execute('''
            CREATE TABLE test (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT UNIQUE NOT NULL,
                generator TEXT NOT NULL,
                measurements INTEGER NOT NULL,
                description TEXT
        )''')

        c.execute('''
            CREATE TABLE testcase (
                test_id INTEGER NOT NULL,
                run_id INTEGER NOT NULL,
                execution_time INT NOT NULL,
                class TEXT NOT NULL,
                input BLOB NOT NULL,
                output INTEGER NOT NULL,
                FOREIGN KEY(test_id) REFERENCES test(id)
        )''')
        self.db.commit()

    def new_test(self):

        try:
            name = input("Enter the name of the test: ")
            description = input("Enter a description of this test (optional): ")
            generator = input("Enter the name of the test case generator to be used: ")
            measurements = input("Enter the number of measurements per test case: ")

            self.db.execute('''
                INSERT INTO test (name, generator, measurements, description)
                VALUES (?, ?, ?, ?)
            ''',
                            (name, generator, int(measurements), description))
            self.db.commit()

        except sqlite3.IntegrityError as e:
            print("The data entered is invalid. Error: " + str(e))
        except ValueError as e:
            print("The data entered is malformed. Error: " + str(e))

    def get_tests(self):
        c = self.db.cursor()
        c.row_factory = dict_factory

        tests = c.execute('''
            SELECT id, name, description, generator, measurements, runs FROM test
            LEFT JOIN (
                SELECT test_id, COUNT(*) AS runs FROM testcase
                GROUP BY test_id
            )
            ON test.id = test_id;
            ''')

        return tests

    def list_tests(self):

        tests = self.get_tests().fetchall()

        # Get the t-value and p-value using Scipy
        for test in tests:
            testcases = self.load_test(test["name"]).fetchall()
            left = []
            right = []

            for testcase in testcases:
                if testcase["class"] == "left":
                    left.append(testcase["output"])
                else:
                    right.append(testcase["output"])

            (t, p) = scipy.stats.ttest_ind(left, right, equal_var=False)
            test["t"] = abs(t)
            test["p"] = p


        # I have no idea why the headers=["name1", "name2", .. ] thingy doesnt work. We just rename the keys here to
        # make the headers pretty
        for test in tests:
            test.pop("id")
            test["Name"] = test.pop("name")
            test["Description"] = test.pop("description")
            test["Generator"] = test.pop("generator")
            test["Number of measurements"] = test.pop("measurements")
            test["Number of runs"] = test.pop("runs")
            test["t-statistic"] = test.pop("t")
            test["p-value"] = test.pop("p")

        print(tabulate(tests, headers="keys"))

    def store_testcases(self, test_id, testcases):

        # Translate the class into a string
        runs = []
        for testcase in testcases:
            class_string = "left" if testcase[3] else "right"
            runs.append((testcase[0], testcase[1], testcase[2], class_string,
                         testcase[4], testcase[5]))

        self.db.executemany('''
            INSERT INTO testcase (test_id, run_id, execution_time, class, input, output)
            VALUES (?,?,?,?,?,?)
            ''', runs)

        self.db.commit()

    # Load the tests from the database.
    # If the plot variable is set to a path, the plots are generated in that path.
    def load_test(self, name):
        # There is lots of code duplication inside the Queries.
        # Any ideas to reduce number/complexity of queries welcome

        c = self.db.cursor()
        c.row_factory = dict_factory

        # Check that the test exists
        test = c.execute('''
            SELECT * FROM test
            WHERE test.name=?
        ''', (name,))
        test = test.fetchone()
        if test is None:
            print("The test " + name + " does not exist. Create one with new")
            exit(1)

        print(str(test))

        tests = c.execute('''
            SELECT "class", "output" FROM (
                SELECT * FROM testcase
                JOIN test ON test.id = testcase.test_id
            )
            AS tests 
            WHERE tests.name=?
            ORDER BY run_id
        ''', (name,))

        return tests

    def print_extremes(self, name):
        left_min = self.query_extreme(name, left=True, asc=True)
        print("Left class minimum with output: " + str(left_min[1]))
        hexdump.hexdump(left_min[0])

        left_max = self.query_extreme(name, left=True, asc=False)
        print("Left class maximum with output: " + str(left_max[1]))
        hexdump.hexdump(left_max[0])

        right_min = self.query_extreme(name, left=False, asc=True)
        print("Right class minimum with output: " + str(right_min[1]))
        hexdump.hexdump(right_min[0])

        right_max = self.query_extreme(name, left=False, asc=False)
        print("Right class maximum with output: " + str(right_max[1]))
        hexdump.hexdump(right_max[0])


    def query_extreme(self, name, left=True, asc=True):
        class_string = "left" if left else "right"
        asc_string = "ASC" if asc else "DESC"

        c = self.db.cursor()
        c.row_factory = dict_factory

        tests = c.execute('''
            SELECT input, output FROM (
                SELECT * FROM testcase
                JOIN test ON test.id = testcase.test_id
            )
            AS tests
            WHERE tests.name=? AND tests.class == ?
            ORDER BY output ''' + asc_string + '''
            LIMIT 1
        ''', (name, class_string,))

        tests = tests.fetchone()

        return tests["input"], tests["output"]


# Taken from
# https://stackoverflow.com/questions/3300464/how-can-i-get-dict-from-sqlite-query
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
