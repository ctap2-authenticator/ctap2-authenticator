import argparse

import matplotlib.pyplot as plt

from tools.dudect.db import Database
from tools.dudect.generators import RandomZeroFixedDataGenerator
from tools.dudect.plot import plot_tstat, plot_cdf
from tools.dudect.probe import DeviceProbe
from tools.dudect.testrun import TestRun

my_random_data = b'''}?l\x914\xc1\x0bAL\x1e\xf3DN[\xe4\x9e\xc8ji\x1e\x87\xb4\xc1\xc9\x99\x91Q?c]\xd4\x0047|T\xd4\xed
    \x8c\x8e\x99\xdb\xc3\xfa%\xc4\xbfb(T\x19\x0e&\xfb\x8c%\xba\xe0\xec \xb5\xca\xf07\x93$i\xa0\xb8'''


def parse_args():
    parser = argparse.ArgumentParser(description="Ctap2-authenticator timing leakage tester")
    parser.add_argument("-d", dest="db", metavar="DB_PATH", required=False,
                        help="Path of the Sqlite database to use. If it does not exist, a new database is created",
                        default="dude.db", action="store")

    subparsers = parser.add_subparsers(title="commands", dest="command")

    subparsers.add_parser("new", help="Starts a dialogue to create a new test")
    subparsers.add_parser("list", help="Lists all the tests recorded in this database")

    cont = subparsers.add_parser("continue", help="Continue a test")
    cont.add_argument("name", metavar="NAME", help="Name of the test to continue")
    cont.add_argument("-n", type=int, metavar="NUM_TESTS", required=False,
                      help="Only run tests until total number is smaller than n")

    plot = subparsers.add_parser("plot", help="Generate plots for a test")
    plot.add_argument("name", metavar="NAME", help="Name of the test to plot")
    plot.add_argument("-n", type=int, metavar="NUM_TESTS", required=False,
                      help="Only plot the first n tests")

    extremes = subparsers.add_parser("extremes", help="Query the extremes of the test")
    extremes.add_argument("name", metavar="NAME", help="Name of the test to query")

    return parser.parse_args()


if __name__ == "__main__":

    args = parse_args()
    print(str(args))

    if not args.command:
        "No command was specified"

    if args.command in ["merge", "delete"]:
        print(args.command + " is not implemented yet")
        exit(0)

    db = Database(args.db)

    if args.command == "new":
        db.new_test()
        print("New test generated. Now run with with continue")
    elif args.command == "list":
        db.list_tests()
    elif args.command == "plot":
        plot_cdf(db, args.name, plot_until=args.n)
        plot_tstat(db, args.name, plot_until=args.n)

        plt.show()
    elif args.command == "continue":
        test = None
        tests = db.get_tests().fetchall()

        for maybe_test in tests:
            if maybe_test["name"] == args.name:
                test = maybe_test
                break

        if test is None:
            print("Could not find the test. Print available tests with list")
            exit(0)

        generator = RandomZeroFixedDataGenerator(my_random_data)

        test = TestRun(test["name"], test["id"], db,
                       DeviceProbe(), generator,
                       n_runs=test["runs"],
                       n_measurements=test["measurements"],
                       max_runs=args.n)
        while True:
            test.step()
    elif args.command == "extremes":
        db.print_extremes(args.name)

    else:
        print("Command was not recognized")

    exit(0)
