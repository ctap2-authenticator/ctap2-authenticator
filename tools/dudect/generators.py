import os


class RandomZeroGenerator:

    def __init__(self, length=112):
        self.length = 112

    def generate_input(self, left, test_run):
        if left:
            return bytearray(os.urandom(self.length))
        else:
            return bytearray(b"\x00" * self.length)


class RandomZeroFixedKeyGenerator:

    def __init__(self, key, length=112):
        self.length = length - len(key)
        self.key = bytearray(key)

    def generate_input(self, left, test_run):
        if left:
            return self.key + bytearray(os.urandom(self.length))
        else:
            return self.key + bytearray(b"\x00" * self.length)


class RandomZeroFixedDataGenerator:
    def __init__(self, data):
        self.data = bytearray(data)

    def generate_input(self, left, test_run):
        if left:
            return bytearray(os.urandom(32)) + self.data
        else:
            return bytearray(b"\x00" * 31 + b"\x01") + self.data


class RandomRandomGenerator:

    def __init__(self, length=112):
        self.length = length

    def generate_input(self, left, test_run):
        return bytearray(os.urandom(self.length))
