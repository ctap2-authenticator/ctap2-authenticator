from tools.dudect.ttest import TPair
import random
import math
import time


# TODO: Remove Statistics related stuff
class TestRun:

    def __init__(self, name, test_id, db, dut, gen, n_runs=0, n_measurements=20, max_runs=None):

        # Set the metadata necessary for data recording
        self.name = name
        self.test_id = test_id
        self.db = db

        self.n_runs = n_runs
        if self.n_runs is None:
            self.n_runs = 0

        self.max_runs = max_runs

        # Set the test parameters
        self.n_measurements = n_measurements


        # Store the device under test and the testcase generator
        self.dut = dut
        self.gen = gen

        # We always switch back and forth between left and right class
        self.left = False

    def step(self):
        if self.max_runs is not None and self.n_runs >= self.max_runs:
            raise Exception("Ran " + str(self.n_runs)
                            + " tests already. We wanted to run "
                            + str(self.max_runs) + " Goodbye")

        exec_times = []
        runs = []
        # In a single step we take a fixed number of measurements

        # Randomly select whether we want to test the left or the right class
        #left = bool(random.getrandbits(1))

        # Switch back and forth between the two classes
        self.left = not self.left

        left_sting = "left" if self.left else "right"

        # Generate input data via a user supplied
        # It is up to the user to decide whether the two classes should be served different data or not
        # I.e. null vector / random vs. random / random
        test_data = self.gen.generate_input(self.left, self)

        for i in range(0, self.n_measurements):
            # Take a measurement with the generated input
            exec_time = self.dut.measure(test_data)
            if exec_time == 0:
                print("INVALID DATA, skipping value. Check Probe!!!")
                i -= 1
                continue

            exec_times.append((exec_time, self.left))

            now = int(time.time())
            self.n_runs += 1
            runs.append((self.test_id, self.n_runs, now, self.left, test_data, exec_time))

        self.db.store_testcases(self.test_id, runs)

        print("Tests run(" + left_sting + "): " + str(self.n_runs - 1))
