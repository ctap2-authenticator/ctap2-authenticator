from fido2.hid import CtapHidDevice
from fido2.client import Fido2Client


class DeviceProbe:

    def __init__(self):
        self.dev = Probe()
        self.dev.find_device()

    def measure(self, test_data):
        return self.dev.sign_test(test_data[0:32], test_data[32:])


class Probe:

    def __init__(self):

        self.n_tests = 0
        self.dev = None
        self.client = None
        self.ctap2 = None

    def find_device(self):
        devices = list(CtapHidDevice.list_devices())
        print(devices)

        if not devices or len(devices) < 1:
            raise RuntimeError("No suitable devices detected")

        self.dev = devices[0]
        self.client = Fido2Client(self.dev, "https://example.org")
        self.ctap2 = self.client.ctap2

    def sign_test(self, key, data):
        self.n_tests += 1

        out_bytes = b""

        if not isinstance(key, bytearray) or len(key) != 32:
            raise RuntimeError("Key malformed: " + str(key))

        out_bytes += key
        out_bytes += data

        in_bytes = self.dev.call(0x5b, data=out_bytes)

        time = int.from_bytes(in_bytes[1:], byteorder="little", signed=False)
        print("Time: " + str(time) + " instructions")
        return time


class Packet(object):
    def __init__(self, data):
        self.data = data

    def ToWireFormat(self, ):
        return self.data

    @staticmethod
    def FromWireFormat(pkt_size, data):
        return Packet(data)
