import math


class TTest:

    def __init__(self):
        self.n = 0
        self.mean = 0.0
        self.m2 = 0.0

    def push(self, x):
        self.n += 1

        # Welford's online algorithm
        delta = x - self.mean
        self.mean += delta / self.n
        self.m2 += delta * (x - self.mean)

    def test(self, other):
        if self.n == 0 or other.n == 0:
            return 0

        if self.n <= 1 or other.n <= 1:
            return 0

        self_variance = self.m2 / (self.n - 1)
        other_variance = other.m2 / (other.n - 1)

        num = self.mean - other.mean
        den = math.sqrt(self_variance / self.n + other_variance / other.n)
        return abs(num / den)


class TPair:

    def __init__(self):
        self.left = TTest()
        self.right = TTest()

    def test(self):
        return self.left.test(self.right)

    def get_n(self):
        return min(self.left.n, self.right.n)


class TStats:

    def __init__(self, exec_times):
        self.first_order = TPair()
        self.second_order = TPair()

    def update_ttests(self, left, exec_time):
        # Update first order statistics
        if left:
            self.first_order.left.push(exec_time)
        else:
            self.first_order.right.push(exec_time)

        # Update second order statistics if there are enough samples
        if self.first_order.get_n() > 10000:
            if left:
                centered = exec_time - self.first_order.left.mean
                self.second_order.left.push(centered * centered)
            else:
                centered = exec_time - self.first_order.right.mean
                self.second_order.right.push(centered * centered)
