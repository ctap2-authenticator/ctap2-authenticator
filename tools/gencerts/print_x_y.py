#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import sys

from ecdsa import SigningKey

sk = SigningKey.from_pem(open(sys.argv[1]).read())


print('Private key in various formats:')
print("Decimal:")
print([c for c in sk.to_string()])
print("Hexadecimal:")
print(''.join(['%02x' % c for c in sk.to_string()]))
print("C literal:")
print('"\\x' + '\\x'.join(['%02x' % c for c in sk.to_string()]) + '"')
print("Rust array:")
print('[0x' + ', 0x'.join(['%02x' % c for c in sk.to_string()]) + ']')
print()
