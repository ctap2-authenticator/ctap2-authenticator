# Ctap2 Authenticator

## Badges

[![Pipelne Probe][pipeline-badge]](https://gitlab.com/ctap2-authenticator/ctap2-authenticator)
[![Version][version-badge]](https://crates.io/crates/ctap2-authenticator)
[![Doc Version][docs-version-badge]](https://docs.rs/ctap2-authenticator)

Embedded Rust CTAP2 Authenticator implementation

## Documentation

The documentation of the latest release can be found [here][docs-release].

A fresh build of the development documentation can be found [here][docs-nightly].

## Installation

Full installation process, assuming no perquisites at all, except a black Ubuntu 20.04. 

### Install Rust toolchain

##### Get Rust

To use the different parts of this project a number of dependencies are required.
This was tested on Ubuntu 20.04.

To build the project, the Rust toolchain is required.
We tested the build with Rust 1.45.2

    sudo apt install -y curl git gcc libssl-dev
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    
After following the instructions of the installer, the toolchain is installed.

**NOTE: All software should build on stable Rust**

It will automatically set up the `.profiles` to include the Rust build
tool `cargo`. It should be available after logging out and then back in.
Alternatively, run:

    source $HOME/.cargo/env
    
##### Get Targets
To build the STM32F401 version, the Rust compiler needs to support the
ARM Cortex-M4 architecture:

    rustup target add thumbv7em-none-eabi

##### Get C cross-compiler

To be able to build the C dependencies (required for the STM32F401 build),
a number of additional packages have to be installed.

    sudo apt install libclang-dev gcc-arm-none-eabi  

**NOTE: The core crate does not have any C dependencies**

##### Clone project
To clone the project, run:

    git clone https://gitlab.com/ctap2-authenticator/ctap2-authenticator.git

##### Build and Test core
After cloning the Repository, you should be able to run the tests:

    cd ctap2-authenticator/ctap2-authenticator
    cargo test --all-features

**NOTE: The tests do not compile without the `std` feature**

#### Build and Test for development board
To build for the development board, `gdb-multiarch` and `openocd` are required.

    sudo apt install gdb-multiarch

Unfortunately, the `openocd` version that comes with Ubuntu 20.04 is unfit.
We need to build our own.

    cd
    sudo apt install make libtool libusb-1.0-0-dev
    git clone https://git.code.sf.net/p/openocd/code openocd-code
    cd openocd-code
    ./bootstrap
    ./configure --enable-stlink
    make -j
    sudo make install

Now that `openocd` is installed, we can install and run the project.
Connect the programmer and:

    cd ctap2-authenticator/stm32f401-authenticator
    openocd
    
**NOTE: Sometimes openocd is not able to grab the programmer. Retry a couple times. Also check udev
rules or try sudo.**

Now, while `openocd` is running, in another window run:
    
    cargo run --release --target thumbv7em-none-eabi

**NOTE: Sometimes there is an error with the nisty packet. In that case try:`**

    CC=/usr/lib/arm-none-eabi/include cargo run --release --target thumbv7em-none-eabi

### Setup dev tools

A number of development tools might come in handy
    
    sudo apt install libc6-dev-i386-cross
    cargo install cargo-audit cargo-outdated cargo-geiger cargo-bloat

### Setup Test tool

The test tool is described in its own `README`.
Check out `ctap2-tests/README.md`.

## License

[Apache-2.0][apache2-link] or [MIT][mit-link].

[//]: # (links)
[docs-release]: https://docs.rs/ctap2-authenticator/
[docs-nightly]: https://ctap2-authenticator.gitlab.io/ctap2-authenticator/ctap2_authenticator
[docs-version-badge]: https://docs.rs/ctap2-authenticator/badge.svg

[pipeline-badge]: https://gitlab.com/ctap2-authenticator/ctap2-authenticator/badges/master/pipeline.svg

[version-badge]:https://img.shields.io/crates/v/ctap2-authenticator

[apache2-link]: https://spdx.org/licenses/Apache-2.0.html
[mit-link]: https://spdx.org/licenses/MIT.htm
