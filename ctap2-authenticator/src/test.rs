use crate::credentials::{RpCredential, UserCredential};
use crate::usbhid::{CtapHidCapabilities, CtapHidPlatform, KeepaliveResponse};
use crate::{AuthenticatorPlatform, CredentialDescriptorList, CtapOptions, PublicKey, Signature};
use core::time::Duration;
use rand;

/// A non function implementation of the `AuthenticatorPlatform` trait.
/// Used to supply the constants to the test cases.
#[derive(Debug)]
pub(crate) struct MockCtapPlatform;
impl AuthenticatorPlatform for MockCtapPlatform {
    const AAGUID: [u8; 16] = [
        0xf8, 0xa0, 0x11, 0xf3, 0x8c, 0x0a, 0x4d, 0x15, 0x80, 0x06, 0x17, 0x11, 0x1f, 0x9e, 0xdc,
        0x7d,
    ];
    const CERTIFICATE: Option<&'static [u8]> = None;
    const MAX_MSG_LENGTH: u16 = 7609;
    const SUPPORTED_OPTIONS: CtapOptions = CtapOptions {
        plat: None,
        rk: None,
        client_pin: None,
        up: Some(true),
        uv: Some(false),
    };
    type CredentialId = Vec<u8>;
    type PublicKeyBuffer = [u8; 32];
    type SignatureBuffer = [u8; 32];
    type CredentialIterator = std::vec::IntoIter<(Self::CredentialId, u32)>;

    fn reset(&mut self) -> Result<(), ()> {
        unimplemented!()
    }

    fn check_exclude_list(&mut self, _rp_id: &str, _list: CredentialDescriptorList) -> bool {
        unimplemented!()
    }

    fn locate_credentials(
        &mut self,
        _rp_id: &str,
        _list: Option<CredentialDescriptorList>,
    ) -> (u16, Self::CredentialIterator) {
        unimplemented!()
    }

    fn create_credential(
        &mut self,
        _rp: RpCredential<&[u8], &str>,
        _user: UserCredential<&[u8], &str>,
    ) -> (Self::CredentialId, PublicKey<Self::PublicKeyBuffer>, u32) {
        unimplemented!()
    }

    fn attest(
        &mut self,
        _id: &Self::CredentialId,
        _data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>> {
        Some(Signature::ed25519(
            [
                0x4a, 0x5a, 0x9d, 0xd3, 0x92, 0x98, 0x14, 0x9d, 0x90, 0x47, 0x69, 0xb5, 0x1a, 0x45,
                0x14, 0x33, 0x00, 0x6f, 0x18, 0x2a, 0x34, 0xfb, 0xdf, 0x66, 0xde, 0x5f, 0xc7, 0x17,
                0xd7, 0x5f, 0xb3, 0x50,
            ],
            [
                0xa4, 0x6b, 0x8e, 0xa3, 0xc3, 0xb9, 0x33, 0x82, 0x1c, 0x6e, 0x7f, 0x5e, 0xf9, 0xda,
                0xae, 0x94, 0xab, 0x47, 0xf1, 0x8d, 0xb4, 0x74, 0xc7, 0x47, 0x90, 0xea, 0xab, 0xb1,
                0x44, 0x11, 0xe7, 0xa0,
            ],
        ))
    }

    fn sign(
        &mut self,
        _id: &Self::CredentialId,
        _data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>> {
        Some(Signature::ed25519(
            [
                0x4a, 0x5a, 0x9d, 0xd3, 0x92, 0x98, 0x14, 0x9d, 0x90, 0x47, 0x69, 0xb5, 0x1a, 0x45,
                0x14, 0x33, 0x00, 0x6f, 0x18, 0x2a, 0x34, 0xfb, 0xdf, 0x66, 0xde, 0x5f, 0xc7, 0x17,
                0xd7, 0x5f, 0xb3, 0x50,
            ],
            [
                0xa4, 0x6b, 0x8e, 0xa3, 0xc3, 0xb9, 0x33, 0x82, 0x1c, 0x6e, 0x7f, 0x5e, 0xf9, 0xda,
                0xae, 0x94, 0xab, 0x47, 0xf1, 0x8d, 0xb4, 0x74, 0xc7, 0x47, 0x90, 0xea, 0xab, 0xb1,
                0x44, 0x11, 0xe7, 0xa0,
            ],
        ))
    }

    fn start_timeout(&mut self) {
        unimplemented!()
    }

    fn has_timed_out(&mut self, _time: Duration) -> bool {
        unimplemented!()
    }
}

#[derive(Debug)]
pub(crate) struct MockCtapHidPlatform {
    pub winked: bool,
    pub locked: bool,
    pub canceled: bool,
    pub next_keepalive: KeepaliveResponse,
}

impl MockCtapHidPlatform {
    pub fn new() -> Self {
        Self {
            winked: false,
            locked: false,
            canceled: false,
            next_keepalive: KeepaliveResponse::None,
        }
    }
}

impl CtapHidPlatform for MockCtapHidPlatform {
    const MAJOR_VERSION: u8 = 1;
    const MINOR_VERSION: u8 = 3;
    const BUILD_VERSION: u8 = 7;
    const CAPABILITIES: CtapHidCapabilities = CtapHidCapabilities {
        wink: true,
        cbor: true,
        msg: false,
    };

    fn wink(&mut self) {
        self.winked = true;
        println!("BLINK BLINK");
    }

    fn lock(&mut self) {
        self.locked = true;
        println!("Locked");
    }

    fn cancel(&mut self) {
        self.canceled = true;
        println!("Canceling request");
    }

    fn keepalive_needed(&mut self) -> KeepaliveResponse {
        self.next_keepalive
    }
}

/// Fill the buffer with random bytes.
/// Useful for randomized testing.
pub(crate) fn random_bytes(data: &mut [u8]) {
    for d in data {
        *d = rand::random::<u8>();
    }
}

/// A mock packet generator, which can generate random packet sequences with the specified values
pub(crate) struct RandomPacketIterator {
    channel_id: u32,
    cmd: u8,
    length: u16,
    bytes_sent: u16,
    pkg_seq: u8,
}

impl RandomPacketIterator {
    pub(crate) fn new(channel_id: u32, cmd: u8, length: u16) -> Self {
        Self {
            channel_id,
            cmd,
            length,
            bytes_sent: 0,
            pkg_seq: 0,
        }
    }
}

impl Iterator for RandomPacketIterator {
    type Item = [u8; 64];

    fn next(&mut self) -> Option<[u8; 64]> {
        assert!(self.bytes_sent <= self.length);

        if self.bytes_sent == self.length {
            return None;
        }

        let mut buf = [0; 64];
        random_bytes(&mut buf);
        buf[0..4].copy_from_slice(&self.channel_id.to_be_bytes());

        if self.bytes_sent == 0 {
            buf[4] = self.cmd;
            buf[5..7].copy_from_slice(&self.length.to_be_bytes());

            let num_bytes = u16::min(57, self.length - self.bytes_sent);
            self.bytes_sent += num_bytes;
            for x in 7 + num_bytes..64 {
                buf[x as usize] = 0;
            }
        } else {
            buf[4] = self.pkg_seq;
            self.pkg_seq += 1;

            let num_bytes = u16::min(59, self.length - self.bytes_sent);
            self.bytes_sent += num_bytes;
            for x in 5 + num_bytes..64 {
                buf[x as usize] = 0;
            }
        }

        Some(buf)
    }
}
