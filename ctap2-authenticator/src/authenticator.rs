use crate::bytes::Bytes;
use crate::credentials::{RpCredential, UserCredential};
use crate::error::CtapError;
use crate::messages::{Fido2Internal, Fido2Request, Fido2Response};
use crate::protocol::authdata::{AttestedCredential, AuthDataFlags, AuthenticatorData};
use crate::protocol::request::{GetAssertionRequest, MakeCredentialRequest, Request};
use crate::protocol::response::{
    GetAssertionResponse, GetInfoResponse, MakeCredentialResponse, Response,
};
use crate::{AuthenticatorPlatform, CredentialDescriptor};
use bitflags::_core::time::Duration;
use serde::export::from_utf8_lossy;
use sha2::digest::Digest;
use sha2::Sha256;

/// The internal state of the [`Authenticator`](crate::Authenticator)
enum AuthenticatorState<I> {
    Ready,
    EnumeratingAssertions {
        client_data_hash: Bytes<[u8; 32]>,
        rp_id_hash: Bytes<[u8; 32]>,
        flags: AuthDataFlags,
        num_credentials: u16,
        credential_counter: u16,
        iterator: I,
    },
}

/// The `Authenticator` handles all the state and processing of CTAP2 messages.
pub struct Authenticator<P: AuthenticatorPlatform> {
    state: AuthenticatorState<P::CredentialIterator>,
    platform: P,
}

impl<P: AuthenticatorPlatform> Authenticator<P> {
    /// Creates an `Authenticator` from the specified platform
    pub fn create(platform: P) -> Self {
        Authenticator {
            state: AuthenticatorState::Ready,
            platform,
        }
    }

    pub fn process<B: AsMut<[u8]>>(&mut self, request: Fido2Request<B>) -> Fido2Response<B> {
        match request.0 {
            Fido2Internal::Probe {
                channel_id,
                mut buf,
                length,
            } => {
                let new_length = self.platform.probe(buf.as_mut(), length);

                Fido2Response(Fido2Internal::Probe {
                    channel_id,
                    buf,
                    length: new_length,
                })
            }
            // TODO: Gracefully return an error here
            Fido2Internal::U2F { .. } => todo!(),
            Fido2Internal::Ctap2 {
                channel_id,
                mut buf,
                length: _length,
            } => {
                let new_length = self.process_ctap2(buf.as_mut());

                Fido2Response(Fido2Internal::Ctap2 {
                    channel_id,
                    buf,
                    length: new_length,
                })
            }
        }
    }

    fn process_ctap2(&mut self, data: &mut [u8]) -> u16 {
        let request = match Request::parse(data) {
            Ok(request) => request,
            Err(error) => {
                // On error, return invalid cbor error
                log::warn!("Error while parsing request: {:?}", error);
                data[0] = error.to_u8();
                return 1;
            }
        };
        log::info!("Parsed request: {:?}", request);

        let mut client_data_hash: [u8; 32] = [0; 32];
        let mut need_late_check = false;

        // Execute the protocol as specified by the CTAP specification chapter 5
        // We dispatch on the actual command while also retrieving the client data hash which we
        // still need during parsings
        let maybe_response = match request {
            Request::MakeCredential(request) => {
                need_late_check = true;
                client_data_hash.clone_from_slice(request.client_data_hash.inner());
                self.process_make_credential(request)
            }
            Request::GetAssertion(request) => {
                need_late_check = true;
                client_data_hash.clone_from_slice(request.client_data_hash.inner());
                self.process_get_assertion(request)
            }
            Request::GetNextAssertion => {
                if let AuthenticatorState::EnumeratingAssertions {
                    client_data_hash: ref inner,
                    ..
                } = self.state
                {
                    client_data_hash.copy_from_slice(inner);
                }

                self.process_get_next_assertion()
            }
            Request::GetInfo => self.process_get_info(),
            Request::Reset => match self.platform.reset() {
                Ok(_) => Ok(Response::Reset),
                Err(_) => Err(CtapError::OperationDenied),
            },
        };

        // Either parse the response CBOR back into the array or return an error
        match maybe_response {
            Ok(response) => {
                log::info!("Returning response: {:?}", response);
                match response.parse(
                    &mut self.platform,
                    Bytes::from(client_data_hash),
                    &mut data[..],
                ) {
                    Ok(size) => {
                        // Perform the late user check
                        if need_late_check && !self.platform.late_user_check() {
                            log::debug!("Late user check failed");
                            data[0] = CtapError::OperationDenied.to_u8();
                            1
                        } else {
                            log::debug!("Parsed response size: {} bytes", size);
                            size as u16
                        }
                    }
                    Err(error) => {
                        log::warn!("Error while parsing response {:?}", error);
                        data[0] = error.to_u8();
                        1
                    }
                }
            }
            Err(error) => {
                // Set the error and return
                log::warn!("Returning error: {:?}", error);
                data[0] = error.to_u8();
                1
            }
        }
    }

    pub(crate) fn process_make_credential<'a>(
        &mut self,
        request: MakeCredentialRequest,
    ) -> Result<Response<&'a [u8], &'a str, P>, CtapError> {
        // If exclude list is present and contains known credentials, return error -- Step 1
        if let Some(list) = request.exclude_list {
            if self
                .platform
                .check_exclude_list(&from_utf8_lossy(request.rp.id.as_ref()), list)
            {
                return Err(CtapError::CredentialExcluded);
            }
        }

        // Return if we don't support the Algorithm. -- Step 2
        if !request.public_key_params.es256 {
            return Err(CtapError::UnsupportedAlgorithm);
        }

        // Process and reject unsupported and invalid options -- Step 3
        let options = request.options;
        // Reject invalid options -- these options MUST be None
        if options.client_pin.is_some() || options.plat.is_some() || options.up.is_some() {
            return Err(CtapError::InvalidOption);
        }

        // Reject unsupported Options
        if (Some(true) == options.rk && Some(true) != P::SUPPORTED_OPTIONS.rk)
            || (Some(true) == options.uv && Some(true) != P::SUPPORTED_OPTIONS.uv)
        {
            return Err(CtapError::UnsupportedOption);
        }

        // We would check extensions here once they are parsable -- Step 4

        // We do not do any PinAuth, thus any parameters here are invalid
        // This 'implements' step 5, 6 and 7
        if request.pin_auth.is_some() || request.pin_protocol.is_some() {
            return Err(CtapError::PinAuthInvalid);
        }

        // Hash the relying party id. We do this here because the data is moved later.
        // We could work around it but it makes the lifetimes more complicated
        let mut hasher = Sha256::new();
        hasher.update(&request.rp.id.inner());

        // Check for user gesture and get the credential id -- Step 8 and 9
        self.platform.make_credential_prompt(
            RpCredential(request.rp.clone()),
            UserCredential(request.user.clone()),
        );
        if !self.platform.user_check() {
            return Err(CtapError::OperationDenied);
        }

        let (credential_id, public_key, sig_count) = self.platform.create_credential(
            RpCredential(request.rp.clone()),
            UserCredential(request.user.clone()),
        );

        // After the call, we are in state ready
        self.state = AuthenticatorState::Ready;

        Ok(Response::MakeCredential(MakeCredentialResponse {
            auth_data: AuthenticatorData {
                rp_id_hash: Bytes::from(hasher.finalize().into()),
                flags: AuthDataFlags::UP | AuthDataFlags::AT,
                sig_count,
                attested_credential: Some(AttestedCredential {
                    credential_id,
                    public_key,
                }),
            },
        }))
    }

    pub(crate) fn process_get_assertion<'a>(
        &mut self,
        request: GetAssertionRequest,
    ) -> Result<Response<&'a [u8], &'a str, P>, CtapError> {
        // Locate all credentials eligible for retrieval under the specified criteria -- Step 1
        let (num_credentials, mut credential_list) = self
            .platform
            .locate_credentials(request.rpid, request.allow_list);

        // We do not support any PinAuth at the moment. Thus any PinAuth requests are invalid.
        // This 'implements' step 2, 3 and 4
        if request.pin_auth.is_some() || request.pin_protocol.is_some() {
            return Err(CtapError::PinAuthInvalid);
        }

        // Process and reject unsupported options -- Step 5
        let options = request.options;
        if options.client_pin.is_some() || options.plat.is_some() {
            //return Err(CtapError::UnsupportedOption);
            return Err(CtapError::InvalidOption);
        }

        // Reject unsupported options -- Still Step 7
        if (Some(true) == options.rk && Some(true) != P::SUPPORTED_OPTIONS.rk)
            || (Some(true) == options.uv && Some(true) != P::SUPPORTED_OPTIONS.uv)
        {
            return Err(CtapError::UnsupportedOption);
        }

        // We would check extensions here once they are parsable -- Step 6

        let (first_credential, sig_count) = match credential_list.next() {
            Some((cred, count)) => (cred, count),
            None => return Err(CtapError::NoCredentials),
        };

        self.platform
            .get_assertion_prompt(request.rpid, &first_credential);
        if !self.platform.user_check() {
            return Err(CtapError::OperationDenied);
        }

        assert_ne!(num_credentials, 0);

        // Hash the rp id
        let mut hasher = Sha256::new();
        hasher.update(&request.rpid);
        let rp_id_hash = Bytes::from(hasher.finalize().into());

        // Check which flags to return
        let flags = if options.up == Some(true) {
            AuthDataFlags::UP
        } else {
            AuthDataFlags::NONE
        };

        // If more than one credential was found:
        if num_credentials > 1 {
            // Store the iterator and context data for use in GetNextAssertion calls
            let mut client_data_hash: [u8; 32] = [0; 32];
            client_data_hash.clone_from_slice(request.client_data_hash.inner());

            self.state = AuthenticatorState::EnumeratingAssertions {
                client_data_hash: Bytes::from(client_data_hash),
                num_credentials,
                flags,
                rp_id_hash: rp_id_hash.clone(),
                credential_counter: 0,
                iterator: credential_list,
            };

            // start the timeout
            self.platform.start_timeout();
        } else {
            // If only one credential is being returned, we are in the ready state
            self.state = AuthenticatorState::Ready;
        }

        Ok(Response::GetAssertion(GetAssertionResponse {
            credential: CredentialDescriptor {
                id: first_credential,
                transports: None,
            },
            auth_data: AuthenticatorData {
                rp_id_hash,
                flags,
                sig_count,
                attested_credential: None,
            },
            user: None,
            num_credentials: if num_credentials > 1 {
                Some(num_credentials)
            } else {
                None
            },
        }))
    }

    pub(crate) fn process_get_next_assertion<'a>(
        &mut self,
    ) -> Result<Response<&'a [u8], &'a str, P>, CtapError> {
        // Check that we are in the correct state
        if let AuthenticatorState::EnumeratingAssertions {
            num_credentials,
            ref rp_id_hash,
            flags,
            ref mut credential_counter,
            ref mut iterator,
            ..
        } = self.state
        {
            // If the credentialCounter is equal to or greater than numberOfCredentials,
            // return CTAP2_ERR_NOT_ALLOWED -- Step 2
            if *credential_counter >= num_credentials {
                return Err(CtapError::NotAllowed);
            }

            // If the time since last get_next_assertion is longer than 30 secs,
            // it has timed out -- Step 4
            if self.platform.has_timed_out(Duration::from_secs(30)) {
                return Err(CtapError::NotAllowed);
            }

            // Reset timer and increment
            *credential_counter += 1;
            self.platform.start_timeout();

            // return the next credential
            let (next_credential, sig_count) = match iterator.next() {
                Some((cred, count)) => (cred, count),
                None => return Err(CtapError::NotAllowed),
            };

            Ok(Response::GetNextAssertion(GetAssertionResponse {
                credential: CredentialDescriptor {
                    id: next_credential,
                    transports: None,
                },
                auth_data: AuthenticatorData {
                    rp_id_hash: rp_id_hash.clone(),
                    flags,
                    sig_count,
                    attested_credential: None,
                },
                // TODO: Add user information here
                user: None,
                num_credentials: None,
            }))
        } else {
            // If we are not enumerating credentials, we return error -- Step 1
            Err(CtapError::NotAllowed)
        }
    }

    pub(crate) fn process_get_info<'a>(
        &mut self,
    ) -> Result<Response<&'a [u8], &'a str, P>, CtapError> {
        // After the call, we are in state ready
        self.state = AuthenticatorState::Ready;

        Ok(Response::GetInfo(GetInfoResponse {
            aaguid: Bytes::from(&P::AAGUID),
            options: &P::SUPPORTED_OPTIONS,
            max_msg_size: P::MAX_MSG_LENGTH,
        }))
    }
}
