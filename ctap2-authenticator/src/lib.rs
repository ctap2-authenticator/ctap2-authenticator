#![cfg_attr(not(feature = "std"), no_std)]
#![forbid(unsafe_code)]

#[cfg(all(feature = "alloc", not(feature = "std")))]
extern crate alloc;

use core::fmt::Debug;
use core::time::Duration;

pub use authenticator::Authenticator;
pub use protocol::descriptor::{CredentialDescriptor, Transports};
pub use protocol::descriptorlist::CredentialDescriptorList;
pub use protocol::entity::CtapOptions;
pub use protocol::key::{PublicKey, PublicKeyParams};
pub use protocol::signature::Signature;

use crate::credentials::{RpCredential, UserCredential};

#[cfg(test)]
mod test;

mod authenticator;
mod bytes;
mod error;
pub(crate) mod protocol;

pub mod credentials;
pub mod messages;
pub mod usbhid;

/// This trait encapsulates the application specific code of the authenticator.
#[allow(unused_variables)]
pub trait AuthenticatorPlatform: Debug {
    /// The AAGUID of this Authenticator.
    ///
    /// This should be a randomly selected value and be the same
    /// for all Authenticators of similar type. See the
    /// [specification](https://www.w3.org/TR/webauthn/#aaguid) for more details.
    const AAGUID: [u8; 16];

    /// The Certificate used during attestation.
    ///
    /// If self certification is used, this value has to be `None`.
    /// If a certificate authority is used, this has to point to the Attestation CA, which is stored
    /// in ASN.1 DER format. See the [specification](https://www.w3.org/TR/webauthn/#attca) for
    /// further detail.
    const CERTIFICATE: Option<&'static [u8]>;

    /// Specified the maximum length of the messages this authenticator supports in bytes.
    ///
    /// This value should not be shorter than 1024 and no longer than 7609 bytes.
    const MAX_MSG_LENGTH: u16;

    /// Specifies the options which are supported by this authenticator.
    const SUPPORTED_OPTIONS: CtapOptions;

    /// Specifies by which type the [`CredentialId`](https://www.w3.org/TR/webauthn/#credential-id)
    /// is backed.
    ///
    /// This needs to be an array of at least 16 bytes and contain 100 bits of entropy. See the
    /// [specification](https://www.w3.org/TR/webauthn/#credential-id) for more details.
    type CredentialId: AsRef<[u8]> + Debug + PartialEq + Eq;

    /// Specifies the type which backs up the [`PublicKey`](struct.PublicKey.html) type.
    ///
    /// This can be any type that implements
    /// [`AsRef<[u8]>`](https://doc.rust-lang.org/std/cell/struct.Ref.html) but it needs
    /// to be large enough to hold any the variant of [`PublicKeys`](struct.PublicKey.html)
    /// that the platform wants to support.
    ///
    /// For example, if this authenticator wants to support 2048-bit RSA, the PublicKeyBuffer needs
    /// to be able to hold 256 bytes. In that case it might be sensible to back the the buffer with
    /// a heap allocated vec.
    ///
    /// For ECC, it needs to be able to holds a single coordinate,
    /// e.g. 32 bytes for Ed25519.
    type PublicKeyBuffer: AsRef<[u8]> + Debug + PartialEq + Eq;

    /// Specifies the type which backs up the signature.
    ///
    /// This can be any type that implements
    /// [`AsRef<[u8]>`](https://doc.rust-lang.org/std/cell/struct.Ref.html) but it needs to be
    /// large enough to hold any signature type this authenticator wants to support.
    ///
    /// For Ed25519 this buffer needs to be able to store 64 bytes.
    type SignatureBuffer: AsRef<[u8]> + Debug + PartialEq + Eq;

    /// Defines the type of the credential iterator.
    ///
    /// In the `GetAssertion` call, a number of credentials might be returned.
    type CredentialIterator: Iterator<Item=(Self::CredentialId, u32)>;

    /// Instructs the [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html) to reset the
    /// Authenticator.
    ///
    /// Depending on the platform, this may require destroying/regenerating all key material,
    /// and deleting all residential keys.
    ///
    /// **Caution: Returning from this function indicates Reset was completed. Do not defer
    /// destruction of secret material beyond returning from this function!**
    ///
    /// Depending on the application, it might be useful to acquire user consent before resetting
    /// the authenticator.
    ///
    /// # Returns
    /// - `Err(())` if the authenticator was not reset, for any reason
    /// - `Ok(())` if the authenticator was successfully reset
    fn reset(&mut self) -> Result<(), ()>;

    /// Check if any of the credentials in the list known are to this authenticator.
    ///
    /// # Arguments
    /// - `list`: The list of [`CredentialDescriptors`](struct.CredentialDescriptor.html)
    /// which are to be checked.
    ///
    /// # Returns
    /// - `true` if any of the [`CredentialDescriptors`](struct.CredentialDescriptor.html)
    /// are known to the authenticator,
    /// - `false` otherwise.
    fn check_exclude_list(&mut self, rp_id: &str, list: CredentialDescriptorList) -> bool;

    /// Ask the [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html) to locate all
    /// [`CredentialIds`](https://www.w3.org/TR/webauthn/#credential-id), which might fit the
    /// [`CredentialDescriptors`](struct.CredentialDescriptor.html) listed in the
    /// [`CredentialDescriptorList`](struct.CredentialDescriptorList.html)
    ///
    /// # Arguments
    /// - `rp_id`: The relying party id
    /// - `list`: The list of [`CredentialDescriptors`](struct.CredentialDescriptor.html).
    ///
    /// # Returns
    /// The number of credentials found and an
    /// [`Iterator`](https://doc.rust-lang.org/std/iter/trait.Iterator.html) over their
    /// [`CredentialIds`](https://www.w3.org/TR/webauthn/#credential-id).
    ///
    /// # Note
    /// The type of the [`CredentialIds`](https://www.w3.org/TR/webauthn/#credential-id)
    /// if specific to the platform and is set via an associated type in this trait.
    fn locate_credentials(
        &mut self,
        rp_id: &str,
        list: Option<CredentialDescriptorList>,
    ) -> (u16, Self::CredentialIterator);

    /// Asks the platform to create a credential.
    ///
    /// This function is called on a make credential request after the first user check was
    /// performed.
    /// The platform has to generate a new credential in a platform specific way.
    /// This might include storing necessary values into a database.
    ///
    /// # Arguments
    /// - `rp`: The [`RpCredential`](struct.RPCredential)
    /// of the relying party which wants to sign the user in. Can be used for display purposes.
    /// - `user`: The [`UserCredential`](struct.UserCredential)
    /// as which the relying party which wants to sign the user in.
    ///
    /// # Returns
    /// - The [`CredentialId`](https://www.w3.org/TR/webauthn/#credential-id) of the newly created
    /// credential
    /// - The corresponding [`PublicKey`](struct.PublicKey.html).
    /// - The initial value of the signature counter
    fn create_credential(
        &mut self,
        rp_id: RpCredential<&[u8], &str>,
        user: UserCredential<&[u8], &str>,
    ) -> (Self::CredentialId, PublicKey<Self::PublicKeyBuffer>, u32);

    /// Prompt the user with the option to create a new credential.
    ///
    /// # Arguments
    /// - `rp`: The [`RpCredential`](struct.RPCredential)
    /// of the relying party which wants to sign the user in. Can be used for display purposes.
    /// - `user`: The [`UserCredential`](struct.UserCredential)
    /// as which the relying party which wants to sign the user in.
    ///
    /// The way to prompt the user to create a new credential is completely platform dependent.
    /// It could simply consist of blinking a LED of display the display the data on the screen.
    ///
    /// The default implementation does nothing.
    fn make_credential_prompt(
        &mut self,
        rp: RpCredential<&[u8], &str>,
        user: UserCredential<&[u8], &str>,
    ) {}

    /// Prompt the user with the option to create and assertion.
    ///
    /// # Arguments
    /// - `id`: The [`CredentialId`](https://www.w3.org/TR/webauthn/#credential-id)
    /// which will be used in the assertion. This can be used by the
    /// [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html)
    /// to look up information about the credential for display purposes.
    ///
    /// The default implementation does nothing.
    fn get_assertion_prompt(&mut self, rp_id: &str, credential_id: &Self::CredentialId) {}

    /// Performs a user check.
    ///
    /// The same user check function is used for creation and assertion of credentials.
    /// The user check is performed immediately after the corresponding prompt function.
    ///
    /// How the user check is performed is completely platform dependent. It might include a
    /// user presence check by pressing a button or something more complex.
    ///
    /// # Returns
    /// - `true` if user check was successful
    /// - `false` otherwise
    ///
    /// By default, it passes all user checks.
    fn user_check(&mut self) -> bool {
        true
    }

    /// Performs a second user check after the attestation/sign stage.
    ///
    /// At the time this function is called the response is already parsed and ready to be sent out.
    /// Having a late user check allows to parallelize user check and signature generation resulting
    /// in a perceived speedup.
    ///
    /// Doing this has no impact on security only  if the user check is a simple user presence check
    /// and the generated signature is kept in the same security domain as the device secret.
    ///
    /// # Returns
    /// - `true` if user check was successful
    /// - `false` otherwise
    ///
    /// By default, it passes all user checks.
    ///
    /// # Note
    /// This function is called after
    /// [`create_credential`](trait.AuthenticatorPlatform.html#tymethod.create_credential) and
    /// [`sign`](trait.AuthenticatorPlatform.html#tymethod.sign) have
    /// been called. To ensure correct behaviour, all state changes that have been performed during
    /// these function calls have to be reverted.
    fn late_user_check(&mut self) -> bool {
        true
    }

    /// Instructs the [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html)
    /// to generate the signature for an attestation.
    ///
    /// If this platform uses self attestation, the data has to be signed by the private key
    /// corresponding to the public key.
    /// If this platform uses an attestation CA, the data has to be signed over the corresponding
    /// attestation private key.
    ///
    /// # Arguments
    /// - `id`: The id to generate the attest for. If this is a self signing authenticator, the data
    /// must be signed with the assertion private key corresponding to this credential id.
    /// Otherwise, this arguments is ignored and the data is signed by the attestation private
    /// key.
    /// - `data`: The data to generate the attestation over.
    ///
    /// # Returns
    /// The signature of the attest on success.
    /// `None` on error.
    ///
    /// # Note
    /// The attest does not need to be included here. It will automatically be included during the
    /// serialization.
    fn attest(
        &mut self,
        id: &Self::CredentialId,
        data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>>;

    /// Instructs the [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html) to sign the data.
    ///
    /// The platform needs to sign over that `data` using the parameters specified by the
    /// [`PublicKeyParams`](struct.PublicKeyParams.html) and return the signature.
    ///
    /// Refer to the [`WebAuthn`](https://www.w3.org/TR/webauthn/#assertion-signature) specification
    /// about the details.
    ///
    /// # Arguments
    /// - `id` The credential id to sign over. Use the
    /// - `data`: The data to sign over.
    ///
    /// # Returns
    /// `None`, if the platform can not perform the task for whatever reason.
    /// The `Signature` and the [`PublicKey`](struct.PublicKey.html) used to generate it,
    /// if the algorithm is supported.
    fn sign(
        &mut self,
        id: &Self::CredentialId,
        data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>>;

    /// Instructs the [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html) to start a new
    /// timer.
    fn start_timeout(&mut self);

    /// Asks, whether the time since the last
    /// [`start_timeout`](trait.AuthenticatorPlatform.html#tymethod.start_timeout) call is longer
    /// than since.
    ///
    /// # Arguments
    /// `time`: The timeout time
    ///
    /// # Returns
    /// - `true` if the last call to
    /// [`start_timeout`](trait.AuthenticatorPlatform.html#tymethod.start_timeout) is a longer
    /// time in the past than `time`.
    /// - `false` otherwise.
    fn has_timed_out(&mut self, time: Duration) -> bool;

    /// Can be used to implement testing software on an authenticator.
    ///
    /// This functionality allows an implementor to send an arbitrary byte buffer to the buffer.
    /// The authenticator can inspect the buffer and its length and then manipulate it.
    /// The length of the return buffer is returned.
    /// # Arguments
    /// - `probe`: The probe buffer. The length of the buffer is not necessarily the length of the
    /// valid data.
    /// - `length`: The length of the valid data.
    ///
    /// # Returns
    /// The length of the response. The response itself is simply written into the probe buffer.
    fn probe(&mut self, probe: &mut [u8], length: u16) -> u16 {
        0
    }
}
