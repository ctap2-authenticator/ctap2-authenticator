use crate::error::CtapError;
use crate::messages::{Fido2Internal, Fido2Response};
use crate::usbhid::messages::{ChannelId, HidCmd, HidError, InitResponse, MsgType};
use crate::usbhid::{CtapHidPlatform, KeepaliveResponse};

/// Holds a number of packets to be sent over the USB connection.
#[derive(Clone, Debug)]
pub(crate) struct ResponseIterator<B: AsMut<[u8]>> {
    msg_type: MsgType,
    channel_id: ChannelId,
    pkg_seq: u8,
    bytes_sent: u16,
    pkg_length: u16,
    data: B,
}

impl<B: AsMut<[u8]>> ResponseIterator<B> {
    pub(crate) fn initialize(
        channel_id: ChannelId,
        data: B,
        pkg_length: u16,
        msg_type: MsgType,
    ) -> Self {
        Self {
            msg_type,
            channel_id,
            pkg_seq: 0,
            bytes_sent: 0,
            pkg_length,
            data,
        }
    }

    pub(crate) fn new(response: Fido2Response<B>) -> Self {
        match response.0 {
            Fido2Internal::U2F {
                channel_id,
                buf,
                length,
            } => Self::initialize(channel_id, buf, length, MsgType::Msg),
            Fido2Internal::Ctap2 {
                channel_id,
                buf,
                length,
            } => Self::initialize(channel_id, buf, length, MsgType::Cbor),
            Fido2Internal::Probe {
                channel_id,
                buf,
                length,
            } => Self::initialize(channel_id, buf, length, MsgType::Probe),
        }
    }

    pub(crate) fn is_finished(&self) -> bool {
        self.pkg_length - self.bytes_sent == 0
    }

    pub(crate) fn into_data(self) -> B {
        self.data
    }
}

impl<B: AsMut<[u8]>> Iterator for ResponseIterator<B> {
    type Item = [u8; 64];

    fn next(&mut self) -> Option<[u8; 64]> {
        let mut output = [0; 64];

        let bytes_left = self.pkg_length - self.bytes_sent;
        if bytes_left == 0 {
            return None;
        }

        if self.bytes_sent == 0 {
            // Build header and send maximal 57 bytes of data
            let pkg_len = u16::min(bytes_left, 57);

            output[0..4].copy_from_slice(&self.channel_id.to_bytes());
            output[4] = match self.msg_type {
                MsgType::Msg => HidCmd::Msg,
                MsgType::Cbor => HidCmd::Cbor,
                MsgType::Ping => HidCmd::Ping,
                MsgType::Probe => HidCmd::Probe,
            }
            .to_u8();
            output[5..7].copy_from_slice(&self.pkg_length.to_be_bytes());

            output[7..(pkg_len + 7) as usize].copy_from_slice(
                &self.data.as_mut()[self.bytes_sent as usize..(self.bytes_sent + pkg_len) as usize],
            );
            self.bytes_sent += pkg_len;

            Some(output)
        } else {
            // Build continuation packet with maximal 59 bytes of data
            let pkg_len = u16::min(bytes_left, 59);

            output[0..4].copy_from_slice(&self.channel_id.to_bytes());
            output[4] = self.pkg_seq;

            output[5..(pkg_len + 5) as usize].copy_from_slice(
                &self.data.as_mut()[self.bytes_sent as usize..(self.bytes_sent + pkg_len) as usize],
            );

            self.pkg_seq += 1;
            self.bytes_sent += pkg_len;

            Some(output)
        }
    }
}

pub(crate) fn init_response<P: CtapHidPlatform>(
    channel_id: ChannelId,
    init: InitResponse,
) -> [u8; 64] {
    let mut buf = [0; 64];

    buf[0..4].copy_from_slice(&channel_id.to_bytes());
    buf[4] = HidCmd::Init.to_u8();
    buf[6] = 17;
    init.to_buf::<P>(&mut buf[7..24]);

    buf
}

pub(crate) fn error_response(channel_id: ChannelId, error: HidError) -> [u8; 64] {
    let mut buf = [0; 64];

    buf[0..4].copy_from_slice(&channel_id.to_bytes());
    buf[4] = HidCmd::Error.to_u8();
    buf[6] = 1;
    buf[7] = error.to_u8();

    buf
}

pub fn keepalive_response(channel_id: ChannelId, kr: KeepaliveResponse) -> Option<[u8; 64]> {
    let kr = match kr {
        KeepaliveResponse::None => return None,
        KeepaliveResponse::Processing => 0x1,
        KeepaliveResponse::UpNeeded => 0x2,
    };

    let mut buf = [0; 64];

    buf[0..4].copy_from_slice(&channel_id.to_bytes());
    buf[4] = HidCmd::Keepalive.to_u8();
    buf[6] = 1;
    buf[7] = kr;

    Some(buf)
}

pub(crate) fn wink_response(channel_id: ChannelId) -> [u8; 64] {
    let mut buf = [0; 64];
    buf[0..4].copy_from_slice(&channel_id.to_bytes());
    buf[4] = HidCmd::Wink.to_u8();
    buf
}

pub(crate) fn lock_response(channel_id: ChannelId) -> [u8; 64] {
    let mut buf = [0; 64];
    buf[0..4].copy_from_slice(&channel_id.to_bytes());
    buf[4] = HidCmd::Lock.to_u8();
    buf
}

pub(crate) fn ctap_error_response(channel_id: ChannelId, error: CtapError) -> [u8; 64] {
    let mut buf = [0; 64];

    buf[0..4].copy_from_slice(&channel_id.to_bytes());
    buf[4] = HidCmd::Cbor.to_u8();
    buf[6] = 0x1;
    buf[7] = error.to_u8();

    buf
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test::{random_bytes, MockCtapHidPlatform};
    use crate::usbhid::test::assert_packet_eq;
    use std::ops::BitOr;

    // Test CBOR with a single packets
    #[test]
    fn cbor_short() {
        let mut cbor = [0; 40];
        random_bytes(&mut cbor);
        let channel_id = ChannelId::random();

        let mut buf = [0; 64];
        buf[0..4].copy_from_slice(&channel_id.to_bytes());
        buf[4] = 0x10_u8.bitor(0b1000_0000) as u8;
        buf[6] = 40;
        buf[7..47].copy_from_slice(&cbor);

        let cbor = Fido2Response(Fido2Internal::Ctap2 {
            channel_id,
            buf: cbor.to_vec(),
            length: cbor.len() as u16,
        });
        let output = ResponseIterator::new(cbor);
        assert_packet_eq(output, vec![buf]);
    }

    // Test CBOR with two packets
    #[test]
    fn cbor_medium() {
        let mut cbor = [0; 64];
        random_bytes(&mut cbor);
        let channel_id = ChannelId::random();

        let mut buf = vec![[0; 64]; 2];

        buf[0][0..4].copy_from_slice(&channel_id.to_bytes());
        buf[0][4] = 0x10_u8.bitor(0b1000_0000) as u8;
        buf[0][6] = 64;
        buf[0][7..64].copy_from_slice(&cbor[0..57]);
        buf[1][0..4].copy_from_slice(&channel_id.to_bytes());
        buf[1][4] = 0;
        buf[1][5..12].copy_from_slice(&cbor[57..64]);

        let cbor = Fido2Response(Fido2Internal::Ctap2 {
            channel_id,
            buf: cbor.to_vec(),
            length: cbor.len() as u16,
        });
        let output = ResponseIterator::new(cbor);
        assert_packet_eq(output, buf);
    }

    // Test CBOR with three packets
    #[test]
    fn cbor_long() {
        let mut cbor = [0; 120];
        random_bytes(&mut cbor);
        let channel_id = ChannelId::random();

        let mut buf = vec![[0; 64]; 3];

        buf[0][0..4].copy_from_slice(&channel_id.to_bytes());
        buf[0][4] = 0x10_u8.bitor(0b1000_0000) as u8;
        buf[0][6] = 120;
        buf[0][7..64].copy_from_slice(&cbor[0..57]);
        buf[1][0..4].copy_from_slice(&channel_id.to_bytes());
        buf[1][4] = 0;
        buf[1][5..64].copy_from_slice(&cbor[57..116]);
        buf[2][0..4].copy_from_slice(&channel_id.to_bytes());
        buf[2][4] = 1;
        buf[2][5..9].copy_from_slice(&cbor[116..120]);

        let cbor = Fido2Response(Fido2Internal::Ctap2 {
            channel_id,
            buf: cbor.to_vec(),
            length: cbor.len() as u16,
        });
        let output = ResponseIterator::new(cbor);
        assert_packet_eq(output, buf);
    }

    // TODO: Test CBOR with exactly 7609 bytes
    // TODO: Test CBOR with more that 7609 bytes

    #[test]
    fn init() {
        let mut nonce = [0; 8];
        random_bytes(&mut nonce);

        let channel_id = ChannelId::broadcast();
        let new_channel_id = ChannelId::random();
        let ir = InitResponse::new(new_channel_id, nonce);

        let mut buf = [0; 64];
        buf[0..4].copy_from_slice(&[255, 255, 255, 255]);
        buf[4] = 0x06_u8.bitor(0b1000_0000) as u8;
        buf[5..7].copy_from_slice(&[0, 17]);
        buf[7..15].copy_from_slice(&nonce);
        buf[15..19].copy_from_slice(&new_channel_id.to_bytes());
        buf[19] = 2;
        buf[20] = 1;
        buf[21] = 3;
        buf[22] = 7;
        buf[23] = 0b0000_1101;

        let output = init_response::<MockCtapHidPlatform>(channel_id, ir);
        assert_eq!(&output[..], &buf[..]);
    }

    // Test Error
    #[test]
    fn error() {
        let mut buf = [0; 64];
        let channel_id = ChannelId::random();

        buf[0..4].copy_from_slice(&channel_id.to_bytes());
        buf[4] = 0x3F_u8.bitor(0b1000_0000) as u8;
        buf[6] = 0x1;
        buf[7] = 0x3;

        let output = error_response(channel_id, HidError::InvalidLen);
        assert_eq!(&output[..], &buf[..]);
    }

    #[test]
    fn keepalive() {
        let mut buf = [0; 64];
        let channel_id = ChannelId::random();

        buf[0..4].copy_from_slice(&channel_id.to_bytes());
        buf[4] = 0x3B_u8.bitor(0b1000_0000) as u8;
        buf[6] = 0x1;
        buf[7] = 0x2;

        let output = keepalive_response(channel_id, KeepaliveResponse::UpNeeded).unwrap();
        assert_eq!(&output[..], &buf[..]);
    }

    #[test]
    fn wink() {
        let mut buf = [0; 64];
        let channel_id = ChannelId::random();

        buf[0..4].copy_from_slice(&channel_id.to_bytes());
        buf[4] = 0x08_u8.bitor(0b1000_0000) as u8;

        let output = wink_response(channel_id);
        assert_eq!(&output[..], &buf[..]);
    }

    #[test]
    fn lock() {
        let mut buf = [0; 64];
        let channel_id = ChannelId::random();

        buf[0..4].copy_from_slice(&channel_id.to_bytes());
        buf[4] = 0x04_u8.bitor(0b1000_0000) as u8;

        let output = lock_response(channel_id);
        assert_eq!(&output[..], &buf[..])
    }
}
