//! Implements the USBHID transport type.

// TODO: Mod level documentation and example usage

mod input;
mod messages;
mod output;

pub use self::input::TransactionProcessor;
pub use self::messages::ChannelId;

use bitflags::bitflags;

/// The capabilities which an authenticator may implement.
///
/// For more information, refer to chapter 8.1.9.1.3. of the
/// [Ctap specification](https://fidoalliance.org/specs/fido-v2.0-ps-20190130/fido-client-to-authenticator-protocol-v2.0-ps-20190130.html).
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CtapHidCapabilities {
    /// Specify whether this authenticator is capable of the wink protocol.
    ///
    /// If this is set to true, the wink function need to be overwritten.
    pub wink: bool,
    /// Specifies whether this authenticator implements the Cbor function, i.e. if this is a fido2
    /// capable authenticator.
    pub cbor: bool,
    /// Specifies whether this authenticator implements the Msg function, i.e. if this is a u2f
    /// capable authenticator.
    ///
    /// **U2F is currently unimplemented.**
    pub msg: bool,
}

impl Default for CtapHidCapabilities {
    fn default() -> Self {
        CtapHidCapabilities {
            wink: true,
            cbor: true,
            msg: false,
        }
    }
}

bitflags! {
    /// Capability flags
    pub(crate) struct CtapHidCapFlags: u8 {
        const NONE = 0b0000_0000;
        const WINK = 0b0000_0001;
        const CBOR = 0b0000_0100;
        const NOT_MSG = 0b0000_1000;
    }
}

impl From<CtapHidCapabilities> for CtapHidCapFlags {
    fn from(cap: CtapHidCapabilities) -> Self {
        let mut flags = CtapHidCapFlags::NONE;

        if cap.wink {
            flags |= CtapHidCapFlags::WINK;
        }
        if cap.cbor {
            flags |= CtapHidCapFlags::CBOR;
        }
        if !cap.msg {
            flags |= CtapHidCapFlags::NOT_MSG;
        }

        flags
    }
}

/// A Keepalive response. Refer to [`CtapHidPlatform`](trait.CtapHidPlatform.html) for usage.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum KeepaliveResponse {
    /// No Keepalive response
    None,
    /// The authenticator is processing a message
    Processing,
    /// The authenticator is waiting for the user presence check
    UpNeeded,
}

/// Implements the Platform dependent parts of the CtapHid protocol
pub trait CtapHidPlatform {
    /// Major device version number
    const MAJOR_VERSION: u8;

    /// Minor device version number
    const MINOR_VERSION: u8;

    /// Build device version number
    const BUILD_VERSION: u8;

    /// Specifies which capabilities an authenticator may have.
    const CAPABILITIES: CtapHidCapabilities;

    /// Called when the authenticator receives a `Wink` message.
    ///
    /// If the authenticator is capable, this function can be used to trigger a LED blink or similar.
    /// It defaults to doing nothing.
    /// The `Wink` response is sent automatically.
    fn wink(&mut self) {}

    /// Called when the authenticator receives a `Lock` message.
    ///
    /// It defaults to doing nothing.
    fn lock(&mut self) {}

    /// Called when the authenticator receives a `Cancel` message.
    ///
    /// The appropriate response is returned automatically.
    /// If nothing is done in this function, the behaviour is correct. However, it might be useful
    /// e.g. to abort User verification.
    ///
    /// It default to doing nothing.
    fn cancel(&mut self) {}

    /// Asks the platform if a keepalive is needed.
    ///
    /// This function gets called on every USB poll while the authenticator is processing.
    /// It is up to the platform to decide, (i.e. through a timer or by calling pools) if a
    /// `KeepaliveResponse` is necessary and what type is appropriate.
    /// If the platform is fast enough, no keepalive messages might be needed at all.
    ///
    /// The default implementation default to never sending keepalives.
    fn keepalive_needed(&mut self) -> KeepaliveResponse {
        KeepaliveResponse::None
    }

    /// Called whenever the `TransactionProcessor` starts receiving or transmitting a message.
    ///
    /// This can be used to start a timer mechanism which can be used in the `has_timed_out`
    /// function.
    ///
    /// Defaults to doing nothing.
    fn start_timer(&mut self) {}

    /// Asks the platform if a transaction has timed out.
    ///
    /// The default implementation never times out.
    fn has_timed_out(&mut self) -> bool {false}
}

#[cfg(test)]
mod test {
    use crate::usbhid::output::ResponseIterator;

    /// Compares the output of an [`OutputIterator`](crate::usbhid::OutputIterator)
    /// with a series of packets.
    pub(crate) fn assert_packet_eq<B: AsMut<[u8]>>(
        data: ResponseIterator<B>,
        packets: Vec<[u8; 64]>,
    ) {
        for (d, p) in data.zip(packets.iter()) {
            assert_eq!(&d as &[u8], p as &[u8])
        }
    }
}
