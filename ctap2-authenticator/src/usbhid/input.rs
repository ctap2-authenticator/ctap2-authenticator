use crate::error::CtapError;
use crate::messages::{Fido2Internal, Fido2Request, Fido2Response};
use crate::usbhid::messages::{ChannelId, HidCmd, HidError, InitResponse, MsgType, MAX_MSG_SIZE};
use crate::usbhid::output::{
    ctap_error_response, error_response, init_response, keepalive_response, lock_response,
    wink_response, ResponseIterator,
};
use crate::usbhid::{CtapHidPlatform, KeepaliveResponse};
use core::convert::TryInto;

pub type PollResult<B> = (Option<Fido2Request<B>>, Option<[u8; 64]>);

/// The internal state of the Parser.
#[derive(Debug)]
enum State<B: AsMut<[u8]>> {
    Ready,
    Receiving {
        channel_id: ChannelId,
        msg_type: MsgType,
        pkg_seq: u8,
        bytes_received: u16,
        bytes_left: u16,
    },
    Transmitting {
        output: Option<ResponseIterator<B>>,
    },
    Processing {
        channel_id: ChannelId,
    },
    Canceling {
        channel_id: ChannelId,
    },
}

#[derive(Debug)]
/// Parses USBHID packets into valid messages.
pub struct TransactionProcessor<B: AsMut<[u8]>, P: CtapHidPlatform> {
    state: State<B>,
    channel_counter: u32,
    platform: P,
    buf: Option<B>,
}

impl<B: AsMut<[u8]>, P: CtapHidPlatform> TransactionProcessor<B, P> {
    /// Creates a new `TransactionProcessor`.
    ///
    /// This `TransactionProcessor` can be driven by calling `poll` on it.
    pub fn new(buf: B, platform: P) -> Self {
        Self {
            state: State::Ready,
            channel_counter: 0,
            platform,
            buf: Some(buf),
        }
    }

    /// Return a response to the `TransactionProcessor`.
    ///
    /// The `TransactionProcessor` will return the provided
    /// [`Fido2Response`](../struct.Fido2Response.html) to the client during the subsequent polls.
    ///
    /// If the `TransactionProcessor` is not in the correct state, the response is simply ignored.
    pub fn response(&mut self, response: Fido2Response<B>) {
        debug_assert!(self.buf.is_none());

        match self.state {
            State::Processing { channel_id } => {
                // Check message and transition accordingly
                let resp_channel_id = match response.0 {
                    Fido2Internal::Probe { channel_id, .. } => channel_id,
                    Fido2Internal::U2F { channel_id, .. } => channel_id,
                    Fido2Internal::Ctap2 { channel_id, .. } => channel_id,
                };

                // Check that the channel id matches
                if channel_id != resp_channel_id {
                    log::warn!("channel id of processor and response are not matching");
                    return;
                }

                self.state = State::Transmitting {
                    output: Some(ResponseIterator::new(response)),
                }
            }

            // The request was cancelled, we just take the buffer and go back to ready state
            State::Canceling { channel_id } => {
                let (buf, resp_channel_id) = match response.0 {
                    Fido2Internal::U2F {
                        channel_id, buf, ..
                    } => (buf, channel_id),
                    Fido2Internal::Ctap2 {
                        channel_id, buf, ..
                    } => (buf, channel_id),
                    Fido2Internal::Probe {
                        channel_id, buf, ..
                    } => (buf, channel_id),
                };

                // Check that the channel id is matching
                if channel_id != resp_channel_id {
                    log::warn!("channel id of processor and response are not matching");
                    return;
                }

                self.buf = Some(buf);
                self.state = State::Ready;
            }
            _ => log::warn!("unexpected response is simply ignored"),
        }
    }

    // TODO: Proper documentation
    pub fn poll(&mut self, pkg: Option<&[u8; 64]>) -> PollResult<B> {
        match self.state {
            State::Ready { .. } => self.parse_ready(pkg),
            State::Receiving { .. } => self.parse_receiving(pkg),
            State::Processing { .. } => self.parse_processing_and_canceling(pkg),
            State::Canceling { .. } => self.parse_processing_and_canceling(pkg),
            State::Transmitting { .. } => self.parse_transmitting(pkg),
        }
    }

    fn parse_ready(&mut self, msg: Option<&[u8; 64]>) -> PollResult<B> {
        debug_assert!(self.buf.is_some());

        // Ignore empty poll. We don't have data waiting
        let msg = match msg {
            None => return (None, None),
            Some(msg) => msg,
        };

        let (channel_id, cmd, request_length) = match self.parse_initial_header(msg) {
            Ok(info) => info,
            Err(error) => return error,
        };

        match cmd {
            HidCmd::Probe => {
                self.handle_request_initial(channel_id, request_length, MsgType::Probe, &msg)
            }
            HidCmd::Msg => {
                self.handle_request_initial(channel_id, request_length, MsgType::Msg, &msg)
            }
            HidCmd::Cbor => {
                self.handle_request_initial(channel_id, request_length, MsgType::Cbor, &msg)
            }
            HidCmd::Ping => {
                self.handle_request_initial(channel_id, request_length, MsgType::Ping, &msg)
            }
            cmd => self.handle_default(cmd, channel_id, msg),
        }
    }

    fn parse_receiving(&mut self, msg: Option<&[u8; 64]>) -> PollResult<B> {
        debug_assert!(self.buf.is_some());

        match self.state {
            State::Receiving {
                channel_id,
                msg_type,
                pkg_seq,
                bytes_received,
                bytes_left,
            } => {
                // At this point we can check if the transaction has timed out
                if self.platform.has_timed_out() {
                    self.state = State::Ready;
                    return self.handle_error(channel_id, HidError::MsgTimeout);
                }

                // Check the header. If this is the beginning of a new transaction employ default handling
                if let Some(msg) = msg {
                    match self.parse_any_header(&msg) {
                        Ok(Some((recv_channel_id, cmd, _))) => {
                            return if cmd == HidCmd::Cancel {
                                // Handle a cancel case while receiving
                                self.platform.cancel();
                                self.state = State::Ready;

                                // Return the Keepalive cancel error
                                self.handle_ctap_error(recv_channel_id, CtapError::KeepaliveCancel)
                            } else {
                                self.handle_default(cmd, recv_channel_id, &msg)
                            };
                        }
                        Err(error) => return error,
                        Ok(None) => (),
                    }
                }

                // Ignore empty poll. We don't have data waiting
                let msg = match msg {
                    None => return (None, None),
                    Some(msg) => msg,
                };

                let buf = self.buf.as_mut().unwrap().as_mut();

                let packet_len = u16::min(59, bytes_left);
                buf[bytes_received as usize..(bytes_received + packet_len) as usize]
                    .copy_from_slice(&msg[5..(packet_len + 5) as usize]);

                if bytes_left - packet_len == 0 {
                    self.handle_request_final(channel_id, bytes_received + packet_len, msg_type)
                } else {
                    // NOTE: We are transitioning from Receiving to Receiving
                    // Thus, we don't need to start the timer here
                    self.state = State::Receiving {
                        channel_id,
                        msg_type,
                        pkg_seq: pkg_seq + 1,
                        bytes_received: bytes_received + packet_len,
                        bytes_left: bytes_left - packet_len,
                    };
                    (None, None)
                }
            }
            _ => core::panic!("matched on state other than receiving in parse_receive"),
        }
    }

    fn parse_processing_and_canceling(&mut self, msg: Option<&[u8; 64]>) -> PollResult<B> {
        let (channel_id, cancel) = match self.state {
            State::Processing { channel_id } => (channel_id, false),
            State::Canceling { channel_id } => (channel_id, true),
            _ => core::panic!(
                "not in state processing or canceling while calling \
            parse_processing_and_cancel",
            ),
        };

        // If the poll message was empty, we have the opportunity to send a keepalive if we need one
        match msg {
            None => match self.platform.keepalive_needed() {
                KeepaliveResponse::None => (None, None),
                kr => (None, keepalive_response(channel_id, kr)),
            },
            Some(msg) => {
                // Look at the message
                let (recv_channel_id, cmd, _) = match self.parse_initial_header(msg) {
                    Ok(info) => info,
                    Err(error) => return error,
                };

                // If we are in Processing, we need to check if we received a cancel
                if !cancel && recv_channel_id == channel_id && cmd == HidCmd::Cancel {
                    self.platform.cancel();
                    self.state = State::Canceling { channel_id };
                    // Return a keepalive cancel
                    self.handle_ctap_error(channel_id, CtapError::KeepaliveCancel)
                } else {
                    // Otherwise default handle
                    self.handle_default(cmd, recv_channel_id, &msg)
                }
            }
        }
    }

    fn parse_transmitting(&mut self, msg: Option<&[u8; 64]>) -> PollResult<B> {
        debug_assert!(self.buf.is_none());

        match self.state {
            State::Transmitting { ref mut output } => {
                debug_assert!(output.is_some());

                match msg {
                    // If the poll has a message, default handle it
                    Some(msg) => match self.parse_initial_header(msg) {
                        Ok((channel_id, cmd, _)) => self.handle_default(cmd, channel_id, msg),
                        Err(error) => error,
                    },

                    // Otherwise try to poll new data
                    None => {
                        let data = output.as_mut().unwrap().next();

                        if output.as_mut().unwrap().is_finished() {
                            self.buf = Some(output.take().unwrap().into_data());
                            self.state = State::Ready;
                        }

                        (None, data)
                    }
                }
            }
            _ => core::panic!("not in state transmitting while calling parse_transmitting"),
        }
    }

    /// Parses the header of the first packet
    /// Returns the channelId, the command and the length of the header on success
    /// Returns Error message on error
    fn parse_initial_header(
        &mut self,
        msg: &[u8; 64],
    ) -> Result<(ChannelId, HidCmd, u16), PollResult<B>> {
        let channel_id = ChannelId::from_slice(&msg[0..4]);

        // Parse Command
        let cmd = match HidCmd::from_u8(msg[4]) {
            Some(cmd) => cmd,
            None => return Err(self.handle_error(channel_id, HidError::InvalidCmd)),
        };

        if channel_id == ChannelId(0)
            || (channel_id == ChannelId::broadcast() && cmd != HidCmd::Init)
        {
            return Err(self.handle_error(channel_id, HidError::InvalidChannel));
        }

        let request_len = u16::from_be_bytes(msg[5..7].try_into().unwrap());

        // Check that length matches, if we have a const sized request
        if let Some(cmd_len) = cmd.get_request_len() {
            if request_len != cmd_len.into() {
                return Err(self.handle_error(channel_id, HidError::InvalidLen));
            }
        }

        Ok((channel_id, cmd, request_len))
    }

    /// Check the header. if it is an initial header it calls parse_initial_header.
    /// If it is a continuation header, it checks that we are in the correct state to parse it
    fn parse_any_header(
        &mut self,
        msg: &[u8; 64],
    ) -> Result<Option<(ChannelId, HidCmd, u16)>, PollResult<B>> {
        if msg[4] >= 127 {
            Ok(Some(self.parse_initial_header(msg)?))
        } else {
            let received_channel_id = ChannelId::from_slice(&msg[0..4]);

            match self.state {
                State::Receiving {
                    channel_id,
                    pkg_seq,
                    ..
                } => match (channel_id == received_channel_id, pkg_seq == msg[4]) {
                    (_, false) => {
                        self.state = State::Ready;
                        Err(self.handle_error(channel_id, HidError::InvalidSeq))
                    }
                    (false, _) => Err(self.handle_error(channel_id, HidError::InvalidChannel)),
                    (true, true) => Ok(None),
                },
                _ => {
                    // Ignore lost continuation packets
                    Err((None, None))
                }
            }
        }
    }

    /// Handles the initialization of a packet reception.
    /// Performs some checks on the input. Returns an error if checks fail.
    /// Transitions into receiving state
    fn handle_request_initial(
        &mut self,
        channel_id: ChannelId,
        request_len: u16,
        msg_type: MsgType,
        msg: &[u8; 64],
    ) -> PollResult<B> {
        // Check if the buffer is sufficient
        match &mut self.state {
            State::Ready => {
                let buf = self.buf.as_mut().unwrap().as_mut();
                // Check that the length is valid
                if request_len as usize > MAX_MSG_SIZE
                    || request_len as usize > buf.len()
                    || request_len == 0
                {
                    return self.handle_error(channel_id, HidError::InvalidLen);
                }

                // Copy the data
                let packet_len = u16::min(57, request_len);
                buf[0..packet_len as usize].copy_from_slice(&msg[7..(packet_len + 7) as usize]);

                // Check if we are finished and progress accordingly
                let bytes_left = request_len - packet_len;
                if bytes_left == 0 {
                    // Deliver packet to the upper layer and go into state Processing
                    self.handle_request_final(channel_id, request_len, msg_type)
                } else {
                    self.platform.start_timer();

                    // Update state
                    self.state = State::Receiving {
                        channel_id,
                        msg_type,
                        pkg_seq: 0,
                        bytes_received: packet_len,
                        bytes_left,
                    };
                    (None, None)
                }
            }
            _ => core::panic!("handle_fido2_first called in other state than Ready"),
        }
    }

    /// Finalizes and delivers the request
    fn handle_request_final(
        &mut self,
        channel_id: ChannelId,
        request_len: u16,
        msg_type: MsgType,
    ) -> PollResult<B> {
        let request = match msg_type {
            MsgType::Probe => Some(Fido2Request(Fido2Internal::Probe {
                channel_id,
                buf: self.buf.take().unwrap(),
                length: request_len,
            })),
            MsgType::Msg => Some(Fido2Request(Fido2Internal::U2F {
                channel_id,
                buf: self.buf.take().unwrap(),
                length: request_len,
            })),
            MsgType::Cbor => Some(Fido2Request(Fido2Internal::Ctap2 {
                channel_id,
                buf: self.buf.take().unwrap(),
                length: request_len,
            })),
            MsgType::Ping => None,
        };

        if request.is_some() {
            self.state = State::Processing { channel_id };
            (request, None)
        } else {
            self.state = State::Transmitting {
                output: Some(ResponseIterator::initialize(
                    channel_id,
                    self.buf.take().unwrap(),
                    request_len,
                    MsgType::Ping,
                )),
            };

            // We trigger the same behaviour as if we just have been polled
            self.parse_transmitting(None)
        }
    }

    /// Handles an init message
    /// Updates channel counter if necessary
    fn handle_init(&mut self, channel_id: ChannelId, nonce: [u8; 8]) -> PollResult<B> {
        if channel_id == ChannelId::broadcast() {
            self.channel_counter += 1;
            let response = InitResponse::new(ChannelId(self.channel_counter), nonce);
            (None, Some(init_response::<P>(channel_id, response)))
        } else {
            let response = InitResponse::new(channel_id, nonce);
            (None, Some(init_response::<P>(channel_id, response)))
        }
    }

    /// Return an error and return to Ready state
    /// In ready and processing state, the error is ignored because it doesnt make sense there
    fn handle_error(&mut self, channel_id: ChannelId, error: HidError) -> PollResult<B> {
        (None, Some(error_response(channel_id, error)))
    }

    /// Return an error and return to Ready state
    /// In ready and processing state, the error is ignored because it doesnt make sense there
    fn handle_ctap_error(&mut self, channel_id: ChannelId, error: CtapError) -> PollResult<B> {
        (None, Some(ctap_error_response(channel_id, error)))
    }

    /// This is a fallthrough which handles all messages in a generic way.
    /// Any behaviour special to a state of the processor should be caught beforehand.
    fn handle_default(
        &mut self,
        cmd: HidCmd,
        channel_id: ChannelId,
        msg: &[u8; 64],
    ) -> PollResult<B> {
        match cmd {
            // We assume that the processor is not in a state capable of handling
            // multi packet transactions by default
            HidCmd::Probe => self.handle_ctap_error(channel_id, CtapError::Processing),
            HidCmd::Msg => self.handle_ctap_error(channel_id, CtapError::Processing),
            HidCmd::Cbor => self.handle_ctap_error(channel_id, CtapError::Processing),
            HidCmd::Init => self.handle_init(channel_id, msg[7..15].try_into().unwrap()),
            HidCmd::Ping => self.handle_error(channel_id, HidError::ChannelBusy),
            // Cancel in Ready phase is ignored
            HidCmd::Cancel => (None, None),
            // These two should not be able to happen, behaves the same as a parse error
            HidCmd::Error => self.handle_error(channel_id, HidError::InvalidCmd),
            HidCmd::Keepalive => self.handle_error(channel_id, HidError::InvalidCmd),
            HidCmd::Wink => {
                self.platform.wink();
                (None, Some(wink_response(channel_id)))
            }
            HidCmd::Lock => {
                self.platform.lock();
                (None, Some(lock_response(channel_id)))
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test::{random_bytes, MockCtapHidPlatform};
    use crate::usbhid::ChannelId;
    use core::ops::BitOr;
    use core::panic;

    //    impl<'a> super::CtapHidRequestParser<'a> {
    //        /// Process the message further.
    //        ///
    //        /// # Returns
    //        /// Either the parser itself or a [`OutputIterator`] containing an error message.
    //        ///
    //        /// # Panics
    //        /// If the process returns unexpected something else then an error.
    //        pub fn try_process(self, msg: &[u8; 64]) -> Result<Self, CtapHidResponseIterator<'a>> {
    //            match self.process(msg) {
    //                CtapHidRequestParserResult::Processing(parser) => Ok(parser),
    //                CtapHidRequestParserResult::Error(output) => Err(output),
    //                _ => panic!("Did not expect the parsing to end here"),
    //            }
    //        }
    //    }
    //
    // Test Cbor Single Pkg
    #[test]
    fn cbor_short_with_keepalive() {
        let mut buf = vec![0; 8000];
        let mut parser = TransactionProcessor::new(&mut buf, MockCtapHidPlatform::new());
        let channel_id = ChannelId::random();
        let mut pkg = [0; 64];

        random_bytes(&mut pkg);
        pkg[0..4].copy_from_slice(&channel_id.to_bytes());
        pkg[4] = 0x10.bitor(0b1000_0000) as u8;
        pkg[5] = 0;
        pkg[6] = 40;

        let (request, output) = parser.poll(Some(&pkg));
        assert!(output.is_none());
        assert!(request.is_some());
        // TODO: Build a proper request test function and use it here
        // TODO: Check that we are correctly generating keepalives
    }
    //
    //    // Test Cbor Two Packets with exaxt fitting buffer
    //    #[test]
    //    fn cbor_medium() -> Result<(), ()> {
    //        let mut buf = [0; 80];
    //        let parser = CtapHidRequestParser::new(&mut buf);
    //        let channel_id = ChannelId::random();
    //
    //        let mut pkgs = vec![[0; 64]; 2];
    //        random_bytes(&mut pkgs[0]);
    //        random_bytes(&mut pkgs[1]);
    //
    //        pkgs[0][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[0][4] = 0x10.bitor(0b1000_0000) as u8;
    //        pkgs[0][5] = 0;
    //        pkgs[0][6] = 80;
    //
    //        pkgs[1][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[1][4] = 0;
    //
    //        // Parse first packet
    //        let parser = parser.try_process(&pkgs[0]).map_err(|_| ())?;
    //
    //        // Parse the second packet
    //        match parser.process(&pkgs[1]) {
    //            CtapHidRequestParserResult::Cbor(my_channel_id, cbor) => {
    //                assert_eq!(my_channel_id, channel_id);
    //                assert_eq!(cbor.0[0..57], pkgs[0][7..64]);
    //                assert_eq!(cbor.0[57..80], pkgs[1][5..28]);
    //            }
    //            other => panic!("Expected Cbor but got {:?}", other),
    //        }
    //        Ok(())
    //    }
    //
    //    // Test CBOR with three packets
    //    #[test]
    //    fn cbor_long() -> Result<(), ()> {
    //        let mut buf = [0; 6000];
    //        let parser = CtapHidRequestParser::new(&mut buf);
    //        let channel_id = ChannelId::random();
    //
    //        let mut pkgs = vec![[0; 64]; 3];
    //        random_bytes(&mut pkgs[0]);
    //        random_bytes(&mut pkgs[1]);
    //        random_bytes(&mut pkgs[2]);
    //
    //        pkgs[0][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[0][4] = 0x10.bitor(0b1000_0000) as u8;
    //        pkgs[0][5] = 0;
    //        pkgs[0][6] = 120;
    //
    //        pkgs[1][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[1][4] = 0;
    //        pkgs[2][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[2][4] = 1;
    //
    //        // Parse first two packets
    //        let parser = parser.try_process(&pkgs[0]).map_err(|_| ())?;
    //        let parser = parser.try_process(&pkgs[1]).map_err(|_| ())?;
    //
    //        // Parse the third packet
    //        match parser.process(&pkgs[2]) {
    //            CtapHidRequestParserResult::Cbor(my_channel_id, cbor) => {
    //                assert_eq!(my_channel_id, channel_id);
    //                assert_eq!(cbor.0[0..57], pkgs[0][7..64]);
    //                assert_eq!(cbor.0[57..116], pkgs[1][5..64]);
    //                assert_eq!(cbor.0[116..120], pkgs[2][5..9]);
    //            }
    //            other => panic!("Expected Cbor but got {:?}", other),
    //        }
    //        Ok(())
    //    }
    //
    //    // TODO: Test Cbor 7609 bytes long
    //    // TODO: Test Cbor longer than 7609 bytes
    //
    //    // Test Cbor with too short buffer
    //    #[test]
    //    fn cbor_buf_to_short() {
    //        let mut buf = [0; 79];
    //        let parser = CtapHidRequestParser::new(&mut buf);
    //        let channel_id = ChannelId::random();
    //
    //        let mut pkgs = vec![[0; 64]; 2];
    //        random_bytes(&mut pkgs[0]);
    //        random_bytes(&mut pkgs[1]);
    //
    //        pkgs[0][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[0][4] = 0x10.bitor(0b1000_0000) as u8;
    //        pkgs[0][5] = 0;
    //        pkgs[0][6] = 80;
    //
    //        pkgs[1][0..4].copy_from_slice(&channel_id.to_bytes());
    //        pkgs[1][4] = 0;
    //
    //        let mut error = [0; 64];
    //        error[0..4].copy_from_slice(&channel_id.to_bytes());
    //        error[4] = 0x3f.bitor(0b1000_0000) as u8;
    //        error[5] = 0;
    //        error[6] = 1;
    //        error[7] = 0x7F;
    //
    //        // Parse first packet
    //        match parser.process(&pkgs[0]) {
    //            CtapHidRequestParserResult::Error(err) => {
    //                assert_packet_eq(err, vec![error]);
    //            }
    //            other => panic!("Expected not enough buffer error but got {:?}", other),
    //        };
    //    }

    // Test Wink
    #[test]
    fn wink() {
        let mut buf = vec![0; 1000];
        let mut parser = TransactionProcessor::new(&mut buf, MockCtapHidPlatform::new());
        let channel_id = ChannelId::random();
        let mut pkg = [0; 64];

        pkg[0..4].copy_from_slice(&channel_id.to_bytes());
        pkg[4] = 0x08.bitor(0b1000_0000) as u8;

        // Parse the packet
        let (request, output) = parser.poll(Some(&pkg));
        assert!(request.is_none());
        assert!(parser.platform.winked);
        assert_eq!(output.unwrap()[..], pkg[..]);
    }

    // Test Init
    #[test]
    fn init() {
        let mut buf = vec![0; 1000];
        let mut parser = TransactionProcessor::new(&mut buf, MockCtapHidPlatform::new());
        let mut nonce = [0; 8];
        random_bytes(&mut nonce);

        let mut pkg = [0; 64];

        pkg[0..4].copy_from_slice(&ChannelId::broadcast().to_bytes());
        pkg[4] = 0x06.bitor(0b1000_0000) as u8;
        pkg[6] = 0x08;
        pkg[7..15].copy_from_slice(&nonce);

        // Parse the packet
        let (request, output) = parser.poll(Some(&pkg));
        assert!(request.is_none());

        let pkg = output.unwrap();
        assert_eq!(&pkg[0..4], &[0xff; 4]);
        assert_eq!(pkg[4], 0x06.bitor(0b1000_0000) as u8);
        assert_eq!(pkg[6], 0x11);
        assert_eq!(pkg[19], 0x2);
        assert_eq!(pkg[20], 0x1);
        assert_eq!(pkg[21], 0x3);
        assert_eq!(pkg[22], 0x7);
        assert_eq!(pkg[23], 0b0000_1101);
    }

    // TODO: Test Ping One packet
    // TODO: Test Ping More than one packet
    // TODO: Test Ping with 7609 bytes
}
