use crate::usbhid::{CtapHidCapFlags, CtapHidPlatform};
use core::convert::TryInto;
use core::ops::{BitAnd, BitOr};

pub(crate) const MAX_MSG_SIZE: usize = 7609;
pub(crate) type Nonce = [u8; 8];

/// The message type which the Parser is currently parsing
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum MsgType {
    Msg,
    Cbor,
    Ping,
    Probe,
}

/// Used to differentiate the the transactions from different client applications.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct ChannelId(pub(crate) u32);

impl ChannelId {
    /// Returns the broadcast `ChannelId`.
    pub fn broadcast() -> ChannelId {
        Self(0xffff_ffff)
    }

    pub(crate) fn to_bytes(self) -> [u8; 4] {
        self.0.to_be_bytes()
    }

    pub(crate) fn from_slice(slice: &[u8]) -> Self {
        debug_assert_eq!(slice.len(), 4);
        Self(u32::from_be_bytes(slice[0..4].try_into().unwrap()))
    }
}

#[derive(Debug, Clone)]
struct InitRequest {
    nonce: [u8; 8],
}

#[derive(Debug, Clone)]
pub(crate) struct InitResponse {
    nonce: Nonce,
    channel_id: ChannelId,
}

impl InitResponse {
    pub(crate) fn new(channel_id: ChannelId, nonce: Nonce) -> Self {
        Self { nonce, channel_id }
    }
}

impl InitResponse {
    pub(crate) fn to_buf<P: CtapHidPlatform>(&self, buf: &mut [u8]) {
        buf[0..8].copy_from_slice(&self.nonce);
        buf[8..12].copy_from_slice(&self.channel_id.to_bytes());
        // CTAP Version Number
        buf[12] = 2;

        // Device Version Number
        buf[13] = P::MAJOR_VERSION;
        buf[14] = P::MINOR_VERSION;
        buf[15] = P::BUILD_VERSION;

        let caps: CtapHidCapFlags = P::CAPABILITIES.into();
        buf[16] = caps.bits();
    }
}

/// A CTAP HID Command as defined in SPEC 8.1.9
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum HidCmd {
    Msg = 0x03,
    Cbor = 0x10,
    Init = 0x06,
    Ping = 0x01,
    Cancel = 0x11,
    Error = 0x3f,
    Keepalive = 0x3b,
    Wink = 0x08,
    Lock = 0x04,
    Probe = 0x5b,
}

impl HidCmd {
    pub(crate) fn to_u8(self) -> u8 {
        use HidCmd::*;
        let r: u8 = match self {
            Msg => 0x03,
            Cbor => 0x10,
            Init => 0x06,
            Ping => 0x01,
            Cancel => 0x11,
            Error => 0x3f,
            Keepalive => 0x3b,
            Wink => 0x08,
            Lock => 0x04,
            Probe => 0x5b
        };
        r.bitor(0b1000_0000)
    }

    pub(crate) fn from_u8(v: u8) -> Option<Self> {
        use HidCmd::*;

        if v < 128 {
            return None;
        }

        match v.bitand(0b0111_1111) {
            0x03 => Some(Msg),
            0x10 => Some(Cbor),
            0x06 => Some(Init),
            0x01 => Some(Ping),
            0x11 => Some(Cancel),
            0x3f => Some(Error),
            0x3b => Some(Keepalive),
            0x08 => Some(Wink),
            0x04 => Some(Lock),
            0x5b => Some(Probe),
            _ => None,
        }
    }

    /// Returns the length of the request, if the response has a fixed length and none otherwise
    pub(crate) fn get_request_len(self) -> Option<u8> {
        use HidCmd::*;
        match self {
            Init => Some(8),
            Cancel | Wink => Some(0),
            Lock => Some(1),
            // Not constant
            Msg | Cbor | Ping | Probe => None,
            // Response only
            Error | Keepalive => None,
        }
    }
}

/// A CTAP HID Error, as defined in SPEC 8.1.9.1.6
#[repr(u8)]
#[derive(Debug, Clone)]
#[allow(dead_code)]
pub(crate) enum HidError {
    InvalidCmd = 0x01,
    InvalidPar = 0x02,
    InvalidLen = 0x03,
    InvalidSeq = 0x04,
    MsgTimeout = 0x05,
    ChannelBusy = 0x6,
    LockRequired = 0x0A,
    InvalidChannel = 0x0B,
    Other = 0x7F,
}

impl HidError {
    pub(crate) fn to_u8(&self) -> u8 {
        use HidError::*;
        match self {
            InvalidCmd => 0x01,
            InvalidPar => 0x02,
            InvalidLen => 0x03,
            InvalidSeq => 0x04,
            MsgTimeout => 0x05,
            ChannelBusy => 0x6,
            LockRequired => 0x0A,
            InvalidChannel => 0x0B,
            Other => 0x7F,
        }
    }
}

#[cfg(test)]
mod test {
    use rand::{random, thread_rng, Rng};

    impl super::ChannelId {
        /// Generate a random channel id
        pub(crate) fn random() -> Self {
            Self(random())
        }
    }

    impl super::HidError {
        /// Generate a random error.
        pub(crate) fn random() -> Self {
            use super::HidError::*;
            match thread_rng().gen_range(0, 9) {
                0 => InvalidCmd,
                1 => InvalidPar,
                2 => InvalidLen,
                3 => InvalidSeq,
                4 => MsgTimeout,
                5 => ChannelBusy,
                6 => LockRequired,
                7 => InvalidChannel,
                8 => Other,
                _ => core::panic!(),
            }
        }
    }
}
