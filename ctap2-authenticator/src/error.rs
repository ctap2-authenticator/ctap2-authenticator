use bitflags::_core::fmt::{Error, Formatter};
use core::fmt::Display;

/// Represents a CTAP2 error as specified by CTAP specification 6.3.
// NOTE: We don't need the Success state, as it will be represented by a response
#[repr(u8)]
#[derive(Clone, Debug)]
#[allow(dead_code)]
pub(crate) enum CtapError {
    InvalidCommand,
    InvalidParameter,
    InvalidLength,
    InvalidSeq,
    Timeout,
    ChannelBusy,
    LockRequired,
    InvalidChannel,
    CborUnexpectedType,
    InvalidCbor,
    MissingParameter,
    LimitExceeded,
    UnsupportedExtension,
    CredentialExcluded,
    Processing,
    InvalidCredential,
    UserActionPending,
    OperationPending,
    NoOperations,
    UnsupportedAlgorithm,
    OperationDenied,
    KeyStoreFull,
    NoOperationPending,
    UnsupportedOption,
    InvalidOption,
    KeepaliveCancel,
    NoCredentials,
    UserActionTimeout,
    NotAllowed,
    PinInvalid,
    PinBlocked,
    PinAuthInvalid,
    PinAuthBlocked,
    PinNotSet,
    PinRequired,
    PinPolicyViolation,
    PinTokenExpired,
    RequestTooLarge,
    ActionTimeout,
    UpRequired,
    Other,
}

impl CtapError {
    /// Returns the error code as specified by CTAP2 specification
    pub(crate) fn to_u8(&self) -> u8 {
        use CtapError::*;
        match self {
            InvalidCommand => 0x01,
            InvalidParameter => 0x02,
            InvalidLength => 0x03,
            InvalidSeq => 0x04,
            Timeout => 0x05,
            ChannelBusy => 0x06,
            LockRequired => 0x0a,
            InvalidChannel => 0x0b,
            CborUnexpectedType => 0x11,
            InvalidCbor => 0x12,
            MissingParameter => 0x14,
            LimitExceeded => 0x15,
            UnsupportedExtension => 0x16,
            CredentialExcluded => 0x19,
            Processing => 0x21,
            InvalidCredential => 0x22,
            UserActionPending => 0x23,
            OperationPending => 0x24,
            NoOperations => 0x25,
            UnsupportedAlgorithm => 0x26,
            OperationDenied => 0x27,
            KeyStoreFull => 0x28,
            NoOperationPending => 0x2a,
            UnsupportedOption => 0x2b,
            InvalidOption => 0x2c,
            KeepaliveCancel => 0x2d,
            NoCredentials => 0x2e,
            UserActionTimeout => 0x2f,
            NotAllowed => 0x30,
            PinInvalid => 0x31,
            PinBlocked => 0x32,
            PinAuthInvalid => 0x33,
            PinAuthBlocked => 0x34,
            PinNotSet => 0x35,
            PinRequired => 0x36,
            PinPolicyViolation => 0x37,
            PinTokenExpired => 0x38,
            RequestTooLarge => 0x39,
            ActionTimeout => 0x3a,
            UpRequired => 0x3b,
            Other => 0x7f,
        }
    }
}

impl Display for CtapError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{:?}", self)?;
        Ok(())
    }
}
