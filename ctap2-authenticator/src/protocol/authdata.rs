use crate::bytes::Bytes;
use crate::error::CtapError;
use crate::protocol::key::PublicKey;
use crate::AuthenticatorPlatform;
use bitflags::bitflags;
use core::ops::BitOr;
use serde::Serialize;
use serde_cbor::ser::SliceWrite;
use serde_cbor::Serializer;

/// The `AttestedCredentialData` object as specified by the WebAuthn specification 6.4.1
// UNIMPLEMENTED: Implement AuthData extensions
#[derive(Debug, PartialEq, Eq)]
pub(crate) struct AttestedCredential<P: AuthenticatorPlatform> {
    pub credential_id: P::CredentialId,
    pub public_key: PublicKey<P::PublicKeyBuffer>,
}

impl<P: AuthenticatorPlatform> AttestedCredential<P> {
    pub(crate) fn to_slice(&self, data: &mut [u8]) -> Result<usize, CtapError> {
        // Check that length is sufficient
        // NOTE: This can not check that there is sufficient space for the COSE Key
        let credential_id_len = self.credential_id.as_ref().len();
        if data.len() < 18 + credential_id_len {
            return Err(CtapError::LimitExceeded);
        }

        // Serialize the COSE Key. We do this first because it can fail if the slice is to short
        // We also remember how many bytes we have written, as it is important for the slicing
        let cose_length = {
            let mut writer = SliceWrite::new(&mut data[18 + credential_id_len..]);
            let mut ser = Serializer::new(&mut writer);
            self.public_key
                .serialize(&mut ser)
                .map_err(|_| CtapError::LimitExceeded)?;
            writer.bytes_written()
        };

        // Write the rest of the data into the slice according to WebAuthn 6.1
        data[0..16].copy_from_slice(&P::AAGUID);
        data[16..18].copy_from_slice(&(credential_id_len as u16).to_be_bytes());
        data[18..18 + credential_id_len].copy_from_slice(self.credential_id.as_ref());

        Ok(18 + credential_id_len + cose_length)
    }
}

/// The `AuthData` object as specified by the WebAuthn specification.
#[derive(Debug, PartialEq, Eq)]
pub(crate) struct AuthenticatorData<P: AuthenticatorPlatform> {
    pub rp_id_hash: Bytes<[u8; 32]>,
    pub flags: AuthDataFlags,
    pub sig_count: u32,
    pub attested_credential: Option<AttestedCredential<P>>,
}

impl<P: AuthenticatorPlatform> AuthenticatorData<P> {
    pub(crate) fn to_slice(&self, data: &mut [u8]) -> Result<usize, CtapError> {
        // Check that length is sufficient
        if data.len() < 37 {
            return Err(CtapError::LimitExceeded);
        }

        // Write the data into the slice according to WebAuthn 6.1
        data[0..32].copy_from_slice(self.rp_id_hash.as_ref());
        data[32] = self.flags.bits;
        data[33..37].copy_from_slice(&self.sig_count.to_be_bytes());

        // Write the AttestedCredentialData into the array if it exists
        let attested_length = if let Some(ref attested_credential) = self.attested_credential {
            attested_credential.to_slice(&mut data[37..])?
        } else {
            0
        };

        Ok(37 + attested_length)
    }

    /// Similar as `to_slice` however, it generates the
    /// fitting CBOR header such that it can be pasted as a CBOR entry somewhere.
    /// On success, this returns a tuple of the total length of the structure and the length of
    /// the CBOR header. This is important because the `attest` and `sign` functions must not sign
    /// over the header.
    pub(crate) fn to_slice_with_header(&self, data: &mut [u8]) -> Result<(usize, usize), CtapError> {
        const BYTE_TYPE: u8 = 0b0100_0000;
        // The value length is at least 37 bytes and at most 7609 bytes. Thus its length can be
        // represented with u8 or u16 but never inline

        // If no attested credential, the length is always 37 bytes plus
        // two header bytes, the type and the length
        if self.attested_credential.is_none() {
            data[0] = 24_u8.bitor(BYTE_TYPE);
            data[1] = 37;
            let length = self.to_slice(&mut data[2..])?;
            debug_assert!(length == 37);
            return Ok((39, 2));
        }

        // Otherwise we assume it is longer than 255 bytes and copy it afterwards if not
        let length = self.to_slice(&mut data[3..])?;
        if length > 255 {
            data[0] = 25_u8.bitor(BYTE_TYPE);
            data[1..3].copy_from_slice(&(length as u16).to_be_bytes());
            Ok((3 + length, 2))
        } else {
            data[0] = 24_u8.bitor(BYTE_TYPE);
            data[1] = length as u8;

            // Move all data one to the left
            for x in 2..length + 3 {
                data[x] = data[x + 1];
            }

            Ok((2 + length, 2))
        }
    }
}

bitflags! {
    /// The Flags of an AuthData object as
    /// specified bz the WebAuthn specification.
    pub(crate) struct AuthDataFlags: u8 {
        const NONE = 0b0000_0000;
        const UP = 0b0000_0001;
        const UV = 0b0000_0100;
        const AT = 0b0100_0000;
        const ED = 0b1000_0000;
    }
}

#[cfg(test)]
pub(crate) mod test {
    use super::AuthDataFlags as Flags;
    use crate::bytes::Bytes;
    use crate::protocol::authdata::{AttestedCredential, AuthenticatorData};
    use crate::protocol::key::test::cose_ed25519_testkey;
    use crate::protocol::key::PublicKey;
    use crate::test::MockCtapPlatform;

    /// Some test data for [`Authdata`](crate::authdata::AuthData)
    pub(crate) const TEST_AUTHDATA: [u8; 71] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e,
        0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d,
        0x2e, 0x2f, // rpidHash
        0xc5, // AuthDataFlags
        0x00, 0x00, 0x13, 0x37, // SigCount,
        0xf8, 0xa0, 0x11, 0xf3, 0x8c, 0x0a, 0x4d, 0x15, 0x80, 0x06, 0x17, 0x11, 0x1f, 0x9e, 0xdc,
        0x7d, // aaguid
        0x00, 0x10, // length
        0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e,
        0x4f, // credential_id
    ];

    const TEST_RPID_HASH: [u8; 32] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e,
        0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d,
        0x2e, 0x2f,
    ];

    const TEST_CREDENTIAL_ID: [u8; 16] = [
        0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e,
        0x4f,
    ];

    pub(crate) const TEST_ECC_X: [u8; 32] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e,
        0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d,
        0x2e, 0x2f,
    ];

    pub(crate) const TEST_ECC_Y: [u8; 32] = [
        0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e,
        0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d,
        0x4e, 0x4f,
    ];

    #[test]
    fn to_slice() {
        let mut data = Vec::new();
        data.extend_from_slice(&TEST_AUTHDATA);
        data.append(&mut cose_ed25519_testkey());

        let auth_data: AuthenticatorData<MockCtapPlatform> = AuthenticatorData {
            rp_id_hash: Bytes::from(TEST_RPID_HASH),
            flags: Flags::AT | Flags::ED | Flags::UV | Flags::UP,
            sig_count: 0x1337,
            attested_credential: Some(AttestedCredential {
                credential_id: TEST_CREDENTIAL_ID.to_vec(),
                public_key: PublicKey::ed25519(TEST_ECC_X, TEST_ECC_Y),
            }),
        };

        let mut buffer = [0; 160];
        let bytes_written = auth_data.to_slice(&mut buffer).unwrap();
        assert_eq!(&buffer[..bytes_written], &data[..]);
    }

    #[test]
    fn flags() {
        let flag = Flags::from_bits(0b1100_0101).unwrap();
        assert_eq!(flag, (Flags::AT | Flags::ED | Flags::UV | Flags::UP))
    }
}
