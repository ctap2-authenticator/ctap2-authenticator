use crate::bytes::Bytes;
use serde::de::{Error, MapAccess, SeqAccess, Visitor};
use serde::export::Formatter;
use serde::ser::SerializeMap;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

// TODO: Move this into a more fitting place
/// `A PublicKeyParam` is passed as the fourth Argument in a `MakeCredentialRequest`
#[derive(Debug, Clone, PartialEq, Eq)]
enum PublicKeyParam {
    ES256,
    Other,
}

struct PublicKeyParamVisitor;
impl<'de> Visitor<'de> for PublicKeyParamVisitor {
    type Value = PublicKeyParam;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str(
            "a map with strings as keys and signed integers and \
            strings as values",
        )
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        //let mut param = PublicKeyParam::Other;
        let mut param: Option<PublicKeyParam> = None;
        let mut typ: Option<&'de str> = None;

        while let Some(key) = access.next_key()? {
            match key {
                "alg" => match access.next_value()? {
                    -7 => param = Some(PublicKeyParam::ES256),
                    _ => param = Some(PublicKeyParam::Other),
                },
                "type" => typ = Some(access.next_value()?),
                a => return Err(<M::Error as Error>::unknown_field(a, &["alg", "type"])),
            };
        }

        let param = match param {
            Some(param) => param,
            None => return Err(<M::Error as Error>::missing_field("alg")),
        };

        match typ {
            Some("public-key") => Ok(param),
            Some(_) => Ok(PublicKeyParam::Other),
            None => Err(<M::Error as Error>::missing_field("type")),
        }
    }
}

impl<'de> Deserialize<'de> for PublicKeyParam {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(PublicKeyParamVisitor)
    }
}

/// Contains information about which algorithms the relying party permits to use in.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PublicKeyParams {
    pub es256: bool,
}

impl Default for PublicKeyParams {
    fn default() -> Self {
        PublicKeyParams { es256: false }
    }
}

struct PublicKeyParamsVisitor;
impl<'de> Visitor<'de> for PublicKeyParamsVisitor {
    type Value = PublicKeyParams;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a byte string")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'de>,
    {
        let mut params = PublicKeyParams::default();
        while let Some(param) = seq.next_element()? {
            match param {
                PublicKeyParam::ES256 => params.es256 = true,
                _ => {
                    // We skip all the `Other` KeyParams here
                    continue;
                }
            }
        }

        Ok(params)
    }
}

impl<'de> Deserialize<'de> for PublicKeyParams {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_any(PublicKeyParamsVisitor)
    }
}

/// A `PublicKey` which can be used to verify a signature.
///
/// At the moment, this type can only represent either a `NIST-P256` key or a `Ed29915` key.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PublicKey<B>(InternalPublicKey<B>);

#[derive(Debug, Clone, PartialEq, Eq)]
enum InternalPublicKey<B> {
    NistP256 { x: Bytes<B>, y: Bytes<B> },
    Ed25519 { x: Bytes<B>, y: Bytes<B> },
    // UNIMPLEMENTED: Support for other types of Keys
}

impl<B: AsRef<[u8]>> PublicKey<B> {
    // TODO: Check the length of the buffers and reject wrong sized buffers
    /// Create a key based on a `NIST-P256` curve.
    ///
    /// # Arguments
    /// - `x`: The x-coordinate of the curve
    /// - `y`: The y-coordinate of the curve
    ///
    /// # Returns
    /// The `PublicKey`
    pub fn nistp256(x: B, y: B) -> Self {
        Self(InternalPublicKey::NistP256 {
            x: Bytes::from(x),
            y: Bytes::from(y),
        })
    }

    /// Create a key based on the `Ed29915` curve.
    ///
    /// # Arguments
    /// - `x`: The x-coordinate of the curve
    /// - `y`: The y-coordinate of the curve
    ///
    /// # Returns
    /// The `PublicKey
    pub fn ed25519(x: B, y: B) -> Self {
        Self(InternalPublicKey::Ed25519 {
            x: Bytes::from(x),
            y: Bytes::from(y),
        })
    }
}

impl<B> Serialize for PublicKey<B>
where
    B: AsRef<[u8]>,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(5))?;

        // We only support ECDSA with SHA256 for now
        map.serialize_entry(&1, &2)?;
        map.serialize_entry(&3, &-7)?;

        match &self.0 {
            InternalPublicKey::NistP256 { x, y } => {
                map.serialize_entry(&-1, &1)?;
                map.serialize_entry(&-2, &Bytes::from(&x[..32]))?;
                map.serialize_entry(&-3, &Bytes::from(&y[..32]))?;
            }
            InternalPublicKey::Ed25519 { x, y } => {
                map.serialize_entry(&-1, &6)?;
                map.serialize_entry(&-2, &Bytes::from(&x[..32]))?;
                map.serialize_entry(&-3, &Bytes::from(&y[..32]))?;
            }
        }

        map.end()
    }
}

struct PublicKeyVisitor;
impl<'de> Visitor<'de> for PublicKeyVisitor {
    type Value = PublicKey<&'de [u8]>;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str(
            "a map with signed integers as \
            keys and signed integers and byte \
            strings as values",
        )
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let mut key_type: Option<i16> = None;
        let mut alg: Option<i16> = None;
        let mut curve_type: Option<i16> = None;

        let mut x: Option<&'de [u8]> = None;
        let mut y: Option<&'de [u8]> = None;

        while let Some(key) = access.next_key()? {
            match key {
                1 => key_type = Some(access.next_value()?),
                3 => alg = Some(access.next_value()?),
                -1 => curve_type = Some(access.next_value()?),
                -2 => x = Some(access.next_value()?),
                -3 => y = Some(access.next_value()?),
                a => {
                    return Err(<M::Error as Error>::custom(format_args!(
                        "the COSE key {} is not supported",
                        a
                    )))
                }
            };
        }

        // Check that we have all the fields we need
        if let (Some(2), Some(-7), Some(curve_type), Some(x), Some(y)) =
            (key_type, alg, curve_type, x, y)
        {
            match curve_type {
                // Create the corresponding public key
                1 => Ok(PublicKey::nistp256(x, y)),
                6 => Ok(PublicKey::ed25519(x, y)),
                a => Err(<M::Error as Error>::custom(format_args!(
                    "Curve type {} is not supported",
                    a
                ))),
            }
        } else {
            Err(<M::Error as Error>::missing_field(
                "the COSE key is missing a required field",
            ))
        }
    }
}

impl<'de> Deserialize<'de> for PublicKey<&'de [u8]> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(PublicKeyVisitor)
    }
}

#[cfg(test)]
pub(crate) mod test {
    use crate::protocol::key::{PublicKey, PublicKeyParam, PublicKeyParams};

    impl super::PublicKeyParams {
        pub(crate) fn es256() -> Self {
            PublicKeyParams { es256: true }
        }
    }

    const TEST_ECC_X: [u8; 32] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e,
        0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d,
        0x2e, 0x2f,
    ];

    const TEST_ECC_Y: [u8; 32] = [
        0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e,
        0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d,
        0x4e, 0x4f,
    ];

    /// A TestKey of type NistP256
    pub(crate) fn cose_nistp256_testkey() -> Vec<u8> {
        vec![
            0xa5, // map(5)
            0x01, // KeyType(1)
            0x02, // ECC(2),
            0x03, // Algorithm(3)
            0x26, // ECDSA_with_ SHA256(-7)
            0x20, // CurveType(-1)
            0x01, // NistP256(1)
            0x21, // X(-2),
            0x58, 0x20, // bytes(32)
            // x-coordinate
            0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
            0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b,
            0x2c, 0x2d, 0x2e, 0x2f, //
            0x22, // Y(-3),
            0x58, 0x20, // bytes(32)
            // y-coordinate
            0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d,
            0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b,
            0x4c, 0x4d, 0x4e, 0x4f,
        ]
    }

    /// A TestKey of type Ed25519
    pub(crate) fn cose_ed25519_testkey() -> Vec<u8> {
        vec![
            0xa5, // map(5)
            0x01, // KeyType(1)
            0x02, // ECC(2),
            0x03, // Algorithm(3)
            0x26, // ECDSA_with_ SHA256(-7)
            0x20, // CurveType(-1)
            0x06, // Ed25519(6)
            0x21, // X(-2),
            0x58, 0x20, // bytes(16)
            // x-coordinate
            0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
            0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b,
            0x2c, 0x2d, 0x2e, 0x2f, //
            0x22, // Y(-3),
            0x58, 0x20, // bytes(16)
            // y-coordinate
            0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d,
            0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b,
            0x4c, 0x4d, 0x4e, 0x4f,
        ]
    }

    const TEST_PUBLIC_KEY_PARAM: [u8; 22] = [
        0xa2, // map(2)
        0x63, // text(3)
        0x61, 0x6c, 0x67, // "alg"
        0x26, // -7 (ES256)
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
    ];

    const TEST_PUBLIC_KEY_PARAMS: [u8; 47] = [
        0x82, // array(2)
        0xa2, // map(2)
        0x63, // text(3)
        0x61, 0x6c, 0x67, // "alg"
        0x26, // -7 (ES256)
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0xa2, // map(2)
        0x63, // text(3)
        0x61, 0x6c, 0x67, // "alg"
        0x39, 0x01, 0x00, // -257 (RS256)
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
    ];

    #[test]
    fn serialize_nistp256() {
        let key = PublicKey::nistp256(TEST_ECC_X, TEST_ECC_Y);
        assert_eq!(&serde_cbor::to_vec(&key).unwrap(), &cose_nistp256_testkey());
    }

    #[test]
    fn deserialize_nistp256() {
        let bytes = cose_nistp256_testkey();
        let parsed_key: PublicKey<&[u8]> = serde_cbor::from_slice(&bytes[..]).unwrap();
        let key = PublicKey::nistp256(&TEST_ECC_X[..], &TEST_ECC_Y[..]);
        assert_eq!(parsed_key, key);
    }

    #[test]
    fn serialize_ed25519() {
        let key = PublicKey::ed25519(&TEST_ECC_X, &TEST_ECC_Y);
        assert_eq!(&serde_cbor::to_vec(&key).unwrap(), &cose_ed25519_testkey());
    }

    #[test]
    fn deserialize_ed25519() {
        let bytes = cose_ed25519_testkey();
        let parsed_key: PublicKey<&[u8]> = serde_cbor::from_slice(&bytes[..]).unwrap();
        let key = PublicKey::ed25519(&TEST_ECC_X[..], &TEST_ECC_Y[..]);
        assert_eq!(parsed_key, key);
    }

    #[test]
    fn deserialize_public_key_param() {
        let parsed_param: PublicKeyParam = serde_cbor::from_slice(&TEST_PUBLIC_KEY_PARAM).unwrap();
        let param = PublicKeyParam::ES256;
        assert_eq!(parsed_param, param);
    }

    #[test]
    fn deserialize_public_key_params() {
        let params: PublicKeyParams = serde_cbor::from_slice(&TEST_PUBLIC_KEY_PARAMS).unwrap();
        assert_eq!(params.es256, true);
    }
}
