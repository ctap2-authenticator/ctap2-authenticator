use crate::bytes::Bytes;
use crate::error::CtapError;
use crate::protocol::entity::{CtapOptions, Entity};
use crate::protocol::extensions::ExtensionStub;
use crate::protocol::key::PublicKeyParams;
use crate::CredentialDescriptorList;
use bitflags::_core::fmt::Formatter;
use serde::de::{Error, MapAccess, Visitor};
use serde::{Deserialize, Deserializer};
use serde_cbor::de::SliceReadFixed;

/// Contains the data, which was passed via a `MakeCredentialRequest`
// UNIMPLEMENTED: Extensions
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct MakeCredentialRequest<'a> {
    pub client_data_hash: Bytes<&'a [u8]>,
    pub rp: Entity<&'a [u8], &'a str>,
    pub user: Entity<&'a [u8], &'a str>,
    pub public_key_params: PublicKeyParams,
    pub exclude_list: Option<CredentialDescriptorList<'a>>,
    pub options: CtapOptions,
    pub pin_auth: Option<Bytes<&'a [u8]>>,
    pub pin_protocol: Option<u8>,
}

struct MakeCredentialRequestVisitor;
impl<'de> Visitor<'de> for MakeCredentialRequestVisitor {
    type Value = MakeCredentialRequest<'de>;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a map with unsigned integers as keys")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, <M as MapAccess<'de>>::Error>
    where
        M: MapAccess<'de>,
    {
        let mut client_data_hash: Option<Bytes<&'de [u8]>> = None;
        let mut rp: Option<Entity<&'de [u8], &'de str>> = None;
        let mut user: Option<Entity<&'de [u8], &'de str>> = None;
        let mut public_key_params: Option<PublicKeyParams> = None;
        let mut exclude_list: Option<CredentialDescriptorList<'de>> = None;
        let mut options: Option<CtapOptions> = None;
        let mut pin_auth: Option<Bytes<&'de [u8]>> = None;
        let mut pin_protocol: Option<u8> = None;

        let mut _ignore: ExtensionStub;
        while let Some(key) = access.next_key()? {
            match key {
                1 => client_data_hash = Some(access.next_value()?),
                2 => rp = Some(access.next_value()?),
                3 => user = Some(access.next_value()?),
                4 => public_key_params = Some(access.next_value()?),
                5 => exclude_list = Some(access.next_value()?),
                // Ignore extensions
                6 => _ignore = access.next_value()?,
                7 => options = Some(access.next_value()?),
                8 => pin_auth = Some(access.next_value()?),
                9 => pin_protocol = Some(access.next_value()?),
                a => {
                    return Err(<M::Error as Error>::custom(format_args!(
                        "unknown key {}",
                        a
                    )))
                }
            }
        }

        if let (Some(client_data_hash), Some(rp), Some(user), Some(public_key_params)) =
            (client_data_hash, rp, user, public_key_params)
        {
            Ok(MakeCredentialRequest {
                client_data_hash,
                rp,
                user,
                public_key_params,
                exclude_list,
                options: options.unwrap_or(CtapOptions {
                    plat: None,
                    rk: Some(false),
                    client_pin: None,
                    up: None,
                    uv: Some(false),
                }),
                pin_auth,
                pin_protocol,
            })
        } else {
            Err(<M::Error as Error>::missing_field(
                "the MakeCredentialRequest is missing some required fields",
            ))
        }
    }
}

impl<'de> Deserialize<'de> for MakeCredentialRequest<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(MakeCredentialRequestVisitor)
    }
}

/// Contains the data, which was passed via a `GetAssertionRequest`
#[derive(Debug, Clone, PartialEq, Eq)]
// UNIMPLEMENTED: Extensions
pub(crate) struct GetAssertionRequest<'a> {
    pub rpid: &'a str,
    pub client_data_hash: Bytes<&'a [u8]>,
    pub allow_list: Option<CredentialDescriptorList<'a>>,
    pub options: CtapOptions,
    pub pin_auth: Option<Bytes<&'a [u8]>>,
    pub pin_protocol: Option<u8>,
}

struct GetAssertionRequestVisitor;
impl<'de> Visitor<'de> for GetAssertionRequestVisitor {
    type Value = GetAssertionRequest<'de>;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a map with unsigned integers as keys")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, <M as MapAccess<'de>>::Error>
    where
        M: MapAccess<'de>,
    {
        let mut rpid: Option<&'de str> = None;
        let mut client_data_hash: Option<Bytes<&'de [u8]>> = None;
        let mut allow_list: Option<CredentialDescriptorList<'de>> = None;
        let mut options: Option<CtapOptions> = None;
        let mut pin_auth: Option<Bytes<&'de [u8]>> = None;
        let mut pin_protocol: Option<u8> = None;

        let mut _ignore: ExtensionStub;
        while let Some(key) = access.next_key()? {
            match key {
                1 => rpid = Some(access.next_value()?),
                2 => client_data_hash = Some(access.next_value()?),
                3 => allow_list = Some(access.next_value()?),
                // Ignore extensions
                4 => _ignore = access.next_value()?,
                5 => options = Some(access.next_value()?),
                6 => pin_auth = Some(access.next_value()?),
                7 => pin_protocol = Some(access.next_value()?),
                a => {
                    return Err(<M::Error as Error>::custom(format_args!(
                        "unknown key {}",
                        a
                    )))
                }
            }
        }

        if let (Some(rpid), Some(client_data_hash)) = (rpid, client_data_hash) {
            Ok(GetAssertionRequest {
                rpid,
                client_data_hash,
                allow_list,
                options: options.unwrap_or(CtapOptions {
                    plat: None,
                    rk: None,
                    client_pin: None,
                    up: Some(true),
                    uv: Some(false),
                }),
                pin_auth,
                pin_protocol,
            })
        } else {
            Err(<M::Error as Error>::missing_field(
                "the GetAssertionRequest is missing a required field",
            ))
        }
    }
}

impl<'de> Deserialize<'de> for GetAssertionRequest<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(GetAssertionRequestVisitor)
    }
}

/// A parsed `Request`.
#[allow(clippy::large_enum_variant)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) enum Request<'a> {
    MakeCredential(MakeCredentialRequest<'a>),
    GetAssertion(GetAssertionRequest<'a>),
    GetNextAssertion,
    GetInfo,
    Reset,
}

impl<'a> Request<'a> {
    pub(crate) fn parse(data: &'a [u8]) -> Result<Self, CtapError> {
        // UNIMPLEMENTED: Return invalid length if the data is not fitting
        // This required a refacotr in the authenticator function

        let mut scratch = [0; 100];
        let reader = SliceReadFixed::new(&data[1..], &mut scratch);
        let mut deser = serde_cbor::Deserializer::new(reader);

        match data[0] {
            0x01 => Ok(Request::MakeCredential(
                MakeCredentialRequest::deserialize(&mut deser)
                    .map_err(|_| CtapError::MissingParameter)?,
            )),
            0x02 => Ok(Request::GetAssertion(
                GetAssertionRequest::deserialize(&mut deser)
                    .map_err(|_| CtapError::MissingParameter)?,
            )),
            0x04 => Ok(Request::GetInfo),
            0x07 => Ok(Request::Reset),
            0x08 => Ok(Request::GetNextAssertion),
            _ => Err(CtapError::InvalidCommand),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::bytes::Bytes;
    use crate::protocol::descriptorlist::test::test_descriptor_list;
    use crate::protocol::entity::test::{simple_test_rp_entity, test_user_entity};
    use crate::protocol::entity::CtapOptions;
    use crate::protocol::key::PublicKeyParams;
    use crate::protocol::request::{GetAssertionRequest, MakeCredentialRequest, Request};

    const TEST_MAKE_CREDENTIAL_REQUEST: [u8; 261] = [
        0x01, // authenticatorMakeCredential command
        0xa5, // map(5)
        0x01, // unsigned(1) - clientDataHash
        0x58, 0x20, // bytes(32)
        // h’687134968222ec17202e42505f8ed2b16ae22f16bb05b88c25db9e602645f141'
        0x68, 0x71, 0x34, 0x96, 0x82, 0x22, 0xec, 0x17, 0x20, 0x2e, 0x42, 0x50, 0x5f, 0x8e, 0xd2,
        0xb1, 0x6a, 0xe2, 0x2f, 0x16, 0xbb, 0x05, 0xb8, 0x8c, 0x25, 0xdb, 0x9e, 0x60, 0x26, 0x45,
        0xf1, 0x41, //
        0x02, // unsigned(2) - rp
        0xa2, // map(2)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x6b, // text(11)
        0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, // "example.com"
        0x64, // text(4)
        0x6e, 0x61, 0x6d, 0x65, // "name"
        0x64, // text(4)
        0x41, 0x63, 0x6d, 0x65, // "Acme"
        0x03, // unsigned(3) - user
        0xa4, // map(4)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x20, // bytes(32)
        // userid
        0x30, 0x82, 0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82,
        0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82, 0x01, 0x93,
        0x30, 0x82, //
        0x64, // text(4)
        0x69, 0x63, 0x6f, 0x6e, // "icon"
        0x78, 0x2b, // text(43)
        // "https://pics.example.com/00/p/aBjjjpqPb.png"
        0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x70, 0x69, 0x63, 0x73, 0x2e, 0x65, 0x78,
        0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x30, 0x30, 0x2f, 0x70, 0x2f,
        0x61, 0x42, 0x6a, 0x6a, 0x6a, 0x70, 0x71, 0x50, 0x62, 0x2e, 0x70, 0x6e, 0x67, //
        0x64, // text(4)
        0x6e, 0x61, 0x6d, 0x65, // "name"
        0x76, // text(22)
        // "johnpsmith@example.com"
        0x6a, 0x6f, 0x68, 0x6e, 0x70, 0x73, 0x6d, 0x69, 0x74, 0x68, 0x40, 0x65, 0x78, 0x61, 0x6d,
        0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, //
        0x6b, // text(11)
        0x64, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x4e, 0x61, 0x6d, 0x65, // "displayName"
        0x6d, // text(13)
        0x4a, 0x6f, 0x68, 0x6e, 0x20, 0x50, 0x2e, 0x20, 0x53, 0x6d, 0x69, 0x74,
        0x68, // "John P. Smith"
        0x04, // unsigned(4) - pubKeyCredParams
        0x82, // array(2)
        0xa2, // map(2)
        0x63, // text(3)
        0x61, 0x6c, 0x67, // "alg"
        0x26, // -7 (ES256)
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0xa2, // map(2)
        0x63, // text(3)
        0x61, 0x6c, 0x67, // "alg"
        0x39, 0x01, 0x00, // -257 (RS256)
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0x07, // unsigned(7) - options
        0xa1, // map(1)
        0x62, // text(2)
        0x72, 0x6b, // "rk"
        0xf5, // primitive(21)
    ];

    const TEST_CLIENT_DATA_HASH: [u8; 32] = [
        0x68, 0x71, 0x34, 0x96, 0x82, 0x22, 0xec, 0x17, 0x20, 0x2e, 0x42, 0x50, 0x5f, 0x8e, 0xd2,
        0xb1, 0x6a, 0xe2, 0x2f, 0x16, 0xbb, 0x05, 0xb8, 0x8c, 0x25, 0xdb, 0x9e, 0x60, 0x26, 0x45,
        0xf1, 0x41,
    ];

    #[test]
    fn make_credential_deserialize() {
        let parsed_request = Request::parse(&TEST_MAKE_CREDENTIAL_REQUEST).unwrap();
        let request = Request::MakeCredential(MakeCredentialRequest {
            client_data_hash: Bytes::from(&TEST_CLIENT_DATA_HASH),
            rp: simple_test_rp_entity(),
            user: test_user_entity(),
            public_key_params: PublicKeyParams::es256(),
            exclude_list: None,
            options: CtapOptions {
                plat: None,
                rk: Some(true),
                client_pin: None,
                up: None,
                uv: None,
            },
            pin_auth: None,
            pin_protocol: None,
        });
        assert_eq!(parsed_request, request);
    }

    const TEST_GET_ASSERTION_REQUEST: [u8; 216] = [
        0x02, // authenticatorGetAssertion command
        0xa4, // map(4)
        0x01, // unsigned(1)
        0x6b, // text(11)
        0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, // "example.com"
        0x02, // unsigned(2)
        0x58, 0x20, // bytes(32)
        // clientDataHash
        0x68, 0x71, 0x34, 0x96, 0x82, 0x22, 0xec, 0x17, 0x20, 0x2e, 0x42, 0x50, 0x5f, 0x8e, 0xd2,
        0xb1, 0x6a, 0xe2, 0x2f, 0x16, 0xbb, 0x05, 0xb8, 0x8c, 0x25, 0xdb, 0x9e, 0x60, 0x26, 0x45,
        0xf1, 0x41, //
        0x03, // unsigned(3)
        0x82, // array(2)
        0xa2, // map(2)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x40, // bytes(64)
        // credential ID
        0xf2, 0x20, 0x06, 0xde, 0x4f, 0x90, 0x5a, 0xf6, 0x8a, 0x43, 0x94, 0x2f, 0x02, 0x4f, 0x2a,
        0x5e, 0xce, 0x60, 0x3d, 0x9c, 0x6d, 0x4b, 0x3d, 0xf8, 0xbe, 0x08, 0xed, 0x01, 0xfc, 0x44,
        0x26, 0x46, 0xd0, 0x34, 0x85, 0x8a, 0xc7, 0x5b, 0xed, 0x3f, 0xd5, 0x80, 0xbf, 0x98, 0x08,
        0xd9, 0x4f, 0xcb, 0xee, 0x82, 0xb9, 0xb2, 0xef, 0x66, 0x77, 0xaf, 0x0a, 0xdc, 0xc3, 0x58,
        0x52, 0xea, 0x6b, 0x9e, //
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0xa2, // map(2)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x32, // bytes(50)
        // credential ID
        0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
        0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
        0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
        0x03, 0x03, 0x03, 0x03, 0x03, //
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0x05, // unsigned(5)
        0xa1, // map(1)
        0x62, // text(2)
        0x75, 0x76, // "uv"
        0xf5, // true
    ];

    #[test]
    fn get_assertion_deserialize() {
        let parsed_assertion = Request::parse(&TEST_GET_ASSERTION_REQUEST).unwrap();
        let assertion = Request::GetAssertion(GetAssertionRequest {
            rpid: "example.com",
            client_data_hash: Bytes::from(&TEST_CLIENT_DATA_HASH),
            allow_list: Some(test_descriptor_list()),
            options: CtapOptions {
                plat: None,
                rk: None,
                client_pin: None,
                up: None,
                uv: Some(true),
            },
            pin_auth: None,
            pin_protocol: None,
        });
        assert_eq!(parsed_assertion, assertion);
    }
}
