#[cfg(not(any(feature = "alloc", feature = "std")))]
pub use fixed_length::*;

#[cfg(any(feature = "alloc", feature = "std"))]
pub use var_length::*;

#[cfg(any(feature = "alloc", feature = "std"))]
/// Implements [`CredentialDescriptor`](struct.CredentialDescriptorList.html) simply as a wrapper
/// around a Vector
mod var_length {
    use crate::CredentialDescriptor;
    use core::fmt::{Error, Formatter};
    use serde::de::{SeqAccess, Visitor};
    use serde::{Deserialize, Deserializer};

    // If we use alloc but not std, we need to include the Vector implementation manually
    #[cfg(all(feature = "alloc", not(feature = "std")))]
    use alloc::vec::Vec;

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct CredentialDescriptorList<'a> {
        index: usize,
        vec: Vec<CredentialDescriptor<&'a [u8]>>,
    }

    impl<'a> Iterator for CredentialDescriptorList<'a> {
        type Item = CredentialDescriptor<&'a [u8]>;

        fn next(&mut self) -> Option<Self::Item> {
            if self.index < self.vec.len() {
                self.index += 1;
                Some(self.vec[self.index - 1])
            } else {
                None
            }
        }
    }

    struct DescriptorListVisitor;
    impl<'de> Visitor<'de> for DescriptorListVisitor {
        type Value = CredentialDescriptorList<'de>;

        fn expecting(&self, formatter: &mut Formatter) -> Result<(), Error> {
            formatter.write_str("a sequence of CredentialDescriptors")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>,
        {
            let mut items = Vec::new();
            while let Some(elem) = seq.next_element()? {
                items.push(elem);
            }

            Ok(CredentialDescriptorList {
                index: 0,
                vec: items,
            })
        }
    }

    impl<'de> Deserialize<'de> for CredentialDescriptorList<'de> {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
        {
            deserializer.deserialize_map(DescriptorListVisitor)
        }
    }

    #[cfg(test)]
    pub mod test {
        use crate::protocol::descriptorlist::CredentialDescriptorList;
        use crate::CredentialDescriptor;

        #[cfg(any(feature = "alloc", feature = "std"))]
        pub(crate) fn test_descriptor_list() -> CredentialDescriptorList<'static> {
            CredentialDescriptorList {
                index: 0,
                vec: vec![
                    CredentialDescriptor {
                        id: &[
                            0xf2, 0x20, 0x06, 0xde, 0x4f, 0x90, 0x5a, 0xf6, 0x8a, 0x43, 0x94, 0x2f,
                            0x02, 0x4f, 0x2a, 0x5e, 0xce, 0x60, 0x3d, 0x9c, 0x6d, 0x4b, 0x3d, 0xf8,
                            0xbe, 0x08, 0xed, 0x01, 0xfc, 0x44, 0x26, 0x46, 0xd0, 0x34, 0x85, 0x8a,
                            0xc7, 0x5b, 0xed, 0x3f, 0xd5, 0x80, 0xbf, 0x98, 0x08, 0xd9, 0x4f, 0xcb,
                            0xee, 0x82, 0xb9, 0xb2, 0xef, 0x66, 0x77, 0xaf, 0x0a, 0xdc, 0xc3, 0x58,
                            0x52, 0xea, 0x6b, 0x9e,
                        ],
                        transports: None,
                    },
                    CredentialDescriptor {
                        id: &[
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03,
                        ],
                        transports: None,
                    },
                ],
            }
        }
    }
}

#[cfg(not(any(feature = "alloc", feature = "std")))]
mod fixed_length {
    use crate::CredentialDescriptor;
    use core::fmt::{Error, Formatter};
    use serde::de::{SeqAccess, Visitor};
    use serde::{Deserialize, Deserializer};

    const DESC_LIST_MAX_SIZE: usize = 32;

    /// A list of [`CredentialDescriptors`](struct.CredentialDescriptor.html).
    ///
    /// It is used in `MakeCredential` and `GetAssertion` calls.
    /// See [`AuthenticatorPlatform`](trait.AuthenticatorPlatform.html) for usage.
    // UNIMPLEMENTED: Settable length. It is hardcoded right now
    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct CredentialDescriptorList<'a> {
        index: usize,
        length: usize,
        list: [Option<CredentialDescriptor<&'a [u8]>>; DESC_LIST_MAX_SIZE],
    }

    impl<'a> Iterator for CredentialDescriptorList<'a> {
        type Item = CredentialDescriptor<&'a [u8]>;

        fn next(&mut self) -> Option<Self::Item> {
            if self.list[self.index].is_some() {
                self.index += 1;
                self.list[self.index - 1]
            } else {
                None
            }
        }
    }

    struct DescriptorListVisitor;
    impl<'de> Visitor<'de> for DescriptorListVisitor {
        type Value = CredentialDescriptorList<'de>;

        fn expecting(&self, formatter: &mut Formatter) -> Result<(), Error> {
            formatter.write_str("a sequence of CredentialDescriptors")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>,
        {
            let mut num_elems = 0;
            let mut collected_elems: [Option<CredentialDescriptor<&'de [u8]>>; DESC_LIST_MAX_SIZE] =
                [None; DESC_LIST_MAX_SIZE];

            while let Some(elem) = seq.next_element()? {
                if num_elems >= DESC_LIST_MAX_SIZE {
                    // NOTE: We can only store a fixed number of descriptors.
                    // If there are to many, we truncate
                    break;
                }
                num_elems += 1;
                collected_elems[num_elems - 1] = Some(elem)
            }

            Ok(CredentialDescriptorList {
                index: 0,
                length: num_elems,
                list: collected_elems,
            })
        }
    }

    impl<'de> Deserialize<'de> for CredentialDescriptorList<'de> {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
        {
            deserializer.deserialize_map(DescriptorListVisitor)
        }
    }

    #[cfg(test)]
    pub(crate) mod test {
        use crate::bytes::Bytes;
        use crate::protocol::descriptorlist::CredentialDescriptorList;
        use crate::CredentialDescriptor;

        #[cfg(not(any(feature = "alloc", feature = "std")))]
        pub(crate) fn test_descriptor_list() -> CredentialDescriptorList<'static> {
            CredentialDescriptorList {
                index: 0,
                length: 2,
                list: [
                    Some(CredentialDescriptor {
                        id: Bytes::from(&[
                            0xf2, 0x20, 0x06, 0xde, 0x4f, 0x90, 0x5a, 0xf6, 0x8a, 0x43, 0x94, 0x2f,
                            0x02, 0x4f, 0x2a, 0x5e, 0xce, 0x60, 0x3d, 0x9c, 0x6d, 0x4b, 0x3d, 0xf8,
                            0xbe, 0x08, 0xed, 0x01, 0xfc, 0x44, 0x26, 0x46, 0xd0, 0x34, 0x85, 0x8a,
                            0xc7, 0x5b, 0xed, 0x3f, 0xd5, 0x80, 0xbf, 0x98, 0x08, 0xd9, 0x4f, 0xcb,
                            0xee, 0x82, 0xb9, 0xb2, 0xef, 0x66, 0x77, 0xaf, 0x0a, 0xdc, 0xc3, 0x58,
                            0x52, 0xea, 0x6b, 0x9e,
                        ]).as_ref(),
                        transports: None,
                    }),
                    Some(CredentialDescriptor {
                        id: Bytes::from(&[
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
                            0x03, 0x03,
                        ]).as_ref(),
                        transports: None,
                    }),
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                    None,
                ],
            }
        }
    }
}
