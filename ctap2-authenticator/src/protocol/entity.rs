use crate::bytes::Bytes;
use serde::de::{Error, MapAccess, Visitor};
use serde::export::Formatter;
use serde::ser::SerializeMap;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct Entity<B: AsRef<[u8]>, S: AsRef<str>> {
    pub id: Bytes<B>,
    pub icon: Option<S>,
    pub name: S,
    pub display_name: Option<S>,
}

// NOTE: We write the serializer ourselves, because SerdeDerive does not know we dont need the
// members to be serializeable
impl<B: AsRef<[u8]>, Str: AsRef<str>> Serialize for Entity<B, Str> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let len = 2 + [self.icon.is_some(), self.display_name.is_some()]
            .iter()
            .filter(|x| **x)
            .count();
        let mut map = serializer.serialize_map(Some(len))?;
        map.serialize_entry("id", &self.id)?;

        if let Some(ref icon) = self.icon {
            map.serialize_entry("icon", icon.as_ref())?;
        }

        map.serialize_entry("name", &self.name.as_ref())?;

        if let Some(ref display_name) = self.display_name {
            map.serialize_entry("displayName", display_name.as_ref())?;
        }

        map.end()
    }
}

struct EntityVisitor;
impl<'de> Visitor<'de> for EntityVisitor {
    type Value = Entity<&'de [u8], &'de str>;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a map with strings as values and strings and byte strings as values")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let mut id: Option<&'de [u8]> = None;
        let mut icon: Option<&'de str> = None;
        let mut name: Option<&'de str> = None;
        let mut display_name: Option<&'de str> = None;

        while let Some(key) = access.next_key()? {
            match key {
                // FIXME: If we parse a UserCredential, id MUST NOT be a string
                // If we parse RpCredential, id MUST be a string
                // This causes test_bad_type_user_id to fail
                // Why does this not cause a problem with serialization?
                "id" => id = Some(access.next_value()?),
                "icon" => icon = Some(access.next_value()?),
                "name" => name = Some(access.next_value()?),
                "displayName" => display_name = Some(access.next_value()?),
                a => {
                    return Err(<M::Error as Error>::unknown_field(
                        a,
                        &["id", "icon", "name", "displayName"],
                    ))
                }
            }
        }

        if let (Some(id), Some(name)) = (id, name) {
            log::debug!("Id is {:?}", id);
            Ok(Entity {
                id: Bytes::from(id),
                icon,
                name,
                display_name,
            })
        } else {
            Err(<M::Error as Error>::missing_field(
                "the UserEntity is missing some required fields",
            ))
        }
    }
}

impl<'de> Deserialize<'de> for Entity<&'de [u8], &'de str> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(EntityVisitor)
    }
}

/// The Options which an Authenticator can support.
///
/// Refer to[`WebAuthn`](https://www.w3.org/TR/webauthn/) for more information.
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CtapOptions {
    /// Indicates, that this authenticator is bound to a platform, i.e. not a roaming authenticator.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub plat: Option<bool>,
    /// Indicates, that this authenticator supports ResidentKeys (not supported at the moment).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rk: Option<bool>,
    /// Indicates, that this authenticator supports the ClientPin protocol
    /// (not supported at the moment).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub client_pin: Option<bool>,
    /// Indicates, that this authenticator is able to check that the user is present.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub up: Option<bool>,
    /// Indicates, that this authenticator is capable of verifying the user (e.g. via fingerprint)
    /// (not supported at the moment)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uv: Option<bool>,
}

#[cfg(test)]
pub(crate) mod test {
    use crate::bytes::Bytes;
    use crate::protocol::entity::{CtapOptions, Entity};

    // This is a test case taken from the CTAP specification 6.1
    const TEST_USER_ENTITY: [u8; 142] = [
        0xa4, // map(4)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x20, // bytes(32)
        0x30, 0x82, 0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82,
        0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82, 0x01, 0x93,
        0x30, 0x82, // userid
        0x64, // text(4)
        0x69, 0x63, 0x6f, 0x6e, // "icon"
        0x78, 0x2b, // text(43)
        0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x70, 0x69, 0x63, 0x73, 0x2e, 0x65, 0x78,
        0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x30, 0x30, 0x2f, 0x70, 0x2f,
        0x61, 0x42, 0x6a, 0x6a, 0x6a, 0x70, 0x71, 0x50, 0x62, 0x2e, 0x70, 0x6e,
        0x67, // "https://pics.example.com/00/p/aBjjjpqPb.png"
        0x64, // text(4)
        0x6e, 0x61, 0x6d, 0x65, // "name"
        0x76, // text(22)
        0x6a, 0x6f, 0x68, 0x6e, 0x70, 0x73, 0x6d, 0x69, 0x74, 0x68, 0x40, 0x65, 0x78, 0x61, 0x6d,
        0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, // "johnpsmith@example.com"
        0x6b, // text(11)
        0x64, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x4e, 0x61, 0x6d, 0x65, // "displayName"
        0x6d, // text(13)
        0x4a, 0x6f, 0x68, 0x6e, 0x20, 0x50, 0x2e, 0x20, 0x53, 0x6d, 0x69, 0x74,
        0x68, // "John P. Smith"
    ];

    pub(crate) fn test_user_entity() -> Entity<&'static [u8], &'static str> {
        Entity {
            name: "johnpsmith@example.com",
            id: Bytes::from(&TEST_ENTITY_ID),
            icon: Some("https://pics.example.com/00/p/aBjjjpqPb.png"),
            display_name: Some("John P. Smith"),
        }
    }

    // This is a test case taken from the CTAP specification 6.1
    const TEST_RP_ENTITY: [u8; 116] = [
        0xa3, // map(3)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x20, // bytes(32)
        0x30, 0x82, 0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82,
        0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82, 0x01, 0x93,
        0x30, 0x82, // userid
        0x64, // text(4)
        0x69, 0x63, 0x6f, 0x6e, // "icon"
        0x78, 0x2b, // text(43)
        0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x70, 0x69, 0x63, 0x73, 0x2e, 0x65, 0x78,
        0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x30, 0x30, 0x2f, 0x70, 0x2f,
        0x61, 0x42, 0x6a, 0x6a, 0x6a, 0x70, 0x71, 0x50, 0x62, 0x2e, 0x70, 0x6e,
        0x67, // "https://pics.example.com/00/p/aBjjjpqPb.png"
        0x64, // text(4)
        0x6e, 0x61, 0x6d, 0x65, // "name"
        0x76, // text(22)
        0x6a, 0x6f, 0x68, 0x6e, 0x70, 0x73, 0x6d, 0x69, 0x74, 0x68, 0x40, 0x65, 0x78, 0x61, 0x6d,
        0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, // "johnpsmith@example.com"
    ];

    pub(crate) const TEST_ENTITY_ID: [u8; 32] = [
        0x30, 0x82, 0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82,
        0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82, 0x01, 0x93,
        0x30, 0x82,
    ];

    fn test_rp_entity() -> Entity<&'static [u8], &'static str> {
        Entity {
            name: "johnpsmith@example.com",
            id: Bytes::from(&TEST_ENTITY_ID),
            icon: Some("https://pics.example.com/00/p/aBjjjpqPb.png"),
            display_name: None,
        }
    }

    pub(crate) fn simple_test_rp_entity() -> Entity<&'static [u8], &'static str> {
        Entity {
            id: Bytes::from(b"example.com"),
            icon: None,
            name: "Acme",
            display_name: None,
        }
    }

    #[test]
    fn user_entity_serialize() {
        let user_entity = test_user_entity();
        assert_eq!(
            &serde_cbor::to_vec(&user_entity).unwrap()[..],
            &TEST_USER_ENTITY[..]
        );
    }

    #[test]
    fn user_entity_deserialize() {
        let user_entity = test_user_entity();
        let parsed_user_entity: Entity<&[u8], &str> =
            serde_cbor::from_slice(&TEST_USER_ENTITY).unwrap();
        assert_eq!(parsed_user_entity, user_entity,);
    }

    #[test]
    fn rp_entity_serialize() {
        let rp_entity = test_rp_entity();

        assert_eq!(
            &serde_cbor::to_vec(&rp_entity).unwrap()[..],
            &TEST_RP_ENTITY[..]
        );
    }

    #[test]
    fn rp_entity_deserialize() {
        let rp_entity = test_rp_entity();
        let parsed_user_entity: Entity<&[u8], &str> =
            serde_cbor::from_slice(&TEST_RP_ENTITY).unwrap();
        assert_eq!(parsed_user_entity, rp_entity,);
    }

    #[test]
    fn ctap_option_serialize() {
        let option_cbor: [u8; 9] = [
            0xa2, // map(3)
            0x62, // text(2)
            0x72, 0x6b, // "rk"
            0xf4, // false
            0x62, // text(2)
            0x75, 0x76, // "uv"
            0xf5, //true
        ];

        let option = CtapOptions {
            plat: None,
            rk: Some(false),
            client_pin: None,
            up: None,
            uv: Some(true),
        };
        assert_eq!(serde_cbor::to_vec(&option).unwrap(), option_cbor);
    }

    #[test]
    fn ctap_option_deserialize() {
        let option_cbor: [u8; 13] = [
            0xa3, // map(2)
            0x62, // text(2)
            0x75, 0x76, // "uv"
            0xf5, //true
            0x62, // text(2)
            0x75, 0x70, // "up"
            0xf4, // false
            0x62, // text(2)
            0x72, 0x6b, // "rk"
            0xf4, // false
        ];

        let option = CtapOptions {
            plat: None,
            rk: Some(false),
            client_pin: None,
            up: Some(false),
            uv: Some(true),
        };

        let parsed: CtapOptions = serde_cbor::from_slice(&option_cbor).unwrap();
        assert_eq!(parsed, option);
    }
}
