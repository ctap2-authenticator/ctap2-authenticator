use core::fmt::{Error, Formatter};
use serde::de::{IgnoredAny, MapAccess, Visitor};
use serde::{Deserialize, Deserializer};

/// This is a stub for extensions.
/// It essentially just checks that the top level structure is a map and ignores all content.
// UNIMPLEMENTED: Extension registry API for consumers of this Library to implement their own
// extensions.
pub struct ExtensionStub;
struct ExtensionStubVisitor;

impl<'de> Visitor<'de> for ExtensionStubVisitor {
    type Value = ExtensionStub;

    fn expecting(&self, formatter: &mut Formatter) -> Result<(), Error> {
        formatter.write_str("a map with strings as inouts and arbitrary data as as output")
    }

    fn visit_map<A>(self, mut access: A) -> Result<Self::Value, <A as MapAccess<'de>>::Error>
    where
        A: MapAccess<'de>,
    {
        // Ignore all the data in the value
        let mut _ignore: IgnoredAny;
        while let Some(_key) = access.next_key::<&str>()? {
            _ignore = access.next_value()?;
        }

        Ok(ExtensionStub)
    }
}

impl<'de> Deserialize<'de> for ExtensionStub {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(ExtensionStubVisitor)
    }
}
