use bitflags::_core::fmt::Formatter;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{Error, MapAccess, SeqAccess, Visitor};
use serde::ser::{SerializeMap, SerializeSeq};

use crate::bytes::Bytes;

/// A descriptor as specified by the
/// [`WebAuthn`](https://www.w3.org/TR/webauthn/#dictdef-publickeycredentialdescriptor)
/// specifictaion.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CredentialDescriptor<B: AsRef<[u8]>> {
    pub(crate) id: B,
    pub(crate) transports: Option<Transports>,
}

impl<'a> Copy for CredentialDescriptor<&'a [u8]> {}

impl<B: AsRef<[u8]>> CredentialDescriptor<B> {
    /// Get the id of this [`CredentialDescriptor`](struct.CredentialDescriptor.html)
    ///
    /// # Returns
    /// The id of this credential
    pub fn get_id(&self) -> &B {
        &self.id
    }

    /// Get the allowed transports of this
    /// [`CredentialDescriptor`](struct.CredentialDescriptor.html)
    ///
    /// # Returns
    /// The [`Transport`](struct.Transports.html), that this
    /// [`CredentialDescriptor`](struct.CredentialDescriptor.html) is allowed to use
    pub fn get_transport(&self) -> Option<Transports> {
        self.transports
    }
}

impl<B: AsRef<[u8]>> Serialize for CredentialDescriptor<B> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        let len = if self.transports.is_none() { 2 } else { 3 };

        let mut map = serializer.serialize_map(Some(len))?;
        map.serialize_entry("id", &Bytes::from(&self.id))?;
        map.serialize_entry("type", "public-key")?;
        if let Some(ref transports) = self.transports {
            map.serialize_entry("transports", transports)?;
        }
        map.end()
    }
}

struct CredentialDescriptorVisitor;

impl<'de> Visitor<'de> for CredentialDescriptorVisitor {
    type Value = CredentialDescriptor<&'de [u8]>;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a map with strings as values")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
        where
            M: MapAccess<'de>,
    {
        let mut pubkey = false;
        let mut id: Option<&'de [u8]> = None;
        let mut transports: Option<Transports> = None;

        while let Some(key) = access.next_key()? {
            match key {
                "type" => {
                    let val: &str = access.next_value()?;
                    // FIXME: The fact that we only allow public-key should not be represented here
                    // This causes exclude lists to fail, which can have arbitrary types
                    // This causes test_exclude_list(2) fail
                    // This causes test_allow_list_fake_item fail
                    if "public-key" == val {
                        pubkey = true;
                    } else {
                        return Err(<M::Error as Error>::custom(format_args!(
                            "The CredentialDescriptor of type {} is not supported",
                            val
                        )));
                    }
                }
                // FIXME: We should somehow check the correct length of the ID field.
                // This causes test_bad_type_exclude_list_id fail
                // This causes test_allow_list_field_wrong_type
                "id" => id = Some(access.next_value()?),
                "transports" => transports = Some(access.next_value()?),
                a => {
                    return Err(<M::Error as Error>::unknown_field(
                        a,
                        &["type", "id", "transports"],
                    ));
                }
            }
        }

        if let Some(id) = id {
            if pubkey {
                return Ok(CredentialDescriptor { id, transports });
            }
        }

        Err(<M::Error as Error>::missing_field(
            "some fields are missing",
        ))
    }
}

impl<'de> Deserialize<'de> for CredentialDescriptor<&'de [u8]> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
    {
        deserializer.deserialize_map(CredentialDescriptorVisitor)
    }
}

/// A transport used by a
/// [`CredentialDescriptor`](struct.CredentialDescriptor.html).
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Transports {
    /// If set, the credential can be used in a USB based authenticator
    pub usb: bool,
    /// If set, the credential can be used in a NFC based authenticator
    pub nfc: bool,
    /// If set, the credential can be used in a Bluetooth based authenticator
    pub ble: bool,
    /// If set, the credential can be used in an internal authenticator
    pub internal: bool,
}

impl Serialize for Transports {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        // Count the number of transports that are true
        let len = [self.usb, self.nfc, self.ble, self.internal]
            .iter()
            .filter(|x| **x)
            .count();

        let mut seq = serializer.serialize_seq(Some(len))?;
        if self.usb {
            seq.serialize_element("usb")?;
        }
        if self.nfc {
            seq.serialize_element("nfc")?;
        }
        if self.ble {
            seq.serialize_element("ble")?;
        }
        if self.internal {
            seq.serialize_element("internal")?;
        }
        seq.end()
    }
}

struct TransportsVisitor;

impl<'de> Visitor<'de> for TransportsVisitor {
    type Value = Transports;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a sequence of strings")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>,
    {
        let mut transport = Transports {
            usb: false,
            nfc: false,
            ble: false,
            internal: false,
        };

        while let Some(elem) = seq.next_element()? {
            match elem {
                "usb" => transport.usb = true,
                "nfc" => transport.nfc = true,
                "ble" => transport.ble = true,
                "internal" => transport.internal = true,
                a => {
                    return Err(<A::Error as Error>::custom(format_args!(
                        "Unknown element {}",
                        a
                    )));
                }
            }
        }

        Ok(transport)
    }
}

impl<'de> Deserialize<'de> for Transports {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
    {
        deserializer.deserialize_map(TransportsVisitor)
    }
}

#[cfg(test)]
pub(crate) mod test {
    use crate::protocol::descriptor::{CredentialDescriptor, Transports};

    const TEST_CREDENTIAL_DESCRIPTOR: [u8; 110] = [
        0xa3, // map(3)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x40, // bytes(64)
        // credential ID
        0xf2, 0x20, 0x06, 0xde, 0x4f, 0x90, 0x5a, 0xf6, 0x8a, 0x43, 0x94, 0x2f, 0x02, 0x4f, 0x2a,
        0x5e, 0xce, 0x60, 0x3d, 0x9c, 0x6d, 0x4b, 0x3d, 0xf8, 0xbe, 0x08, 0xed, 0x01, 0xfc, 0x44,
        0x26, 0x46, 0xd0, 0x34, 0x85, 0x8a, 0xc7, 0x5b, 0xed, 0x3f, 0xd5, 0x80, 0xbf, 0x98, 0x08,
        0xd9, 0x4f, 0xcb, 0xee, 0x82, 0xb9, 0xb2, 0xef, 0x66, 0x77, 0xaf, 0x0a, 0xdc, 0xc3, 0x58,
        0x52, 0xea, 0x6b, 0x9e, //
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0x6a, // text(10)
        0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x73, // "transports"
        0x83, // array(3)
        0x63, // text(3)
        0x75, 0x73, 0x62, // "usb"
        0x63, // text(3)
        0x6e, 0x66, 0x63, // "nfc"
        0x63, // text(3)
        0x62, 0x6c, 0x65, // "ble"
    ];

    pub(crate) const TEST_CREDENTIAL_ID: [u8; 64] = [
        0xf2, 0x20, 0x06, 0xde, 0x4f, 0x90, 0x5a, 0xf6, 0x8a, 0x43, 0x94, 0x2f, 0x02, 0x4f, 0x2a,
        0x5e, 0xce, 0x60, 0x3d, 0x9c, 0x6d, 0x4b, 0x3d, 0xf8, 0xbe, 0x08, 0xed, 0x01, 0xfc, 0x44,
        0x26, 0x46, 0xd0, 0x34, 0x85, 0x8a, 0xc7, 0x5b, 0xed, 0x3f, 0xd5, 0x80, 0xbf, 0x98, 0x08,
        0xd9, 0x4f, 0xcb, 0xee, 0x82, 0xb9, 0xb2, 0xef, 0x66, 0x77, 0xaf, 0x0a, 0xdc, 0xc3, 0x58,
        0x52, 0xea, 0x6b, 0x9e,
    ];

    const TEST_TRANSPORT: [u8; 13] = [
        0x83, // array(3)
        0x63, // text(3)
        0x75, 0x73, 0x62, // usb
        0x63, // text(3)
        0x6e, 0x66, 0x63, // nfc
        0x63, // text(3)
        0x62, 0x6c, 0x65, // ble
    ];

    #[test]
    fn credential_descriptor_serialize() {
        let desc = CredentialDescriptor {
            id: &TEST_CREDENTIAL_ID[..],
            transports: Some(Transports {
                usb: true,
                nfc: true,
                ble: true,
                internal: false,
            }),
        };
        assert_eq!(
            &serde_cbor::to_vec(&desc).unwrap()[..],
            &TEST_CREDENTIAL_DESCRIPTOR[..]
        );
    }

    #[test]
    fn credential_descriptor_deserialize() {
        let parsed_desc: CredentialDescriptor<&[u8]> =
            serde_cbor::from_slice(&TEST_CREDENTIAL_DESCRIPTOR).unwrap();
        let desc = CredentialDescriptor {
            id: &TEST_CREDENTIAL_ID[..],
            transports: Some(Transports {
                usb: true,
                nfc: true,
                ble: true,
                internal: false,
            }),
        };
        assert_eq!(parsed_desc, desc);
    }

    #[test]
    fn transport_serialize() {
        let transport = Transports {
            usb: true,
            nfc: true,
            ble: true,
            internal: false,
        };
        assert_eq!(&serde_cbor::to_vec(&transport).unwrap(), &TEST_TRANSPORT);
    }

    #[test]
    fn transport_deserialize() {
        let parsed_transport: Transports = serde_cbor::from_slice(&TEST_TRANSPORT).unwrap();
        let transport = Transports {
            usb: true,
            nfc: true,
            ble: true,
            internal: false,
        };
        assert_eq!(parsed_transport, transport);
    }
}
