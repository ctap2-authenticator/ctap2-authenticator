use crate::bytes::Bytes;
use crate::error::CtapError;
use crate::protocol::authdata::AuthenticatorData;
use crate::protocol::descriptor::CredentialDescriptor;
use crate::protocol::entity::{CtapOptions, Entity};
use crate::protocol::signature::AttestationStatement;
use crate::AuthenticatorPlatform;
use core::ops::BitOr;
use serde::ser::SerializeMap;
use serde::Serialize;
use serde_cbor::ser::SliceWrite;
use serde_cbor::Serializer;

/// Creates a [`Serializer`](serde::Serializer) on the spot and serializes data into the slice
fn adhoc_serialize<T>(object: &T, data: &mut [u8]) -> Result<usize, CtapError>
where
    T: Serialize,
{
    let mut writer = SliceWrite::new(data);
    let mut ser = Serializer::new(&mut writer);
    object
        .serialize(&mut ser)
        .map_err(|_| CtapError::LimitExceeded)?;
    Ok(writer.bytes_written())
}

/// The Response to the [`MakeCredentialRequest`](crate::request::Request)
#[derive(Debug, PartialEq, Eq)]
pub(crate) struct MakeCredentialResponse<P: AuthenticatorPlatform> {
    pub auth_data: AuthenticatorData<P>,
}

impl<P: AuthenticatorPlatform> MakeCredentialResponse<P> {
    fn to_slice(
        &self,
        platform: &mut P,
        client_data_hash: Bytes<[u8; 32]>,
        data: &mut [u8],
    ) -> Result<usize, CtapError> {
        // Map of 3 entries
        data[0] = 0xa3; // map(3)

        // NOTE: We only support "packet" attestation statements and
        // statically parse the format here
        data[1..9].copy_from_slice(&[
            0x01, // unsigned(2)
            0x66, // text(6)
            0x70, 0x61, 0x63, 0x6b, 0x65, 0x64, // "packed"
        ]);

        // Deserialize the authenticator data
        let mut data_cursor = 9;
        let (auth_data_length, auth_data_header_length) = self
            .auth_data
            .to_slice_with_header(&mut data[data_cursor + 1..])?;

        // Copy the hash behind the auth_data in the buffer
        let client_hash_start = data_cursor + 1 + auth_data_length;
        data[client_hash_start..client_hash_start + 32].copy_from_slice(client_data_hash.inner());

        // Generate the attest
        let id = &self
            .auth_data
            .attested_credential
            .as_ref()
            .unwrap()
            .credential_id;
        let signature = platform
            .attest(
                id,
                &data[data_cursor + 1 + auth_data_header_length..client_hash_start + 32],
            )
            .ok_or(CtapError::OperationDenied)?;

        data[data_cursor] = 0x02;
        data_cursor += 1 + auth_data_length;

        // Build and serialize the attestation statement
        let attestation_statement = AttestationStatement {
            signature,
            attest: P::CERTIFICATE,
        };

        let attestation_stmt_size =
            adhoc_serialize(&attestation_statement, &mut data[data_cursor + 1..])?;
        data[data_cursor] = 0x03;
        data_cursor += 1 + attestation_stmt_size;

        Ok(data_cursor + 1)
    }
}

/// The Response to the [`GetAssertionRequest`](crate::request::Request) or
/// [`GetNextAssertion`](crate::request::Request)
#[derive(Debug, PartialEq, Eq)]
pub(crate) struct GetAssertionResponse<B, S, P>
where
    B: AsRef<[u8]>,
    S: AsRef<str>,
    P: AuthenticatorPlatform,
{
    pub credential: CredentialDescriptor<P::CredentialId>,
    pub auth_data: AuthenticatorData<P>,
    pub user: Option<Entity<B, S>>,
    pub num_credentials: Option<u16>,
}

impl<B, S, P> GetAssertionResponse<B, S, P>
where
    B: AsRef<[u8]>,
    S: AsRef<str>,
    P: AuthenticatorPlatform,
{
    // NOTE: We serialize this part of the protocol manually (without Serde, because we can save
    // some unnecessary buffering this way
    fn to_slice(
        &self,
        platform: &mut P,
        client_data_hash: Bytes<[u8; 32]>,
        data: &mut [u8],
    ) -> Result<usize, CtapError> {
        // Count the number of entries the map is going to have
        let mut map_entries: u8 = 3;
        let mut data_cursor = 0;

        // Serialize the CredentialDescriptor
        let descriptor_size = adhoc_serialize(&self.credential, &mut data[2..])?;

        // NOTE: The first bit is written last, because we need to know how many entries this map
        // is going to have

        data[1] = 1;
        data_cursor += 2 + descriptor_size;

        // Serialize the auth data
        let (auth_data_length, auth_data_header_length) = self
            .auth_data
            .to_slice_with_header(&mut data[data_cursor + 1..])?;

        // Copy the client data hash behind it
        let client_hash_start = data_cursor + 1 + auth_data_length;
        data[client_hash_start..client_hash_start + 32].copy_from_slice(client_data_hash.inner());

        let signature = platform
            .sign(&self.credential.id ,&data[data_cursor + 1 + auth_data_header_length..client_hash_start + 32])
            .ok_or(CtapError::OperationDenied)?;

        data[data_cursor] = 2;
        data_cursor += 1 + auth_data_length;

        let signature_length = signature.to_slice_with_header(&mut data[data_cursor + 1..])?;
        data[data_cursor] = 3;
        data_cursor += 1 + signature_length;

        // Serialize user if it exists
        if let Some(ref user) = self.user {
            map_entries += 1;
            let user_length = adhoc_serialize(user, &mut data[data_cursor + 1..])?;
            data[data_cursor] = 4;
            data_cursor += 1 + user_length;
        }

        // Serialize numCredentials if it exists
        if let Some(ref num_credentials) = self.num_credentials {
            map_entries += 1;
            let num_credentials_length =
                adhoc_serialize(num_credentials, &mut data[data_cursor + 1..])?;
            data[data_cursor] = 5;
            data_cursor += 1 + num_credentials_length;
        }
        data[0] = map_entries.bitor(0b1010_0000);

        Ok(data_cursor + 1)
    }
}

/// The Response to the [`GetInfoRequest`](crate::request::Request)
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct GetInfoResponse<'a> {
    pub aaguid: Bytes<&'a [u8]>,
    pub options: &'a CtapOptions,
    pub max_msg_size: u16,
}

impl<'a> Serialize for GetInfoResponse<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(4))?;
        map.serialize_entry(&1, &["FIDO_2_0"])?;
        map.serialize_entry(&3, &self.aaguid)?;
        map.serialize_entry(&4, self.options)?;
        map.serialize_entry(&5, &self.max_msg_size)?;
        map.end()
    }
}

/// A `Response` ready to be parsed
#[derive(Debug)]
pub(crate) enum Response<B, S, P>
where
    B: AsRef<[u8]>,
    S: AsRef<str>,
    P: AuthenticatorPlatform,
{
    MakeCredential(MakeCredentialResponse<P>),
    GetAssertion(GetAssertionResponse<B, S, P>),
    GetNextAssertion(GetAssertionResponse<B, S, P>),
    GetInfo(GetInfoResponse<'static>),
    Reset,
}

impl<'a, B, S, P> Response<B, S, P>
where
    B: AsRef<[u8]>,
    S: AsRef<str>,
    P: AuthenticatorPlatform,
{
    pub(crate) fn parse(
        &self,
        platform: &mut P,
        client_data_hash: Bytes<[u8; 32]>,
        data: &mut [u8],
    ) -> Result<usize, CtapError> {
        // Set response code to success
        data[0] = 0;

        match self {
            Response::MakeCredential(response) => {
                response.to_slice(platform, client_data_hash, &mut data[1..])
            }
            Response::GetAssertion(response) => {
                response.to_slice(platform, client_data_hash, &mut data[1..])
            }
            Response::GetNextAssertion(response) => {
                response.to_slice(platform, client_data_hash, &mut data[1..])
            }
            Response::GetInfo(response) => {
                let mut writer = SliceWrite::new(&mut data[1..]);
                let mut ser = Serializer::new(&mut writer);
                response
                    .serialize(&mut ser)
                    .map_err(|_| CtapError::LimitExceeded)?;
                Ok(writer.bytes_written() + 1)
            }
            Response::Reset => Ok(1),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::bytes::Bytes;
    use crate::protocol::authdata::{
        AttestedCredential, AuthDataFlags as Flags, AuthenticatorData,
    };
    use crate::protocol::descriptor::test::TEST_CREDENTIAL_ID;
    use crate::protocol::descriptor::CredentialDescriptor;
    use crate::protocol::entity::test::test_user_entity;
    use crate::protocol::entity::CtapOptions;
    use crate::protocol::response::{
        GetAssertionResponse, GetInfoResponse, MakeCredentialResponse, Response,
    };
    use crate::test::MockCtapPlatform;
    use crate::PublicKey;

    const TEST_GET_INFO_RESPONSE: [u8; 45] = [
        0x00, // status = success
        0xa4, // map(4),
        0x01, // unsigned(1) - version
        0x81, // array(1)
        0x68, // text(8)
        0x46, 0x49, 0x44, 0x4f, 0x5f, 0x32, 0x5f, 0x30, // "FIDO_2_0"
        0x03, // unsigned(3) - aaguid
        0x50, // bytes(16),
        // aaguid
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e,
        0x1f, //
        0x04, // unsigned(4) - options
        0xa2, // map(3)
        0x62, // text(2)
        0x72, 0x6b, // "rk"
        0xf4, // false
        0x62, // text(2)
        0x75, 0x76, // "uv"
        0xf5, //true
        0x05, // unsigned(5) - maxMsgSize
        0x19, 0x1d, 0xb9, // unsigned(7609)
    ];

    #[test]
    fn get_info_serialize() {
        let response = Response::<&[u8], &str, MockCtapPlatform>::GetInfo(GetInfoResponse {
            aaguid: Bytes::from(&[
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
                0x1e, 0x1f,
            ]),
            options: &CtapOptions {
                plat: None,
                rk: Some(false),
                client_pin: None,
                up: None,
                uv: Some(true),
            },
            max_msg_size: 7609,
        });
        let mut parsed_response = [0; 7609];
        // NOTE: The clients data hash is not used in this response
        let bytes_written = response
            .parse(
                &mut MockCtapPlatform,
                Bytes::from([0; 32]),
                &mut parsed_response,
            )
            .unwrap();
        assert_eq!(
            &parsed_response[..bytes_written],
            &TEST_GET_INFO_RESPONSE[..]
        );
    }

    const TEST_GET_ASSERTION_RESPONSE: [u8; 348] = [
        0x00, // status = success
        0xa5, // map(5)
        0x01, // unsigned(1) - Credential
        0xa2, // map(2)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x40, // bytes(64)
        // credential ID
        0xf2, 0x20, 0x06, 0xde, 0x4f, 0x90, 0x5a, 0xf6, 0x8a, 0x43, 0x94, 0x2f, 0x02, 0x4f, 0x2a,
        0x5e, 0xce, 0x60, 0x3d, 0x9c, 0x6d, 0x4b, 0x3d, 0xf8, 0xbe, 0x08, 0xed, 0x01, 0xfc, 0x44,
        0x26, 0x46, 0xd0, 0x34, 0x85, 0x8a, 0xc7, 0x5b, 0xed, 0x3f, 0xd5, 0x80, 0xbf, 0x98, 0x08,
        0xd9, 0x4f, 0xcb, 0xee, 0x82, 0xb9, 0xb2, 0xef, 0x66, 0x77, 0xaf, 0x0a, 0xdc, 0xc3, 0x58,
        0x52, 0xea, 0x6b, 0x9e, //
        0x64, // text(4)
        0x74, 0x79, 0x70, 0x65, // "type"
        0x6a, // text(10)
        0x70, 0x75, 0x62, 0x6C, 0x69, 0x63, 0x2D, 0x6B, 0x65, 0x79, // "public-key"
        0x02, // unsigned(2)
        0x58, 0x25, // bytes(37)
        // authData
        0x62, 0x5d, 0xda, 0xdf, 0x74, 0x3f, 0x57, 0x27, 0xe6, 0x6b, 0xba, 0x8c, 0x2e, 0x38, 0x79,
        0x22, 0xd1, 0xaf, 0x43, 0xc5, 0x03, 0xd9, 0x11, 0x4a, 0x8f, 0xba, 0x10, 0x4d, 0x84, 0xd0,
        0x2b, 0xfa, 0x01, 0x00, 0x00, 0x00, 0x11, //
        0x03, // unsigned(3)
        0x58, 0x47, // bytes(71)
        // signature
        0x30, // sequence
        0x45, // bytes(69)
        0x02, // integer
        0x20, // bytes(32)
        0x4a, 0x5a, 0x9d, 0xd3, 0x92, 0x98, 0x14, 0x9d, 0x90, 0x47, 0x69, 0xb5, 0x1a, 0x45, 0x14,
        0x33, 0x00, 0x6f, 0x18, 0x2a, 0x34, 0xfb, 0xdf, 0x66, 0xde, 0x5f, 0xc7, 0x17, 0xd7, 0x5f,
        0xb3, 0x50, //
        0x02, // integer
        0x21, // bytes(33) -- NOTE: Padding because the first Bytes of the Key starts with a 1 bit
        0x00, 0xa4, 0x6b, 0x8e, 0xa3, 0xc3, 0xb9, 0x33, 0x82, 0x1c, 0x6e, 0x7f, 0x5e, 0xf9, 0xda,
        0xae, 0x94, 0xab, 0x47, 0xf1, 0x8d, 0xb4, 0x74, 0xc7, 0x47, 0x90, 0xea, 0xab, 0xb1, 0x44,
        0x11, 0xe7, 0xa0, //
        0x04, // unsigned(4) - publicKeyCredentialUserEntity
        0xa4, // map(4)
        0x62, // text(2)
        0x69, 0x64, // "id"
        0x58, 0x20, // bytes(32)
        // userid
        0x30, 0x82, 0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82,
        0x01, 0x93, 0x30, 0x82, 0x01, 0x38, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x30, 0x82, 0x01, 0x93,
        0x30, 0x82, //
        0x64, // text(4)
        0x69, 0x63, 0x6f, 0x6e, // "icon"
        0x78, 0x2b, // text(43)
        // "https://pics.example.com/00/p/aBjjjpqPb.png"
        0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x70, 0x69, 0x63, 0x73, 0x2e, 0x65, 0x78,
        0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x30, 0x30, 0x2f, 0x70, 0x2f,
        0x61, 0x42, 0x6a, 0x6a, 0x6a, 0x70, 0x71, 0x50, 0x62, 0x2e, 0x70, 0x6e, 0x67, //
        0x64, // text(4)
        0x6e, 0x61, 0x6d, 0x65, // "name"
        0x76, // text(22)
        // "johnpsmith@example.com"
        0x6a, 0x6f, 0x68, 0x6e, 0x70, 0x73, 0x6d, 0x69, 0x74, 0x68, 0x40, 0x65, 0x78, 0x61, 0x6d,
        0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d, //
        0x6b, // text(11)
        0x64, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x4e, 0x61, 0x6d, 0x65, // "displayName"
        0x6d, // text(13)
        0x4a, 0x6f, 0x68, 0x6e, 0x20, 0x50, 0x2e, 0x20, 0x53, 0x6d, 0x69, 0x74,
        0x68, // "John P. Smith"
        0x05, // unsigned(5) - number of credentials
        0x01, // unsigned(1)
    ];

    const TEST_RPID_HASH_1: [u8; 32] = [
        0x62, 0x5d, 0xda, 0xdf, 0x74, 0x3f, 0x57, 0x27, 0xe6, 0x6b, 0xba, 0x8c, 0x2e, 0x38, 0x79,
        0x22, 0xd1, 0xaf, 0x43, 0xc5, 0x03, 0xd9, 0x11, 0x4a, 0x8f, 0xba, 0x10, 0x4d, 0x84, 0xd0,
        0x2b, 0xfa,
    ];

    #[test]
    fn get_assertion_serialize() {
        let response =
            Response::<&[u8], &str, MockCtapPlatform>::GetAssertion(GetAssertionResponse {
                credential: CredentialDescriptor {
                    id: TEST_CREDENTIAL_ID.to_vec(),
                    transports: None,
                },
                auth_data: AuthenticatorData {
                    rp_id_hash: Bytes::from(TEST_RPID_HASH_1),
                    flags: (Flags::UP),
                    sig_count: 0x11,
                    attested_credential: None,
                },
                user: Some(test_user_entity()),
                num_credentials: Some(1),
            });
        let mut parsed_response = [0; 7609];
        let bytes_written = response
            .parse(
                &mut MockCtapPlatform,
                Bytes::from([0; 32]),
                &mut parsed_response,
            )
            .unwrap();
        assert_eq!(
            &parsed_response[..bytes_written],
            &TEST_GET_ASSERTION_RESPONSE[..]
        );
    }

    const TEST_MAKE_CREDENTIAL_RESPONSE: [u8; 245] = [
        0x00, // status = success
        0xa3, // map(3)
        0x01, // unsigned(1)
        0x66, // text(6)
        0x70, 0x61, 0x63, 0x6b, 0x65, 0x64, // "packed"
        0x02, // unsigned(2)
        0x58, 0x94, // bytes(148)
        // authData
        // rpid hash
        0xc2, 0x89, 0xc5, 0xca, 0x9b, 0x04, 0x60, 0xf9, 0x34, 0x6a, 0xb4, 0xe4, 0x2d, 0x84, 0x27,
        0x43, 0x40, 0x4d, 0x31, 0xf4, 0x84, 0x68, 0x25, 0xa6, 0xd0, 0x65, 0xbe, 0x59, 0x7a, 0x87,
        0x05, 0x1d, //
        0x41, // flags (UP + AT)
        0x00, 0x00, 0x00, 0x0b, // signature counter (11)
        0xf8, 0xa0, 0x11, 0xf3, 0x8c, 0x0a, 0x4d, 0x15, 0x80, 0x06, 0x17, 0x11, 0x1f, 0x9e, 0xdc,
        0x7d, // aaguid
        0x00, 0x10, // length
        // credential id
        0x89, 0x59, 0xce, 0xad, 0x5b, 0x5c, 0x48, 0x16, 0x4e, 0x8a, 0xbc, 0xd6, 0xd9, 0x43, 0x5c,
        0x6f, //
        // COSE Key
        0xa5, // map(5)
        0x01, // KeyType(1)
        0x02, // ECC(2),
        0x03, // Algorithm(3)
        0x26, // ECDSA_with_SHA256(-7)
        0x20, // CurveType(-1)
        0x06, // Ed25519(6)
        0x21, // X(-2),
        0x58, 0x20, // bytes(32)
        // x-coordinate
        0xf7, 0xc4, 0xf4, 0xa6, 0xf1, 0xd7, 0x95, 0x38, 0xdf, 0xa4, 0xc9, 0xac, 0x50, 0x84, 0x8d,
        0xf7, 0x08, 0xbc, 0x1c, 0x99, 0xf5, 0xe6, 0x0e, 0x51, 0xb4, 0x2a, 0x52, 0x1b, 0x35, 0xd3,
        0xb6, 0x9a, //
        0x22, // Y(-3),
        0x58, 0x20, // bytes(32)
        // y-coordinate
        0xde, 0x7b, 0x7d, 0x6c, 0xa5, 0x64, 0xe7, 0x0e, 0xa3, 0x21, 0xa4, 0xd5, 0xd9, 0x6e, 0xa0,
        0x0e, 0xf0, 0xe2, 0xdb, 0x89, 0xdd, 0x61, 0xd4, 0x89, 0x4c, 0x15, 0xac, 0x58, 0x5b, 0xd2,
        0x36, 0x84, //
        // NOTE: This was the key given in the example,
        // but it is not encoded in the way specified in the  CTAP spec
        //        0xa3, // map(3)
        //        0x63, // text(3)
        //        0x61, 0x6c, 0x67, // "alg"
        //        0x65, // text(5)
        //        0x45, 0x53, 0x32, 0x35, 0x36, // "ES256"
        //        0x61, // text(1)
        //        0x78, // "x"
        //        0x58, 0x20, // bytes(32)
        //        0xf7, 0xc4, 0xf4, 0xa6, 0xf1, 0xd7, 0x95, 0x38, 0xdf, 0xa4, 0xc9, 0xac, 0x50, 0x84, 0x8d,
        //        0xf7, 0x08, 0xbc, 0x1c, 0x99, 0xf5, 0xe6, 0x0e, 0x51, 0xb4, 0x2a, 0x52, 0x1b, 0x35, 0xd3,
        //        0xb6, 0x9a, //
        //        0x61, // text(1)
        //        0x79, // "y"
        //        0x58, 0x20, // bytes(32)
        //        0xde, 0x7b, 0x7d, 0x6c, 0xa5, 0x64, 0xe7, 0x0e, 0xa3, 0x21, 0xa4, 0xd5, 0xd9, 0x6e, 0xa0,
        //        0x0e, 0xf0, 0xe2, 0xdb, 0x89, 0xdd, 0x61, 0xd4, 0x89, 0x4c, 0x15, 0xac, 0x58, 0x5b, 0xd2,
        //        0x36, 0x84, //
        0x03, // unsigned(3)
        0xa2, // map(2)
        0x63, // text(3)
        0x61, 0x6c, 0x67, // "alg"
        0x26, // -7 (ES256)
        0x63, // text(3)
        0x73, 0x69, 0x67, // "sig"
        0x58, 0x47, // bytes(71)
        // signature in ASN.1 DER
        0x30, // sequence
        0x45, // bytes(69)
        0x02, // integer
        0x20, // bytes(32)
        0x4a, 0x5a, 0x9d, 0xd3, 0x92, 0x98, 0x14, 0x9d, 0x90, 0x47, 0x69, 0xb5, 0x1a, 0x45, 0x14,
        0x33, 0x00, 0x6f, 0x18, 0x2a, 0x34, 0xfb, 0xdf, 0x66, 0xde, 0x5f, 0xc7, 0x17, 0xd7, 0x5f,
        0xb3, 0x50, //
        0x02, // integer
        0x21, // bytes(33) -- NOTE: Padding because the first Bytes of the Key starts with a 1 bit
        0x00, 0xa4, 0x6b, 0x8e, 0xa3, 0xc3, 0xb9, 0x33, 0x82, 0x1c, 0x6e, 0x7f, 0x5e, 0xf9, 0xda,
        0xae, 0x94, 0xab, 0x47, 0xf1, 0x8d, 0xb4, 0x74, 0xc7, 0x47, 0x90, 0xea, 0xab, 0xb1, 0x44,
        0x11, 0xe7, 0xa0, //
    ];

    const TEST_RPID_HASH_2: [u8; 32] = [
        0xc2, 0x89, 0xc5, 0xca, 0x9b, 0x04, 0x60, 0xf9, 0x34, 0x6a, 0xb4, 0xe4, 0x2d, 0x84, 0x27,
        0x43, 0x40, 0x4d, 0x31, 0xf4, 0x84, 0x68, 0x25, 0xa6, 0xd0, 0x65, 0xbe, 0x59, 0x7a, 0x87,
        0x05, 0x1d,
    ];

    // This test is failing for two reasons. First, because the COSE Key gets serialized using text
    // rather than numbers in the test data. Which is right?
    // secondly we have not figured out a way to provide the correct dummy data here.
    #[test]
    fn make_credential_serialize() {
        let response =
            Response::<&[u8], &str, MockCtapPlatform>::MakeCredential(MakeCredentialResponse {
                auth_data: AuthenticatorData {
                    rp_id_hash: Bytes::from(TEST_RPID_HASH_2),
                    flags: (Flags::AT | Flags::UP),
                    sig_count: 11,
                    attested_credential: Some(AttestedCredential {
                        credential_id: [
                            0x89, 0x59, 0xce, 0xad, 0x5b, 0x5c, 0x48, 0x16, 0x4e, 0x8a, 0xbc, 0xd6,
                            0xd9, 0x43, 0x5c, 0x6f,
                        ]
                        .to_vec(),
                        public_key: PublicKey::ed25519(
                            [
                                0xf7, 0xc4, 0xf4, 0xa6, 0xf1, 0xd7, 0x95, 0x38, 0xdf, 0xa4, 0xc9,
                                0xac, 0x50, 0x84, 0x8d, 0xf7, 0x08, 0xbc, 0x1c, 0x99, 0xf5, 0xe6,
                                0x0e, 0x51, 0xb4, 0x2a, 0x52, 0x1b, 0x35, 0xd3, 0xb6, 0x9a,
                            ],
                            [
                                0xde, 0x7b, 0x7d, 0x6c, 0xa5, 0x64, 0xe7, 0x0e, 0xa3, 0x21, 0xa4,
                                0xd5, 0xd9, 0x6e, 0xa0, 0x0e, 0xf0, 0xe2, 0xdb, 0x89, 0xdd, 0x61,
                                0xd4, 0x89, 0x4c, 0x15, 0xac, 0x58, 0x5b, 0xd2, 0x36, 0x84,
                            ],
                        ),
                    }),
                },
            });
        let mut parsed_response = [0; 7609];
        let bytes_written = response
            .parse(
                &mut MockCtapPlatform,
                Bytes::from([0; 32]),
                &mut parsed_response,
            )
            .unwrap();
        assert_eq!(
            &parsed_response[..bytes_written],
            &TEST_MAKE_CREDENTIAL_RESPONSE[..]
        );
    }
}
