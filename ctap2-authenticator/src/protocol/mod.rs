/// This module largely contains objects, that where defined in the CTAP2 or WebAuthn specification.
/// All of them are used while deserializing or serializing Requests respectively Responses.
/// Some of them are re-exported because they are used in the public API as well
pub mod authdata;
pub mod descriptor;
pub mod descriptorlist;
pub mod entity;
pub mod extensions;
pub mod key;
pub mod request;
pub mod response;
pub mod signature;
