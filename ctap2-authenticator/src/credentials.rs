//! Credential types used in the WebAuthn protocol.

use crate::protocol::entity::Entity;

/// Information about a relying party.
///
/// This can be used for display during user presence of user verification check.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct RpCredential<B: AsRef<[u8]>, S: AsRef<str>>(pub(crate) Entity<B, S>);

impl<B: AsRef<[u8]>, S: AsRef<str>> RpCredential<B, S> {
    /// # Returns
    /// The relying party ID of this relying party
    pub fn rp_id(&self) -> &B {
        &self.0.id.inner()
    }

    /// # Returns
    /// The name of this relying party
    pub fn name(&self) -> &S {
        &self.0.name
    }

    /// # Returns
    /// The icon url of this relying party, if it exists, `None` otherwise
    pub fn icon_url(&self) -> Option<&S> {
        self.0.icon.as_ref()
    }
}

/// The credentials of the account, which will be used to sign in.
///
/// This can be used for display during user presence of user verification check.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct UserCredential<B: AsRef<[u8]>, S: AsRef<str>>(pub(crate) Entity<B, S>);

impl<B: AsRef<[u8]>, S: AsRef<str>> UserCredential<B, S> {
    /// # Returns
    /// The user ID of this user credential
    pub fn user_id(&self) -> &B {
        &self.0.id.inner()
    }

    /// # Returns
    /// The name of this user.
    pub fn name(&self) -> &S {
        &self.0.name
    }

    /// # Returns
    /// The icon url of this user, if it exists, `None` otherwise
    pub fn icon_url(&self) -> Option<&S> {
        self.0.icon.as_ref()
    }

    /// # Returns
    /// The display name of this user, if it exists, `None` otherwise
    pub fn display_name(&self) -> Option<&S> {
        self.0.display_name.as_ref()
    }
}
