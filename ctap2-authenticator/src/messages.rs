//! Messages which are passed between the the transport layer and the application layer

use crate::usbhid::ChannelId;

/// A [`Fido2Request`](struct.Fido2Request.html) containing either a U2F message or a CTAP2 Cbor
/// request to be processed by the authenticator
pub struct Fido2Request<B: AsMut<[u8]>>(pub(crate) Fido2Internal<B>);

/// A [`Fido2Response`](struct.Fido2Response.html) to be sent out to the client.
pub struct Fido2Response<B: AsMut<[u8]>>(pub(crate) Fido2Internal<B>);

/// Internal representation of Fido2 data passed between the transport layer and the application
/// layer.
// NOTE: We want to reuse the buffer with the data. Thus we internally return the full slice and
// store the length of the message in an extra variable
#[derive(Debug, PartialEq, Eq)]
pub(crate) enum Fido2Internal<B: AsMut<[u8]>> {
    U2F {
        channel_id: ChannelId,
        buf: B,
        length: u16,
    },
    Ctap2 {
        channel_id: ChannelId,
        buf: B,
        length: u16,
    },
    Probe {
        channel_id: ChannelId,
        buf: B,
        length: u16,
    },
}
