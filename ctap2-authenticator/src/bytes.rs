use core::ops::{Deref, DerefMut};
use core::ops::{Index, Range};
use serde::de::{Error, Visitor};
use serde::export::Formatter;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

/// This is an internally used [`Bytes`] type.
/// It is essentially just a wrapper around AsRef<[u8]> but with a custom
/// [`Serializer`](serde::Serializer) and [`Deserializer`](serde::Deserializer) implementation.
/// This is necessary because [`serde`] wants to serialize and deserialize Arrays to a sequence
/// of elements. But we want to serialize them to byte strings.
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct Bytes<B>(B);

impl<'a> Copy for Bytes<&'a [u8]> {}

impl<B> Bytes<B> {
    pub(crate) fn from(b: B) -> Self {
        Bytes(b)
    }

    pub(crate) fn inner(&self) -> &B {
        &self.0
    }
}

impl<B: AsRef<[u8]>> Deref for Bytes<B> {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

impl<B: AsRef<[u8]> + AsMut<[u8]>> DerefMut for Bytes<B> {
    fn deref_mut(&mut self) -> &mut [u8] {
        self.0.as_mut()
    }
}

// NOTE: This does not return an Index into a Bytes type but into an underlying type.
// This requires Byteslicing to call Bytes::from(bytes[5..10])
// We would need to implement Index on Bytes in order to do this correctly, however the methods
// on index are nightly only at the moment.
impl<B: Index<Range<usize>>> Index<Range<usize>> for Bytes<B> {
    type Output = <B as Index<Range<usize>>>::Output;

    #[inline]
    fn index(&self, index: Range<usize>) -> &Self::Output {
        Index::index(self.inner(), index)
    }
}

impl<B: AsRef<[u8]>> Serialize for Bytes<B> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_bytes(&self.0.as_ref())
    }
}

/// This visitor works only on borrowed data. Thus it can only produce slices and the output data
/// can not outlive the buffer, because it contains references to it.
struct BorrowedBytesVisitor;
impl<'de> Visitor<'de> for BorrowedBytesVisitor {
    type Value = Bytes<&'de [u8]>;

    fn expecting(&self, formatter: &mut Formatter) -> core::fmt::Result {
        formatter.write_str("a byte string")
    }

    fn visit_borrowed_bytes<E>(self, v: &'de [u8]) -> Result<Self::Value, E>
    where
        E: Error,
    {
        Ok(Bytes::from(v))
    }
}

impl<'de> Deserialize<'de> for Bytes<&'de [u8]> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_bytes(BorrowedBytesVisitor)
    }
}
