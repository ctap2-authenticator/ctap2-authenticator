# TODO List

This is a irregularly updated list of things, that still need to be
done in the project. Short-term TODO items will not be listed here.

## Mid-term TODOs

-[ ] API improvements
    -[ ] Hand `ChannelID` to `attest()` and `sign()` functions
    -[x] Hand `rp_id` to `any_credential_exist`
    -[ ] Make the Attest non constant and hand it down in the `attest()` function.
        There are applications of the library, where the attest can change over time.
    -[ ] Improvement of the `usbhid` transport. Currently it is easy to use it wrong.  
-[ ] Cleanup
    -[ ] Better Debug output of some structures
        -[ ] `CborRequest`
        -[ ] `DescriptorList` 

## Long-term TODOs

These things are needed by a proper CTAP2 implementation but
are not part of the MVP.

-[ ] Pin authentication
    -[ ] Packet parsing
    -[ ] Handing of packets
-[ ] Extensions
    -[ ] Parsing Extensions
    -[ ] API to implement Extensions
    -[ ] Implement HMAC
-[ ] Other Transport protocols
    -[ ] NFC
    -[ ] Bluetooth 
-[ ] U2F backwards compatibility
