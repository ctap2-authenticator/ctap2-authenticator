use ecdsa::{Error, PublicKey};
use ecdsa::hazmat::SignPrimitive;
use generic_array::GenericArray;
use hmac::{Hmac, Mac, NewMac};
use p256::{ProjectivePoint, Scalar};
use p256::ecdsa::Signature;
use sha2::{Digest, Sha256};
use subtle::{CtOption, ConditionallySelectable, Choice};

type HmacSha256 = Hmac<Sha256>;

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub struct PrivateKey(Scalar);

impl ConditionallySelectable for PrivateKey {
    fn conditional_select(a: &Self, b: &Self, choice: Choice) -> Self {
        Self(ConditionallySelectable::conditional_select(&a.0, &b.0, choice))
    }
}

impl Default for PrivateKey {
    fn default() -> Self {
        Self(Scalar::default())
    }
}

impl PrivateKey {
    pub fn from_bytes(bytes: &[u8; 32]) -> CtOption<Self> {
        Scalar::from_bytes(bytes).map(|s| Self(s))
    }

    pub fn to_bytes(&self) -> [u8; 32] {
        self.0.to_bytes()
    }

    // TODO: Should be named invalid?
    pub fn one() -> Self {
        Self(Scalar::one())
    }

    pub fn pub_key(&self) -> [u8; 64] {
        // TODO: Is this right?
        let p = (&ProjectivePoint::generator() * &self.0)
            .to_affine()
            .unwrap()
            .to_pubkey(false);

        match p {
            PublicKey::Uncompressed(p) => {
                let mut res: [u8; 64] = [0; 64];
                let pb = p.as_bytes();
                // NOTE: We remove the leading zero here
                res.clone_from_slice(&pb[1..]);
                res
            }
            _ => panic!(),
        }
    }

    pub fn try_sign_deterministic(
        &self,
        msg: &[u8],
        masking_scalar: Option<&Scalar>,
    ) -> Result<Signature, Error> {
        let msg_hash = Sha256::digest(msg);

        let maybe_k = derive_k(&self.0, msg_hash.as_ref());
        let k = maybe_k.unwrap_or(Scalar::zero());

        self.0.try_sign_prehashed(&k, masking_scalar, GenericArray::from_slice(msg_hash.as_ref()))
    }
}

/// This is the deterministic array derivation algorithm as specified in RFC 6979
/// https://tools.ietf.org/html/rfc6979
fn derive_k(private_key: &Scalar, msg_hash: &[u8; 32]) -> CtOption<Scalar> {
    // a. message is already hashed

    // b.
    let v: [u8; 32] = [0x01; 32];

    // c.
    let k: [u8; 32] = [0x00; 32];

    // d.
    let mut hmac_k = HmacSha256::new_varkey(&k).unwrap();
    hmac_k.update(&v);
    hmac_k.update(&[0x00]);
    hmac_k.update(&private_key.to_bytes());
    hmac_k.update(msg_hash);
    let k = hmac_k.finalize().into_bytes();

    // e.
    let mut hmac_k = HmacSha256::new_varkey(&k).unwrap();
    hmac_k.update(&v);
    let v = hmac_k.finalize().into_bytes();

    // f.
    let mut hmac_k = HmacSha256::new_varkey(&k).unwrap();
    hmac_k.update(&v);
    hmac_k.update(&[0x01]);
    hmac_k.update(&private_key.to_bytes());
    hmac_k.update(msg_hash);
    let k = hmac_k.finalize().into_bytes();

    // g.
    let mut hmac_k = HmacSha256::new_varkey(&k).unwrap();
    hmac_k.update(&v);
    let v = hmac_k.finalize().into_bytes();

    // h. We are only doing one step.
    let mut hmac_k = HmacSha256::new_varkey(&k).unwrap();
    hmac_k.update(&v);
    let v = hmac_k.finalize().into_bytes();

    Scalar::from_bytes(&v.as_ref())
}

#[cfg(test)]
mod test {
    use p256::Scalar;
    use sha2::{Digest, Sha256};

    use crate::ct_sign::{derive_k, PrivateKey};

    const X: [u8; 32] = [
        0xC9, 0xAF, 0xA9, 0xD8, 0x45, 0xBA, 0x75, 0x16, 0x6B, 0x5C, 0x21, 0x57, 0x67, 0xB1, 0xD6,
        0x93, 0x4E, 0x50, 0xC3, 0xDB, 0x36, 0xE8, 0x9B, 0x12, 0x7B, 0x8A, 0x62, 0x2B, 0x12, 0x0F,
        0x67, 0x21,
    ];

    const SAMPLE_K: [u8; 32] = [
        0xA6, 0xE3, 0xC5, 0x7D, 0xD0, 0x1A, 0xBE, 0x90, 0x08, 0x65, 0x38, 0x39, 0x83, 0x55, 0xDD,
        0x4C, 0x3B, 0x17, 0xAA, 0x87, 0x33, 0x82, 0xB0, 0xF2, 0x4D, 0x61, 0x29, 0x49, 0x3D, 0x8A,
        0xAD, 0x60,
    ];

    const SAMPLE_R: [u8; 32] = [
        0xEF, 0xD4, 0x8B, 0x2A, 0xAC, 0xB6, 0xA8, 0xFD, 0x11, 0x40, 0xDD, 0x9C, 0xD4, 0x5E, 0x81,
        0xD6, 0x9D, 0x2C, 0x87, 0x7B, 0x56, 0xAA, 0xF9, 0x91, 0xC3, 0x4D, 0x0E, 0xA8, 0x4E, 0xAF,
        0x37, 0x16,
    ];

    const SAMPLE_S: [u8; 32] = [
        0xF7, 0xCB, 0x1C, 0x94, 0x2D, 0x65, 0x7C, 0x41, 0xD4, 0x36, 0xC7, 0xA1, 0xB6, 0xE2, 0x9F,
        0x65, 0xF3, 0xE9, 0x00, 0xDB, 0xB9, 0xAF, 0xF4, 0x06, 0x4D, 0xC4, 0xAB, 0x2F, 0x84, 0x3A,
        0xCD, 0xA8,
    ];

    #[test]
    fn test_k_sample() {
        let msg_hash = Sha256::digest("sample".as_bytes());
        let k = derive_k(&Scalar::from_bytes(&X).unwrap(), msg_hash.as_ref())
            .unwrap()
            .to_bytes();
        assert_eq!(k[..], SAMPLE_K[..]);
    }

    #[test]
    fn test_r_s_sample() {
        let private_key = PrivateKey::from_bytes(&X).unwrap();
        let sig = private_key.try_sign_deterministic("sample".as_bytes(), None).unwrap();
        assert_eq!(&sig.r().to_vec()[..], &SAMPLE_R[..]);
        assert_eq!(&sig.s().to_vec()[..], &SAMPLE_S[..]);
    }


    const TEST_K: [u8; 32] = [
        0xD1, 0x6B, 0x6A, 0xE8, 0x27, 0xF1, 0x71, 0x75, 0xE0, 0x40, 0x87, 0x1A, 0x1C, 0x7E, 0xC3,
        0x50, 0x01, 0x92, 0xC4, 0xC9, 0x26, 0x77, 0x33, 0x6E, 0xC2, 0x53, 0x7A, 0xCA, 0xEE, 0x00,
        0x08, 0xE0
    ];

    const TEST_R: [u8; 32] = [
        0xF1, 0xAB, 0xB0, 0x23, 0x51, 0x83, 0x51, 0xCD, 0x71, 0xD8, 0x81, 0x56, 0x7B, 0x1E, 0xA6,
        0x63, 0xED, 0x3E, 0xFC, 0xF6, 0xC5, 0x13, 0x2B, 0x35, 0x4F, 0x28, 0xD3, 0xB0, 0xB7, 0xD3,
        0x83, 0x67
    ];

    const TEST_S: [u8; 32] = [
        0x01, 0x9F, 0x41, 0x13, 0x74, 0x2A, 0x2B, 0x14, 0xBD, 0x25, 0x92, 0x6B, 0x49, 0xC6, 0x49,
        0x15, 0x5F, 0x26, 0x7E, 0x60, 0xD3, 0x81, 0x4B, 0x4C, 0x0C, 0xC8, 0x42, 0x50, 0xE4, 0x6F,
        0x00, 0x83
    ];


    #[test]
    fn test_k_test() {
        let msg_hash = Sha256::digest("test".as_bytes());
        let k = derive_k(&Scalar::from_bytes(&X).unwrap(), msg_hash.as_ref())
            .unwrap()
            .to_bytes();
        assert_eq!(k[..], TEST_K[..]);
    }

    #[test]
    fn test_r_s_test() {
        let private_key = PrivateKey::from_bytes(&X).unwrap();
        let sig = private_key.try_sign_deterministic("test".as_bytes(), None).unwrap();
        assert_eq!(&sig.r().to_vec()[..], &TEST_R[..]);
        assert_eq!(&sig.s().to_vec()[..], &TEST_S[..]);
    }
}
