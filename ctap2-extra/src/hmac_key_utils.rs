use core::convert::TryInto;
use core::fmt::{Debug, Error, Formatter};

use ctap2_authenticator::PublicKey;
#[cfg(not(any(feature = "alloc", feature = "std")))]
pub(crate) use fixed_length::*;
#[cfg(not(feature = "ct-sign"))]
use nisty::Keypair;
#[cfg(any(feature = "alloc", feature = "std"))]
pub(crate) use var_length::*;

#[cfg(feature = "ct-sign")]
use crate::ct_sign::PrivateKey;

/// Data structure used inside HmacKey
#[derive(Debug, Clone)]
pub(crate) struct HmacCredentialData {
    pub(crate) credential: HmacKeyCredential,
    #[cfg(not(feature = "ct-sign"))]
    pub(crate) key: Keypair,
    #[cfg(feature = "ct-sign")]
    pub(crate) key: PrivateKey,
    pub(crate) counter_id: u32,
    pub(crate) counter_offset: u32,
}

impl HmacCredentialData {
    #[cfg(not(feature = "ct-sign"))]
    pub(crate) fn get_pubkey(&self) -> PublicKey<[u8; 32]> {
        PublicKey::nistp256(
            self.key.public.as_bytes()[..32].try_into().unwrap(),
            self.key.public.as_bytes()[32..].try_into().unwrap(),
        )
    }

    #[cfg(feature = "ct-sign")]
    pub(crate) fn get_pubkey(&self) -> PublicKey<[u8; 32]> {
        let key = self.key.pub_key();
        PublicKey::nistp256(
            key[..32].try_into().unwrap(),
            key[32..].try_into().unwrap(),
        )
    }
}

#[derive(Clone, Copy)]
pub struct HmacKeyCredential(pub(crate) [u8; 40]);

impl HmacKeyCredential {
    pub(crate) fn new(mac: &[u8; 32], nonce: [u8; 8]) -> Self {
        let mut new = [0; 40];
        new[..32].copy_from_slice(&mac[..]);
        new[32..].copy_from_slice(&nonce[..]);
        Self(new)
    }
}

impl AsRef<[u8]> for HmacKeyCredential {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl Debug for HmacKeyCredential {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "HmacKeyCredential {{ mac: {:?}, nonce: {:?} }}",
            &self.0[..32],
            &self.0[32..]
        )
    }
}

impl PartialEq for HmacKeyCredential {
    fn eq(&self, other: &Self) -> bool {
        self.0[..] == other.0[..]
    }
}

impl Eq for HmacKeyCredential {}

#[cfg(any(feature = "alloc", feature = "std"))]
mod var_length {
    // If we use alloc but not std, we need to include the Vector implementation manually
    #[cfg(all(feature = "alloc", not(feature = "std")))]
    use alloc::vec::Vec;

    // Unimplemented: We can implement this better if we use IntoIterator
    use crate::hmac_key_utils::HmacKeyCredential;

    pub struct HmacCredentialIterator {
        index: usize,
        vec: Vec<(HmacKeyCredential, u32)>,
    }

    impl HmacCredentialIterator {
        pub(crate) fn new() -> Self {
            Self {
                index: 0,
                vec: Vec::new(),
            }
        }

        pub(crate) fn push(
            &mut self,
            credential: HmacKeyCredential,
            counter: u32,
        ) -> Result<(), ()> {
            self.vec.push((credential, counter));
            Ok(())
        }

        pub(crate) fn len(&self) -> usize {
            self.vec.len()
        }
    }

    impl Iterator for HmacCredentialIterator {
        type Item = (HmacKeyCredential, u32);

        fn next(&mut self) -> Option<Self::Item> {
            if self.index < self.vec.len() {
                self.index += 1;
                Some(self.vec[self.index - 1])
            } else {
                None
            }
        }
    }
}

#[cfg(not(any(feature = "alloc", feature = "std")))]
mod fixed_length {
    use crate::hmac_key_utils::HmacKeyCredential;

    const ITERATOR_MAX_LENGTH: usize = 8;

    pub struct HmacCredentialIterator {
        index: usize,
        length: usize,
        list: [Option<(HmacKeyCredential, u32)>; ITERATOR_MAX_LENGTH],
    }

    impl HmacCredentialIterator {
        pub(crate) fn new() -> Self {
            Self {
                index: 0,
                length: 0,
                list: [None; ITERATOR_MAX_LENGTH],
            }
        }

        pub(crate) fn push(
            &mut self,
            credential: HmacKeyCredential,
            counter: u32,
        ) -> Result<(), ()> {
            if self.length < ITERATOR_MAX_LENGTH {
                self.list[self.length] = Some((credential, counter));
                self.length += 1;
                Ok(())
            } else {
                Err(())
            }
        }

        pub(crate) fn len(&self) -> usize {
            self.length
        }
    }

    impl Iterator for HmacCredentialIterator {
        type Item = (HmacKeyCredential, u32);

        fn next(&mut self) -> Option<Self::Item> {
            if self.list[self.index].is_some() {
                self.index += 1;
                self.list[self.index - 1]
            } else {
                None
            }
        }
    }
}
