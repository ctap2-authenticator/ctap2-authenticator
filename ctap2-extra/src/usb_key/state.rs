use core::sync::atomic::{AtomicUsize, Ordering};

// UNIMPLEMENTED: A version that does not use atomics and can be used on a Cortex-M0 device
mod app_state {
    pub const IDLE: usize = 0x00;
    //pub const RESETTING: usize = 0x01;
    pub const PROCESSING: usize = 0x02;
    pub const WAIT_UP: usize = 0x03;
    pub const WINKING: usize = 0x04;
    pub const CANCELING: usize = 0x05;
}

/// Represents the current state of the authenticator.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AppState {
    /// The authenticator is idling and awaiting requests.
    Idle,

    /// The authenticator is currently processing a request.
    Processing,

    /// The authenticator is waiting for the user to press the button.
    WaitUp,

    /// The authenticator is winking.
    Winking,

    /// A request was cancelled, but the application side needs to finish the processing and return
    /// the buffer.
    Canceling,
}

impl From<AppState> for usize {
    fn from(app_state: AppState) -> Self {
        match app_state {
            AppState::Idle => app_state::IDLE,
            AppState::Processing => app_state::PROCESSING,
            AppState::WaitUp => app_state::WAIT_UP,
            AppState::Winking => app_state::WINKING,
            AppState::Canceling => app_state::CANCELING,
        }
    }
}

impl From<usize> for AppState {
    fn from(app_state: usize) -> Self {
        match app_state {
            app_state::IDLE => AppState::Idle,
            app_state::PROCESSING => AppState::Processing,
            app_state::WAIT_UP => AppState::WaitUp,
            app_state::WINKING => AppState::Winking,
            app_state::CANCELING => AppState::Canceling,
            _ => unreachable!(),
        }
    }
}

static STATE: AtomicUsize = AtomicUsize::new(app_state::IDLE);
static TIME: AtomicUsize = AtomicUsize::new(0);

/// Transitions to a state
///
/// # Arguments
/// - `from` if specified, only transition if this is the current state. Otherwise the state
/// transition is unconditional.
/// - `to` the state to transition to
///
/// # Returns
/// - `Ok(())` if transition was successful
/// - `Err(())` otherwise
pub fn transition_state(from: Option<AppState>, to: AppState) -> Result<(), ()> {
    match from {
        Some(from) => {
            match STATE.compare_exchange(from.into(), to.into(), Ordering::SeqCst, Ordering::SeqCst)
            {
                Ok(_) => Ok(()),
                Err(_) => Err(()),
            }
        }
        None => {
            STATE.store(to.into(), Ordering::Release);
            Ok(())
        }
    }
}

/// Get the current state of the app
///
/// # Returns
/// The current [`AppState`](struct.AppState.html)
pub fn get_state() -> AppState {
    STATE.load(Ordering::Acquire).into()
}

/// Increase the timer by 1.
///
/// This function should be called once per millisecond
///
/// # Returns
/// The current time in milliseconds
pub fn time_tick() -> usize {
    TIME.fetch_add(1, Ordering::Release) + 1
}

/// Get the current time
///
/// # Returns
/// The current time in milliseconds
pub fn get_time() -> usize {
    TIME.load(Ordering::Acquire)
}
