use ctap2_authenticator::messages::{Fido2Request, Fido2Response};
use ctap2_authenticator::{Authenticator, AuthenticatorPlatform};
use heapless::consts;
use heapless::spsc::{Consumer, Producer};

/// The part of the app that is kept in application context.
///
/// This struct abstracts over the actual CTAP2 implementation. Since there are heavy computations
/// and possibly blocking callbacks inside the implementation, the
/// [UsbContext](struct.UsbContext.html) is separated out to be managed separately.
pub struct AppContext<P: AuthenticatorPlatform> {
    pub(crate) authenticator: Authenticator<P>,
    pub(crate) from_usb: Consumer<'static, Fido2Request<&'static mut [u8]>, consts::U1, u8>,
    pub(crate) to_usb: Producer<'static, Fido2Response<&'static mut [u8]>, consts::U1, u8>,
}

impl<P: AuthenticatorPlatform> AppContext<P> {
    /// Processes a request if there is one and sends it back to the
    /// [`UsbContext`](struct.UsbContext.html)
    ///
    /// This function needs to be called from time to time to process the application.
    /// It can be called on an interval or just after every Usb poll.
    ///
    /// # Returns
    /// - `true` if a request was processed
    /// - `false` otherwise
    pub fn poll(&mut self) -> bool {
        match self.from_usb.dequeue() {
            Some(request) => {
                let response = self.authenticator.process(request);
                match self.to_usb.enqueue(response) {
                    Ok(_) => true,
                    // This should not be possible, since we can only come here if we have received
                    // a message from the UsbContext. If this triggers, there is a control flow
                    // bug in the UsbContext
                    Err(_) => core::panic!(),
                }
            }
            None => false,
        }
    }
}
