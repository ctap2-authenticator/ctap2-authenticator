use crate::usb_key::state::{get_state, get_time, transition_state, AppState};
use ctap2_authenticator::messages::{Fido2Request, Fido2Response};
use ctap2_authenticator::usbhid::{
    CtapHidCapabilities, CtapHidPlatform, KeepaliveResponse, TransactionProcessor,
};
use heapless::consts;
use heapless::spsc::{Consumer, Producer};
use usb_device::bus::UsbBus;
use usb_device::device::UsbDevice;
use usbd_hid::hid_class::HIDClass;
use usbd_hid::UsbError;

/// The part of the app that needs to run in an interrupt context.
///
/// This struct abstracts over a USB device and manages all the necessary USB interactions.
/// The implementor is responsible to call the poll function at least once every 10 ms.
pub struct UsbContext<B: UsbBus + 'static> {
    pub(crate) dev: UsbDevice<'static, B>,
    pub(crate) hid: HIDClass<'static, B>,
    pub(crate) tp: TransactionProcessor<&'static mut [u8], UsbKeyHidPlatform>,
    pub(crate) from_app: Consumer<'static, Fido2Response<&'static mut [u8]>, consts::U1, u8>,
    pub(crate) to_app: Producer<'static, Fido2Request<&'static mut [u8]>, consts::U1, u8>,
    pub(crate) buf: Option<[u8; 64]>,
}

impl<B: UsbBus> UsbContext<B> {
    /// Poll the USB Bus for data and process it
    ///
    /// This function polls the USB bus. It manages the the sending and receiving and sending of
    /// data to the client as well as sending and receiving data from the
    /// [`AppContext`](struct.AppContext.html).
    ///
    /// # Returns
    /// - `true` if data was sent or received from the client.
    /// - `false` otherwise
    pub fn poll(&mut self) -> bool {
        // Check if we have pending data in the buffer
        if let Some(data) = self.buf {
            match self.hid.push_raw_input(&data) {
                Ok(size) => {
                    assert_eq!(size, 64);
                    self.buf = None;
                    return true;
                }
                // We can't continue driving the transaction processor if we still have data pending
                // FIXME: Should be return false
                Err(UsbError::WouldBlock) => return true,
                Err(e) => core::panic!("{:?}", e),
            }
        }

        // Poll the usb
        let polled = self.dev.poll(&mut [&mut self.hid]);

        // If the usb was polled, try to extract the data that we have read.
        let mut data: [u8; 64] = [0; 64];
        let data = if polled {
            // Check if there is data
            match self.hid.pull_raw_output(&mut data) {
                Ok(size) => {
                    assert_eq!(size, 64);
                    Some(&data)
                }
                // If there is an error, we just try again later
                Err(UsbError::WouldBlock) => None,
                Err(e) => core::panic!("{:?}", e),
            }
        } else {
            None
        };

        // Send data to the processor
        let (maybe_request, maybe_data) = self.tp.poll(data);

        // Send the request to the application side, if it exits
        // Throw the request away if there is no way to process it
        if let Some(request) = maybe_request {
            transition_state(Some(AppState::Idle), AppState::Processing).unwrap();
            self.to_app.enqueue(request).unwrap_or(());
        }

        // Send data back if there is some
        if let Some(data) = maybe_data {
            match self.hid.push_raw_input(&data) {
                Ok(size) => assert_eq!(size, 64),
                // If we can't send data, we store it here
                Err(UsbError::WouldBlock) => self.buf = Some(data),
                Err(e) => core::panic!("{:?}", e),
            }
        }

        // Check if we have data to put into the transaction processor
        if let Some(response) = self.from_app.dequeue() {
            transition_state(Some(AppState::Processing), AppState::Idle).unwrap();
            self.tp.response(response);
        }

        polled
    }
}

/// This is the provided implementation of `CtapHidPlatform` to be used in the `UsbContext`
pub(crate) struct UsbKeyHidPlatform {
    timer: usize,
    last_keepalive: usize,
}

impl UsbKeyHidPlatform {
    pub fn new() -> Self {
        let now = get_time();
        Self {
            timer: now,
            last_keepalive: now,
        }
    }
}

impl CtapHidPlatform for UsbKeyHidPlatform {
    // Should be kept in sink with the Crates version if possible
    const MAJOR_VERSION: u8 = 0;
    const MINOR_VERSION: u8 = 0;
    const BUILD_VERSION: u8 = 0;
    const CAPABILITIES: CtapHidCapabilities = CtapHidCapabilities {
        wink: true,
        cbor: true,
        msg: false,
    };

    fn wink(&mut self) {
        let _ = transition_state(Some(AppState::Idle), AppState::Winking);
    }

    fn cancel(&mut self) {
        transition_state(Some(AppState::Processing), AppState::Canceling).unwrap();
    }

    fn keepalive_needed(&mut self) -> KeepaliveResponse {
        let now = get_time();
        let state = get_state();

        if now - self.last_keepalive > 85 {
            self.last_keepalive = now;

            // If it is time, we check what state we are in and decide if we send a keepalive
            match state {
                AppState::Processing => KeepaliveResponse::Processing,
                AppState::WaitUp => KeepaliveResponse::UpNeeded,
                _ => KeepaliveResponse::None,
            }
        } else {
            KeepaliveResponse::None
        }
    }

    fn start_timer(&mut self) {
        self.timer = get_time();
    }

    fn has_timed_out(&mut self) -> bool {
        let now = get_time();

        // Check that the transaction is still active.
        // It should time out between 400 and 800 ms without packets
        now - self.timer > 800
    }
}
