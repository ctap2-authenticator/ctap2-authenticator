use crate::usb_key::transport::UsbKeyHidPlatform;
pub use app::AppContext;
use ctap2_authenticator::messages::{Fido2Request, Fido2Response};
use ctap2_authenticator::usbhid::TransactionProcessor;
use ctap2_authenticator::{Authenticator, AuthenticatorPlatform};
use heapless::consts;
use heapless::spsc::Queue;
pub use state::{get_state, get_time, time_tick, transition_state, AppState};
pub use transport::UsbContext;
use usb_device::bus::{UsbBus, UsbBusAllocator};
use usb_device::device::UsbDeviceBuilder;
use usb_device::prelude::UsbVidPid;
use usbd_hid::hid_class::HIDClass;

mod app;
mod state;
mod transport;

// from https://github.com/nickray/usbd-ctaphid/blob/main/src/class.rs
const FIDO_HID_REPORT_DESCRIPTOR_LENGTH: usize = 34;
#[rustfmt::skip]
const FIDO_HID_REPORT_DESCRIPTOR: [u8; FIDO_HID_REPORT_DESCRIPTOR_LENGTH] = [
    // Usage page (vendor defined): 0xF1D0 (FIDO_USAGE_PAGE)
    0x06, 0xD0, 0xF1,
    // Usage ID (vendor defined): 0x1 (FIDO_USAGE_CTAPHID)
    0x09, 0x01,

    // Collection (application)
    0xA1, 0x01,

    // The Input report
    0x09, 0x20,        // Usage ID - vendor defined: FIDO_USAGE_DATA_IN
    0x15, 0x00,        // Logical Minimum (0)
    0x26, 0xFF, 0x00,  // Logical Maximum (255)
    0x75, 0x08,        // Report Size (8 bits)
    0x95, 0x40,        // Report Count (64 fields)
    0x81, 0x02,        // Input (Data, Variable, Absolute)

    // The Output report
    0x09, 0x21,        // Usage ID - vendor defined: FIDO_USAGE_DATA_OUT
    0x15, 0x00,        // Logical Minimum (0)
    0x26, 0xFF, 0x00,  // Logical Maximum (255)
    0x75, 0x08,        // Report Size (8 bits)
    0x95, 0x40,        // Report Count (64 fields)
    0x91, 0x02,        // Output (Data, Variable, Absolute)

    // EndCollection
    0xC0,
];

/// Used to build `UsbKeys`
pub struct UsbKeyBuilder {
    vid_pid: UsbVidPid,
    release: u16,
    self_powered: bool,
    power: usize,
    manufacturer: Option<&'static str>,
    product: Option<&'static str>,
    serial: Option<&'static str>,
    poll_ms: u8, // TODO: Setup logging and probing related things
}

impl UsbKeyBuilder {
    /// Creates a new [`UsbKeyBuilder`](struct.UsbKeyBuilder.html)
    pub fn new(vid: u16, pid: u16) -> Self {
        Self {
            vid_pid: UsbVidPid(vid, pid),
            release: 0x0010,
            self_powered: false,
            power: 100,
            manufacturer: None,
            product: None,
            serial: None,
            poll_ms: 1,
        }
    }

    /// Sets the device release version in BCD.
    ///
    /// Default: `0x0010` (signifies version "0.1")
    pub fn device_release(mut self, device_release: u16) -> Self {
        self.release = device_release;
        self
    }

    /// Sets whether the device may have an external power source.
    ///
    /// This should be set to `true` even if the device is sometimes self-powered and may not
    /// always draw power from the USB bus.
    ///
    /// Default: `false`
    ///
    /// See also: [`max_power`](struct.UsbKeyBuilder.html#method.max_power)
    pub fn self_powered(mut self, self_powered: bool) -> Self {
        self.self_powered = self_powered;
        self
    }

    /// Sets the maximum current drawn from the USB bus by the device in milliamps.
    ///
    /// The default is 100 mA. If your device always uses an external power source and never draws
    /// power from the USB bus, this can be set to 0.
    ///
    /// See also: [`self_powered`](struct.UsbKeyBuilder.html#method.self_powered)
    ///
    /// Default: 100mA
    pub fn max_power(mut self, max_power_ma: usize) -> Self {
        self.power = max_power_ma;
        self
    }

    /// Sets the manufacturer name string descriptor.
    ///
    /// Default: `None`
    pub fn manufacturer(mut self, manufacturer: &'static str) -> Self {
        self.manufacturer = Some(manufacturer);
        self
    }

    /// Sets the product name string descriptor.
    ///
    /// Default: `None`
    pub fn product(mut self, product: &'static str) -> Self {
        self.product = Some(product);
        self
    }

    /// Sets the serial number string descriptor.
    ///
    /// Default: `None`
    pub fn serial_number(mut self, serial_number: &'static str) -> Self {
        self.serial = Some(serial_number);
        self
    }

    /// Sets the interval in which to poll the hid device. Must be a value between 1 and 255.
    /// A value of 0 is ignored.
    ///
    /// Default: `1`
    pub fn poll_ms(mut self, ms: u8) -> Self {
        if ms != 0 {
            self.poll_ms = ms;
        }
        self
    }

    /// Builds a new `UsbKey` from this builder.
    ///
    /// # Arguments
    /// - `alloc`: The USB platform allocator to use in this `UsbKey`. Intitializing a platform
    /// allocator is highly platform dependent. Refere to the
    /// [`UsbBus`](https://docs.rs/usb-device/0.2.5/usb_device/bus/trait.UsbBus.html) implementation
    /// of your platform on how to acquire one.
    /// - `platform`: The `AuthenticatorPlatform` implementation to use.
    ///
    /// # Note
    /// The `UsbKey` uses static memory and there can only be one in the system.
    /// This function returns an error if the `UsbKey` was already initialized.
    pub fn try_build<B: UsbBus, P: AuthenticatorPlatform>(
        self,
        platform: P,
        alloc: &'static UsbBusAllocator<B>,
    ) -> Result<(UsbContext<B>, AppContext<P>), ()> {
        // NOTE: usb_hid needs to come before usb_device, otherwise there will be a runtime error
        let hid = HIDClass::new(alloc, &FIDO_HID_REPORT_DESCRIPTOR, self.poll_ms);

        //let logger = UsbLogger::new(alloc, self.log_level);

        // Initialize the USB device
        let mut dev_builder = UsbDeviceBuilder::new(alloc, self.vid_pid)
            .device_release(self.release)
            .self_powered(self.self_powered)
            .max_power(self.power);

        if let Some(manufacturer) = self.manufacturer {
            dev_builder = dev_builder.manufacturer(manufacturer);
        }
        if let Some(product) = self.product {
            dev_builder = dev_builder.product(product);
        }
        if let Some(serial) = self.serial {
            dev_builder = dev_builder.serial_number(serial);
        }

        let dev = dev_builder.build();

        // Initialize the queues to send data across
        let request_queue = cortex_m::singleton!(: Queue<Fido2Request<&'static mut [u8]>, consts::U1, u8> = Queue::u8()).ok_or(())?;
        let (to_app, from_usb) = request_queue.split();

        let response_queue = cortex_m::singleton!(: Queue<Fido2Response<&'static mut [u8]>, consts::U1, u8> = Queue::u8()).ok_or(())?;
        let (to_usb, from_app) = response_queue.split();

        // Initialize the authenticator
        let authenticator = Authenticator::create(platform);
        let app_side = AppContext {
            authenticator,
            from_usb,
            to_usb,
        };

        // Initialize the usb_key side
        let buffer: &'static mut [u8] = &mut cortex_m::singleton!(: [u8; 2048] = [0; 2048]).ok_or(())?[..];
        let tp = TransactionProcessor::new(buffer, UsbKeyHidPlatform::new());
        let usb_side = UsbContext {
            dev,
            hid,
            tp,
            from_app,
            to_app,
            buf: None,
        };

        Ok((usb_side, app_side))
    }
}
