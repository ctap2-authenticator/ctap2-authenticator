//! An implementation of the `HMAC` based credential encryption scheme on
//!
//! This is an opinionated implementation of [`CtapPlatform`]()
//! which uses `NIST P256` encryption and embeds the secret key into the credential id via
//! a `sha256 HMAC`. That way, the authenticator itself only needs to store a device secret of
//! fixed length as well as the counters.
//!
//! Since we can't store a counter for every credential, we use a counter pool.
//! We deduce from the credential which counter to use for the credential as well as an offset to
//! the counter.
//!
//! There are still hardware dependent parts to implement. These are abstracted via
//! [`HmacKeyPlatform`](trait.HmacKeyPlatform.html).
//!
//! This way indefinitely many credentials can be stored.

use core::convert::TryInto;
use core::fmt::Debug;
use core::ops::BitAnd;
use core::time::Duration;

use hmac::{Hmac, Mac};
use hmac::crypto_mac::generic_array::GenericArray;
use hmac::crypto_mac::NewMac;
use hmac::crypto_mac::Output;
use sha2::{Sha256, Sha512};

use ctap2_authenticator::{
    AuthenticatorPlatform, CredentialDescriptorList, CtapOptions, PublicKey, Signature,
};
use ctap2_authenticator::credentials::{RpCredential, UserCredential};
#[cfg(not(feature = "ct-sign"))]
use nisty::{Keypair, SecretKey};

#[cfg(feature = "ct-sign")]
use crate::ct_sign::PrivateKey;
use crate::hmac_key_utils::{HmacCredentialData, HmacCredentialIterator, HmacKeyCredential};

type Hmac256 = Hmac<Sha256>;
type Hmac512 = Hmac<Sha512>;

/// `HMAC` based implementation of [`CtapPlatform`]()
///
/// Needs to be provided with a [`HmacKeyPlatform`](trait.HmacKeyPlatform.html).
#[derive(Debug)]
pub struct HmacKey<P: HmacKeyPlatform> {
    current_rp_id: Option<heapless::String<heapless::consts::U128>>,
    platform: P,
}

impl<P: HmacKeyPlatform> HmacKey<P> {
    /// Create a new `HmacKey` authenticator instant from a
    /// [`HmacKeyPlatform`](trait.HmacKeyPlatform.html) implementation
    pub fn new(platform: P) -> Self {
        Self {
            current_rp_id: None,
            platform,
        }
    }

    fn generate_credential(&self, rp_id: &str, nonce: &[u8]) -> HmacCredentialData {
        // NOTE: new_varkey will never return None here because HMACs can take keys of any size.
        // Other MACs might not.
        let device_secret = self.platform.get_device_secret();

        // Generate the first mac
        let mut mac1 = Hmac512::new_varkey(&device_secret).unwrap();
        mac1.update(rp_id.as_bytes());
        mac1.update(nonce);
        let code = mac1.finalize().into_bytes();

        // Get all the data out of the first mac
        let seed: [u8; 32] = code[..32].try_into().unwrap();
        let counter_id = u32::from_le_bytes(code[32..36].try_into().unwrap());
        let counter_offset =
            u32::from_le_bytes(code[36..40].try_into().unwrap()).bitand(0b0111_1111_1111_1111);

        // Generate the second mac
        let mut mac2 = Hmac256::new_varkey(&device_secret).unwrap();
        mac2.update(rp_id.as_bytes());
        mac2.update(&seed);
        mac2.update(&counter_id.to_le_bytes());
        mac2.update(&counter_offset.to_le_bytes());

        HmacCredentialData {
            credential: HmacKeyCredential::new(
                mac2.finalize().into_bytes().as_slice().try_into().unwrap(),
                nonce.try_into().unwrap(),
            ),

            #[cfg(not(feature = "ct-sign"))]
            key: Keypair::generate_patiently(seed),

            #[cfg(feature = "ct-sign")]
            // TODO: This can fail! How to handle?
            key: PrivateKey::from_bytes(&seed).unwrap(),

            counter_id,
            counter_offset,
        }
    }

    /// Generates a Credential from a CredentialId and checks that the Id is valid
    fn checked_generate(&self, rp_id: &str, credential_id: &[u8]) -> Option<HmacCredentialData> {
        if credential_id.len() != 40 {
            return None;
        }

        let received_mac: Output<Hmac256> =
            Output::new(*GenericArray::from_slice(&credential_id[..32]));

        let credential = self.generate_credential(rp_id, &credential_id[32..]);

        let generated_mac: Output<Hmac256> =
            Output::new(*GenericArray::from_slice(&credential.credential.0[..32]));

        if generated_mac == received_mac {
            Some(credential)
        } else {
            None
        }
    }
}

impl<P: HmacKeyPlatform> AuthenticatorPlatform for HmacKey<P> {
    // These values are just passed through from HmacKeyPlatform to AuthenticatorPlatform
    const AAGUID: [u8; 16] = P::AAGUID;
    const CERTIFICATE: Option<&'static [u8]> = Some(P::CERTIFICATE);
    const MAX_MSG_LENGTH: u16 = P::MAX_MSG_LENGTH;

    // The options are fixed, because we will not be able to store residential keys.
    const SUPPORTED_OPTIONS: CtapOptions = CtapOptions {
        plat: None,
        rk: Some(false),
        client_pin: None,
        up: Some(true),
        uv: Some(false),
    };

    type CredentialId = HmacKeyCredential;
    type PublicKeyBuffer = [u8; 32];
    type SignatureBuffer = [u8; 32];
    type CredentialIterator = HmacCredentialIterator;

    fn reset(&mut self) -> Result<(), ()> {
        self.platform.reset()
    }

    fn check_exclude_list(&mut self, rp_id: &str, list: CredentialDescriptorList) -> bool {
        for descriptor in list {
            if self.checked_generate(rp_id, descriptor.get_id()).is_some() {
                return true;
            }
        }

        false
    }

    fn locate_credentials(
        &mut self,
        rp_id: &str,
        list: Option<CredentialDescriptorList>,
    ) -> (u16, Self::CredentialIterator) {
        match list {
            None => (0, HmacCredentialIterator::new()),
            Some(list) => {
                let mut result = HmacCredentialIterator::new();

                for descriptor in list {
                    match self.checked_generate(rp_id, descriptor.get_id()) {
                        None => (),
                        Some(credential) => {
                            let counter = self.platform.fetch_counter(credential.counter_id)
                                + credential.counter_offset;

                            match result.push(credential.credential, counter) {
                                Ok(()) => (),
                                // NOTE: We just truncate if we don't have enough space to store all
                                // fitting credentials
                                Err(()) => break,
                            }
                        }
                    }
                }

                (result.len() as u16, result)
            }
        }
    }

    fn create_credential(
        &mut self,
        rp_id: RpCredential<&[u8], &str>,
        _user: UserCredential<&[u8], &str>,
    ) -> (Self::CredentialId, PublicKey<Self::PublicKeyBuffer>, u32) {
        let rp_id = core::str::from_utf8(rp_id.rp_id()).unwrap();

        let mut nonce = [0; 8];
        self.platform.generate_nonce(&mut nonce);

        let new_credential = self.generate_credential(&rp_id, &nonce);

        let counter = self.platform.fetch_counter(new_credential.counter_id)
            + new_credential.counter_offset
            // Because we will increase the counter after using it
            - 1;

        let pubkey = new_credential.get_pubkey();
        (new_credential.credential, pubkey, counter)
    }

    fn make_credential_prompt(
        &mut self,
        _rp: RpCredential<&[u8], &str>,
        _user: UserCredential<&[u8], &str>,
    ) {
        self.platform.user_check_prompt(true)
    }

    fn get_assertion_prompt(&mut self, rp_id: &str, _credential_id: &Self::CredentialId) {
        self.current_rp_id = Some(rp_id.into());
        self.platform.user_check_prompt(false)
    }

    fn late_user_check(&mut self) -> bool {
        self.platform.check_user_presence()
    }

    fn attest(
        &mut self,
        _id: &Self::CredentialId,
        data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>> {
        #[cfg(not(feature = "ct-sign"))]
            let attest = {
            let sk = SecretKey::try_from_bytes(&P::ATTESTATION_KEY).unwrap();
            let kp = Keypair {
                public: (&sk).into(),
                secret: sk,
            };
            let sig = kp.sign(data).to_bytes();


            Signature::nistp256(sig[..32].try_into().unwrap(), sig[32..].try_into().unwrap())
        };

        #[cfg(feature = "ct-sign")]
            let attest = {
            // NOTE: This can't fail as it is generated and checked offline
            let sk = PrivateKey::from_bytes(&P::ATTESTATION_KEY).unwrap();

            // TODO: This can fail! How to handle?
            let sig = sk.try_sign_deterministic(data, None).unwrap();

            let mut r: [u8; 32] = [0; 32];
            r.clone_from_slice(sig.r().as_ref());
            let mut s: [u8; 32] = [0; 32];
            s.clone_from_slice(sig.s().as_ref());

            Signature::nistp256(r, s)
        };

        log::debug!("Signature: {:?}", attest);
        Some(attest)
    }

    fn sign(
        &mut self,
        id: &Self::CredentialId,
        data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>> {
        log::debug!("Signing data: {:?}", data);

        let rpid = match &self.current_rp_id {
            None => return None,
            Some(rpid) => rpid,
        };

        let key = self
            .checked_generate(rpid, &id.0)
            .map(|cred| {
                self.platform.bump_counter(cred.counter_id);
                cred
            })
            .map(|cred| cred.key);

        #[cfg(not(feature = "ct-sign"))]
        let signature = key
            .map(|kp| kp.sign(data).to_bytes())
            .map(|sig| {
                Signature::nistp256(sig[..32].try_into().unwrap(), sig[32..].try_into().unwrap())
            });

        #[cfg(feature = "ct-sign")]
        let signature = key
            .map(|sk| {
                sk.try_sign_deterministic(data, None).unwrap()
            })
            .map(|sig| {
                let mut r: [u8; 32] = [0; 32];
                r.clone_from_slice(sig.r().as_ref());
                let mut s: [u8; 32] = [0; 32];
                s.clone_from_slice(sig.s().as_ref());
                Signature::nistp256(r, s)
            });

        log::debug!("Signature: {:?}", signature);
        signature
    }

    fn start_timeout(&mut self) {
        self.platform.start_timeout()
    }

    fn has_timed_out(&mut self, time: Duration) -> bool {
        self.platform.has_timed_out(time)
    }

    fn probe(&mut self, probe: &mut [u8], length: u16) -> u16 {
        self.platform.probe(probe, length)
    }
}

/// The platform specific part of the [`HmacKey`](struct.HmacKey.html) is implemented via this
/// trait
///
/// Some functions are the same as found in `AuthenticatorPlatform`.
#[allow(unused_variables)]
pub trait HmacKeyPlatform: Debug {
    /// The `AAGUID` of this authenticator
    const AAGUID: [u8; 16];

    /// The certificate of this authenticator
    ///
    /// If none is provided, the authenticator is self signing, i.e. the attestation and assertion
    /// keys are identical.
    const CERTIFICATE: &'static [u8];

    /// The maximal length if cbor messages, this authenticator supports
    const MAX_MSG_LENGTH: u16;

    /// The attestation key used in this authenticator
    const ATTESTATION_KEY: [u8; 32];

    /// Instructs the authenticator to perform a reset
    ///
    /// In the case of a `HmacKey` this means generating a new device secret and re-initializing the
    /// counter pool.
    ///
    /// **No not return from this function, before the reset was completed.**
    ///
    /// Depending on the application, it might be useful to acquire user consent before resetting
    /// the authenticator.
    ///
    /// # Returns
    /// - `Err(())` if the authenticator was not reset, for any reason
    /// - `Ok(())` if the authenticator was successfully reset
    fn reset(&mut self) -> Result<(), ()>;

    /// Requests the platform to fill the nonce slice with random data
    fn generate_nonce(&mut self, nonce: &mut [u8]);

    /// Requests the platform to return a counter value from the counter pool
    ///
    /// Which counter to return is specified by the counter_id. The mapping from counter_id to
    /// counter is user defined, but the same `counter_id` must always refer to the same counter.
    fn fetch_counter(&mut self, counter_id: u32) -> u32;

    /// Requests the platform to bump the counter specified by the counter id
    fn bump_counter(&mut self, counter_id: u32);

    /// Requests the device secret from the platform
    fn get_device_secret(&self) -> [u8; 32];

    /// Requests the platform to blink the LED to prompt the user to perform a user presence check
    ///
    /// # Arguments
    /// `make_credential`: If `true` the user shall be prompted to create a new credential, if
    /// `false` the user shall be prompted to generate an assertion. Whether to expose different
    /// behaviour at all is platform dependent
    fn user_check_prompt(&mut self, make_credential: bool);

    /// Perform a simple user presence check
    fn check_user_presence(&mut self) -> bool;

    /// Instructs the `AuthenticatorPlatform` to start a new timer
    fn start_timeout(&mut self);

    /// Asks, whether the time since the last `start_timeout` call is longer than
    /// since
    ///
    /// # Arguments
    /// `time`: The timeout time
    ///
    /// # Returns
    /// `true` if the last call to `start_timeout` is a longer time in the past than `time`.
    /// `false` otherwise.
    fn has_timed_out(&mut self, time: Duration) -> bool;

    /// Can be used to implement testing software on an authenticator.
    ///
    /// This functionality allows an implementor to send an arbitrary byte buffer to the buffer.
    /// The authenticator can inspect the buffer and its length and then manipulate it.
    /// The length of the return buffer is returned.
    /// # Arguments
    /// - `probe`: The probe buffer. The length of the buffer is not necessarily the length of the
    /// valid data.
    /// - `length`: The length of the valid data.
    ///
    /// # Returns
    /// The length of the response. The response itself is simply written into the probe buffer.
    fn probe(&mut self, probe: &mut [u8], length: u16) -> u16 {
        0
    }
}
