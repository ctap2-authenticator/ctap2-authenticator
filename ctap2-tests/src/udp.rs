mod db;
mod hmac_platform;
mod test_platform;
mod transport;

use ctap2_authenticator::usbhid::TransactionProcessor;

use std::error::Error;
use std::net::{SocketAddr, UdpSocket};
use std::process::exit;

use crate::hmac_platform::HmacTestPlatform;
use crate::test_platform::TestPlatform;
use crate::transport::TestTransport;

use ctap2_authenticator::Authenticator;
use ctap2_extra::hmac_key::HmacKey;
use std::sync::mpsc::{sync_channel, TryRecvError};
use std::time::Duration;

fn receive_packet(socket: &UdpSocket, packet: &mut [u8; 64]) -> Option<SocketAddr> {
    match socket.recv_from(packet) {
        Ok((size, addr)) => {
            if size != 64 {
                log::warn!("Received USBHID packet with other packets size than 64. Bug?");
            }
            Some(addr)
        }
        Err(_) => None,
    }
}

fn send_packet(socket: &UdpSocket, addr: SocketAddr, packet: &[u8]) {
    match socket.send_to(&packet, addr) {
        Ok(size) => {
            if size != 64 {
                log::warn!("Send USBHID packet with other packets size than 64. Bug!");
            }
        }
        Err(e) => {
            log::error!("Error {:?} while sending packets. Abort", e);
            exit(-1);
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    log::info!("Starting the test runner");

    let socket = UdpSocket::bind("127.0.0.1:8111")?;
    socket.set_read_timeout(Some(Duration::from_millis(5)))?;
    log::info!("Socket established");

    let (to_app, from_transport) = sync_channel(1);
    let (to_transport, from_app) = sync_channel(1);

    std::thread::spawn(move || {
        log::info!("Started Authenticator thread");
        //let mut authenticator = Authenticator::create(TestPlatform::new());
        let mut authenticator = Authenticator::create(HmacKey::new(HmacTestPlatform::new()));

        loop {
            match from_transport.recv() {
                Err(_) => break,
                Ok(data) => to_transport.send(authenticator.process(data)).unwrap(),
            }
        }
    });

    let main_buf = vec![0; 2048];
    let mut parser = TransactionProcessor::new(main_buf, TestTransport::new());

    let mut packet = [0; 64];
    let mut last_address: Option<SocketAddr> = None;
    loop {
        match from_app.try_recv() {
            Ok(data) => parser.response(data),
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => break,
        }

        let (request, output) = match receive_packet(&socket, &mut packet) {
            Some(addr) => {
                last_address = Some(addr);
                parser.poll(Some(&packet))
            }
            None => parser.poll(None),
        };

        if let Some(request) = request {
            to_app.send(request).unwrap()
        }

        if let Some(last_address) = last_address {
            if let Some(output) = output {
                send_packet(&socket, last_address, &output)
            }
        }
    }

    Ok(())
}
