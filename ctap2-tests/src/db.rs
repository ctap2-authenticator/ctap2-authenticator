use ctap2_authenticator::credentials::{RpCredential, UserCredential};
//use ed25519_dalek::SecretKey;
use ctap2_authenticator::PublicKey;
use nisty::{Keypair, SecretKey};
use rand::rngs::OsRng;
use rand::RngCore;
use std::collections::HashMap;

type CredentialId = [u8; 32];

#[derive(Debug)]
pub struct DbEntry {
    pub rp: Vec<u8>,
    pub rp_name: String,
    pub user: String,
    pub secret: SecretKey,
    pub sig_count: u32,
}

#[derive(Debug)]
pub struct Db {
    pub map: HashMap<[u8; 32], DbEntry>,
}

impl Db {
    pub fn new() -> Self {
        Self {
            map: HashMap::new(),
        }
    }

    pub fn new_entry(
        &mut self,
        rp: RpCredential<&[u8], &str>,
        user: UserCredential<&[u8], &str>,
    ) -> (CredentialId, PublicKey<Vec<u8>>) {
        let mut seed: [u8; 32] = [0; 32];
        OsRng.fill_bytes(&mut seed);

        let keypair = Keypair::generate_patiently(&seed);

        let entry = DbEntry {
            rp: rp.rp_id().to_vec(),
            rp_name: (*rp.name()).to_string(),
            user: (*user.name()).to_string(),
            secret: keypair.secret,
            sig_count: 0,
        };

        let mut credential_id: CredentialId = [0; 32];
        OsRng.fill_bytes(&mut credential_id);

        log::info!(
            "Created a new credential: ID {:?} : DATA {:?}",
            credential_id,
            entry
        );
        self.map.insert(credential_id, entry);

        let public_key = PublicKey::nistp256(
            keypair.public.as_bytes()[..32].to_vec(),
            keypair.public.as_bytes()[32..].to_vec(),
        );

        (credential_id, public_key)
    }
}
