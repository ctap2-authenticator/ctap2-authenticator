use crate::db::Db;
use ctap2_authenticator::credentials::{RpCredential, UserCredential};
use ctap2_authenticator::{
    AuthenticatorPlatform, CredentialDescriptorList, CtapOptions, PublicKey, Signature,
};

use nisty::{Keypair, SecretKey};
use std::convert::TryFrom;
use std::time::{Duration, Instant};

const CERT: &[u8; 760] = include_bytes!("../certs/intercert.der");
const ATTESTATION_SIG: [u8; 32] = [
    0x32, 0xc9, 0xf7, 0x5d, 0x73, 0xb5, 0xc5, 0xce, 0x89, 0x0d, 0xaa, 0x57, 0x5a, 0xd5, 0x08, 0x42,
    0x9a, 0xcd, 0x95, 0xd5, 0xd3, 0xd7, 0x77, 0x3f, 0xf2, 0xd0, 0x9c, 0xd0, 0xa5, 0xf3, 0xd6, 0xcd,
];

#[derive(Debug)]
pub struct TestPlatform {
    timer: Instant,
    db: Db,
}

impl TestPlatform {
    pub fn new() -> Self {
        TestPlatform {
            timer: Instant::now(),
            db: Db::new(),
        }
    }
}

impl AuthenticatorPlatform for TestPlatform {
    const AAGUID: [u8; 16] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24,
        0x25,
    ];
    const CERTIFICATE: Option<&'static [u8]> = Some(CERT);
    const MAX_MSG_LENGTH: u16 = 2048;
    const SUPPORTED_OPTIONS: CtapOptions = CtapOptions {
        plat: None,
        rk: Some(true),
        client_pin: None,
        up: Some(true),
        uv: Some(false),
    };
    type CredentialId = [u8; 32];
    type PublicKeyBuffer = Vec<u8>;
    type SignatureBuffer = Vec<u8>;
    type CredentialIterator = std::vec::IntoIter<([u8; 32], u32)>;

    fn reset(&mut self) -> Result<(), ()> {
        println!("Do you want to reset the authenticator and delete all the keys? [y/N]");

        // If real presence check was performed
        //        let mut input = "".to_string();
        //        if stdin().read_line(&mut input).is_ok() && !input.starts_with('y') {
        //            println!("DONT RESET");
        //            return Err(());
        //        }

        println!("RESETTING AUTHENTICATOR");
        self.timer = Instant::now();
        self.db = Db::new();
        Ok(())
    }

    fn check_exclude_list(&mut self, rp_id: &str, list: CredentialDescriptorList) -> bool {
        log::debug!(
            "Checking if any of the following credentials exists {:?} for rp: {}",
            list,
            rp_id,
        );
        let exists = list
            .filter_map(|desc| <&[u8; 32]>::try_from(*(desc.get_id())).ok())
            .map(|desc| self.db.map.get(desc).is_some())
            .any(|x| x);

        log::debug!("Does it?: {}", exists);
        exists
    }

    fn locate_credentials(
        &mut self,
        rp_id: &str,
        list: Option<CredentialDescriptorList>,
    ) -> (u16, Self::CredentialIterator) {
        log::info!("Searching for credentials bound to rpid: {:?}", rp_id);
        let list: Vec<&[u8; 32]> = if let Some(list) = list {
            list.filter_map(|desc| <&[u8; 32]>::try_from(*(desc.get_id())).ok())
                .collect()
        } else {
            self.db.map.keys().collect()
        };

        let credentials = list
            .iter()
            .map(|id| (id, self.db.map.get(*id)))
            // Drop all keys where no entry was found
            .filter_map(|(id, maybe_entry)| maybe_entry.map(|entry| (*id, entry)))
            .filter(|(_, entry)| entry.rp == rp_id.as_bytes())
            .map(|(id, entry)| (id.to_owned(), entry.sig_count));

        let num_credentials = credentials.clone().count() as u16;
        let credentials: Vec<([u8; 32], u32)> = credentials.collect();

        log::info!(
            "Identified the following {} credentials: {:?}",
            num_credentials,
            credentials
        );
        (num_credentials, credentials.into_iter())
    }

    fn create_credential(
        &mut self,
        rp: RpCredential<&[u8], &str>,
        user: UserCredential<&[u8], &str>,
    ) -> (Self::CredentialId, PublicKey<Self::PublicKeyBuffer>, u32) {
        let (credential_id, public_key) = self.db.new_entry(rp, user);
        (credential_id, public_key, 0)
    }

    fn make_credential_prompt(
        &mut self,
        rp_id: RpCredential<&[u8], &str>,
        user: UserCredential<&[u8], &str>,
    ) {
        println!(
            "The relying party {} wants to sign in the user {}. Continue? [y/N]",
            String::from_utf8_lossy(rp_id.rp_id()),
            user.name()
        );
    }

    fn get_assertion_prompt(&mut self, _rp_id: &str, credential_id: &Self::CredentialId) {
        if let Some(entry) = self.db.map.get(credential_id) {
            println!(
                "The relying party {} wants to sign in the user {}. Continue? [y/N]",
                entry.rp_name, entry.user
            );
        } else {
            println!("Unknown Id {:?} wants to sign in.", credential_id);
        }
    }

    fn user_check(&mut self) -> bool {
        // This is what a real check might look like
        //        let mut input = "".to_string();
        //        if stdin().read_line(&mut input).is_ok() && input.starts_with('y') {
        //            println!("USER PRESENT");
        //            return Some(self.db.new_entry(rp, user));
        //        }
        //
        //        println!("USER NOT PRESENT");
        //        None
        println!("USER CHECK SUCCESSFUL");
        true
    }

    fn attest(
        &mut self,
        id: &Self::CredentialId,
        data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>> {
        log::debug!("Attesting data: {:?} for id: {:?}", data, id);

        // This is the code for self sign
        //        let attest = self
        //            .current_key
        //            .map(|k| SecretKey::try_from_bytes(&k).unwrap())
        //            .map(|sk| Keypair {
        //                public: (&sk).into(),
        //                secret: sk,
        //            })
        //            .map(|kp| kp.sign(data).to_bytes())
        //            .map(|sig| Signature::nistp256(sig[..32].to_vec(), sig[32..].to_vec()));

        // This is the code for attestation
        let sk = SecretKey::try_from_bytes(&ATTESTATION_SIG).unwrap();
        let kp = Keypair {
            public: (&sk).into(),
            secret: sk,
        };
        let sig = kp.sign(data).to_bytes();
        let attest = Signature::nistp256(sig[..32].to_vec(), sig[32..].to_vec());

        log::debug!("Signature: {:?}", attest);
        Some(attest)
    }

    fn sign(
        &mut self,
        id: &Self::CredentialId,
        data: &[u8],
    ) -> Option<Signature<Self::SignatureBuffer>> {
        log::debug!("Signing data: {:?}", data);

        let mut entry = self.db.map.get_mut(id);

        // Increase the counter
        match &mut entry {
            None => (),
            Some(entry) => entry.sig_count += 1,
        }

        let signature = entry
            .map(|entry| entry.secret.as_bytes())
            .map(|k| SecretKey::try_from_bytes(&k).unwrap())
            .map(|sk| Keypair {
                public: (&sk).into(),
                secret: sk,
            })
            .map(|kp| kp.sign(data).to_bytes())
            .map(|sig| Signature::nistp256(sig[..32].to_vec(), sig[32..].to_vec()));

        log::debug!("Signature: {:?}", signature);
        signature
    }

    fn start_timeout(&mut self) {
        log::trace!("Starting timer");
        self.timer = Instant::now();
    }

    fn has_timed_out(&mut self, time: Duration) -> bool {
        let timeout = Instant::now().duration_since(self.timer) > time;
        log::trace!("Has timed out?: {:?}", timeout);
        timeout
    }
}
