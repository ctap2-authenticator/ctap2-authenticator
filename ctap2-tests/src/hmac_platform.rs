use ctap2_extra::hmac_key::HmacKeyPlatform;
use rand::rngs::OsRng;
use rand::{Rng, RngCore};
use std::ops::BitAnd;
use std::time::{Duration, Instant};

const COUNTER_POOL_SIZE: usize = 32;

#[derive(Debug)]
pub struct HmacTestPlatform {
    device_secret: [u8; 32],
    timer: Instant,
    counter_pool: [u32; COUNTER_POOL_SIZE],
}

impl HmacTestPlatform {
    pub fn new() -> Self {
        // Generate the counter pool
        let mut pool: [u32; COUNTER_POOL_SIZE] = [0; COUNTER_POOL_SIZE];
        OsRng.fill(&mut pool);
        for num in pool.iter_mut() {
            *num = num.bitand(0b0111_1111_1111_1111);
        }

        let mut device_secret: [u8; 32] = [0; 32];
        OsRng.fill_bytes(&mut device_secret);

        Self {
            device_secret,
            timer: Instant::now(),
            counter_pool: pool,
        }
    }
}

const CERT: &[u8; 760] = include_bytes!("../certs/intercert.der");
const ATTESTATION_KEY: [u8; 32] = [
    0x32, 0xc9, 0xf7, 0x5d, 0x73, 0xb5, 0xc5, 0xce, 0x89, 0x0d, 0xaa, 0x57, 0x5a, 0xd5, 0x08, 0x42,
    0x9a, 0xcd, 0x95, 0xd5, 0xd3, 0xd7, 0x77, 0x3f, 0xf2, 0xd0, 0x9c, 0xd0, 0xa5, 0xf3, 0xd6, 0xcd,
];

impl HmacKeyPlatform for HmacTestPlatform {
    const AAGUID: [u8; 16] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24,
        0x25,
    ];
    const CERTIFICATE: &'static [u8] = CERT;
    const MAX_MSG_LENGTH: u16 = 2048;
    const ATTESTATION_KEY: [u8; 32] = ATTESTATION_KEY;

    fn reset(&mut self) -> Result<(), ()> {
        println!("EXECUTING RESET");

        *self = Self::new();

        Ok(())
    }

    fn generate_nonce(&mut self, nonce: &mut [u8]) {
        OsRng.fill_bytes(nonce);
    }

    fn fetch_counter(&mut self, counter_id: u32) -> u32 {
        self.counter_pool[counter_id as usize % COUNTER_POOL_SIZE]
    }

    fn bump_counter(&mut self, counter_id: u32) {
        self.counter_pool[counter_id as usize % COUNTER_POOL_SIZE] += 1;
    }

    fn get_device_secret(&self) -> [u8; 32] {
        self.device_secret
    }

    fn user_check_prompt(&mut self, _make_credential: bool) {
        println!("USER PRESENT?")
    }

    fn check_user_presence(&mut self) -> bool {
        println!("USER PRESENT!");
        true
    }

    fn start_timeout(&mut self) {
        log::trace!("Starting timer");
        self.timer = Instant::now();
    }

    fn has_timed_out(&mut self, time: Duration) -> bool {
        let timeout = Instant::now().duration_since(self.timer) > time;
        log::trace!("Has timed out?: {:?}", timeout);
        timeout
    }

    fn probe(&mut self, probe: &mut [u8], length: u16) -> u16 {
        log::debug!("Length: {}", length);
        log::debug!("Data: {:?}", &probe[0..length as usize]);

        probe[0] = 0x17;
        6
    }
}
