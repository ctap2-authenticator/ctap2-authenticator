use ctap2_authenticator::usbhid::{CtapHidCapabilities, CtapHidPlatform, KeepaliveResponse};
use std::time::{Duration, SystemTime};

#[derive(Debug)]
pub struct TestTransport {
    last_keepalive: SystemTime,
    transaction_start: SystemTime,
}

impl TestTransport {
    pub fn new() -> Self {
        Self {
            last_keepalive: SystemTime::now(),
            transaction_start: SystemTime::now(),
        }
    }
}

impl CtapHidPlatform for TestTransport {
    const MAJOR_VERSION: u8 = 1;
    const MINOR_VERSION: u8 = 3;
    const BUILD_VERSION: u8 = 7;
    const CAPABILITIES: CtapHidCapabilities = CtapHidCapabilities {
        wink: true,
        cbor: true,
        msg: false,
    };

    fn wink(&mut self) {
        println!("BLINK BLINK");
    }

    fn keepalive_needed(&mut self) -> KeepaliveResponse {
        let now = SystemTime::now();
        if now.duration_since(self.last_keepalive).unwrap() >= Duration::from_millis(90) {
            self.last_keepalive = now;
            KeepaliveResponse::Processing
        } else {
            KeepaliveResponse::None
        }
    }

    fn start_timer(&mut self) {
        self.transaction_start = SystemTime::now();
    }

    fn has_timed_out(&mut self) -> bool {
        let dur = SystemTime::now()
            .duration_since(self.transaction_start)
            .unwrap();
        log::debug!("Duration {:?}", dur);
        dur > Duration::from_millis(800)
    }
}
