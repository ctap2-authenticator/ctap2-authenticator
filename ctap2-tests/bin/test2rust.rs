use std::env;
use std::fs::read_to_string;

#[derive(PartialEq, Eq)]
enum State {
    FirstDigit,
    SecondDigit,
    Comment,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("File to read from was not specified");
        return;
    }

    let input = read_to_string(args[1].clone()).unwrap();
    let mut output = "".to_string();
    let mut state = State::FirstDigit;

    for c in input.chars() {
        if state == State::Comment {
            output.push(c);
            if c == '\n' {
                state = State::FirstDigit;
            }
        } else if state == State::FirstDigit && c.is_ascii_hexdigit() {
            output.push_str("0x");
            output.push(c);
            state = State::SecondDigit;
        } else if state == State::SecondDigit && c.is_ascii_hexdigit() {
            output.push(c);
            output.push(',');
            state = State::FirstDigit;
        } else if c == '#' {
            output.push_str("//");
            state = State::Comment;
        } else if !c.is_whitespace() {
            println!("A parsing error has occurred at character {}", c);
            return;
        }
    }

    println!("{}", output);
}
