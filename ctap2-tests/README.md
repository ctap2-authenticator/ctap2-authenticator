# CTAP2-Authenticator Tests
This is a PC-based test integration of the ctap2-authenticator.
It is designed to be run against `solokeys/fido2-tests`.

## Installation

Some dependencies are required to be able to run the tests.
If required dependencies are missing, please file an issue.

On Ubuntu, run:

    sudo apt install swig libpcsclite-dev libc6-dev-i386

Then, we need to get the `fido2-tests` sources.

    git clone https://github.com/solokeys/fido2-tests.git

Create a virtual environment to install your python dependencies into and activate it

    python3 -m venv venv
    source venv/bin/activate

Next, install the python dependencies

    pip install -r requirements.txt

The test-program requires some dependencies:

    sudo apt install libclang-dev

## Running via UDP

Now, the test-suite is ready to go.
Build and run this repository:

    cargo run --bin udp-test

and in another window, start the tests

    pytest -s -v --sim tests/standard

## Running via UHID

Another way of running the tests is via an emulated HID-device.
This only works on linux and root access might be required.

To build and run the repository:

    sudo cargo run --bin uhid-test --all-features

and in another window, start the tests

    pytest -s -v tests/standard

### Note
The ctap2-authenticator is a Work-in-Progress and large parts of the tests will fail.
Also currently, we are not aiming at feature completeness.
A list of tests that are expected to run will be uploaded soon.


## Setup Timing Test Tool
TODO