#![no_std]
#![no_main]

use core::convert::TryInto;
use core::time::Duration;

use embedded_hal::digital::v2::{InputPin, OutputPin};
#[cfg(not(feature = "semihosting"))]
use panic_halt as _;
#[cfg(feature = "semihosting")]
use panic_semihosting as _;
use rtic::app;
use stm32f4xx_hal::{gpio, stm32};
use stm32f4xx_hal::gpio::{Edge, GpioExt, Output, PushPull};
use stm32f4xx_hal::gpio::ExtiPin;
use stm32f4xx_hal::otg_fs::{USB, UsbBus};
use stm32f4xx_hal::rcc::RccExt;
use stm32f4xx_hal::time::U32Ext;
use stm32f4xx_hal::timer::{Event, Timer};
use usb_device::bus::UsbBusAllocator;

use ctap2_extra::hmac_key::{HmacKey, HmacKeyPlatform};
use ctap2_extra::usb_key::{
    AppContext, AppState, get_state, get_time, time_tick, transition_state, UsbContext,
    UsbKeyBuilder,
};

#[cfg(feature = "semihosting")]
#[allow(non_snake_case)]
#[cortex_m_rt::exception]
fn HardFault(ef: &cortex_m_rt::ExceptionFrame) -> ! {
    use cortex_m_semihosting::hio::hstdout;
    use core::fmt::Write;

    // prints the exception frame using semihosting
    core::writeln!(hstdout().unwrap(), "{:?}", ef).unwrap();
    loop {
        cortex_m::asm::bkpt();
    }
}

pub struct LedState {
    blinked: usize,
    last_state: AppState,
    on: bool,
    since: usize,
    switch_in: usize,
}

impl Default for LedState {
    fn default() -> Self {
        Self {
            blinked: 0,
            last_state: AppState::Idle,
            on: true,
            since: 0,
            switch_in: 400,
        }
    }
}

#[app(device = stm32f4xx_hal::stm32)]
const APP: () = {
    struct Resources {
        blue_led: gpio::gpioc::PC13<Output<PushPull>>,
        button: gpio::gpioa::PA0<gpio::Input<gpio::PullUp>>,
        led_state: LedState,
        timer: Timer<stm32::TIM2>,
        usb_side: UsbContext<UsbBus<USB>>,
        app_side: AppContext<HmacKey<UsbKey>>,
    }

    #[init]
    fn init(_cx: init::Context) -> init::LateResources {
        let mut cp = cortex_m::Peripherals::take().unwrap();
        let mut dp = stm32::Peripherals::take().unwrap();

        // Setup Clocks
        let rcc = dp.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(25.mhz())
            .sysclk(84.mhz())
            //.sysclk(32.mhz())
            .require_pll48clk()
            .pclk1(24.mhz())
            .pclk2(24.mhz())
            .freeze();

        let gpioa = dp.GPIOA.split();
        let gpioc = dp.GPIOC.split();

        // Init LED
        let led_state = LedState::default();
        let blue_led = gpioc.pc13.into_push_pull_output();

        // Init Button Input
        let mut button = gpioa.pa0.into_pull_up_input();
        button.enable_interrupt(&mut dp.EXTI);
        button.trigger_on_edge(&mut dp.EXTI, Edge::RISING_FALLING);

        // This is a super horrible hack to turn c15 into a ground line. This saves me running a
        // cable to ground.
        let mut c15 = gpioc.pc15.into_push_pull_output();
        let _ = c15.set_low();

        // Init USB Device

        // Initialize the Peripheral on the Chip
        let usb = USB {
            usb_global: dp.OTG_FS_GLOBAL,
            usb_device: dp.OTG_FS_DEVICE,
            usb_pwrclk: dp.OTG_FS_PWRCLK,
            pin_dm: gpioa.pa11.into_alternate_af10(),
            pin_dp: gpioa.pa12.into_alternate_af10(),
        };

        // Initialize the allocator. Since it need to live forever, it is initialized as a singleton
        let usb_bus_allocator = cortex_m::singleton!(: UsbBusAllocator<UsbBus<USB>> = UsbBus::new(
            usb,
            cortex_m::singleton!(: [u32; 1024] = [0; 1024]).unwrap(),
        ))
            .unwrap();

        let (usb_side, app_side) = UsbKeyBuilder::new(0x0483, 0xa2ca)
            .manufacturer("Dinokeys")
            .product("USB Fido2 Authenticator Key")
            .try_build(HmacKey::new(UsbKey::new()), usb_bus_allocator)
            .unwrap();

        // Init Timer
        let mut timer = Timer::tim2(dp.TIM2, 1.khz(), clocks);
        timer.listen(Event::TimeOut);

        // Activate the cycle counter for the probe function
        cp.DWT.enable_cycle_counter();

        init::LateResources {
            blue_led,
            button,
            led_state,
            timer,
            usb_side,
            app_side,
        }
    }

    #[task(binds = TIM2, priority = 1, resources = [blue_led, led_state, timer, usb_side])]
    fn tim2(mut cx: tim2::Context) {
        // Clear the interrupt ... otherwise the timeout would immediately trigger again
        cx.resources.timer.clear_interrupt(Event::TimeOut);

        // Handle the timer
        let now = handle_timer(cx.resources.blue_led, cx.resources.led_state);

        // We need to poll the USB to drive it every now and then
        // This is a hack. We should find a better way of doing this
        if now % 10 == 0 {
            cx.resources.usb_side.lock(|usb_side| usb_side.poll());
        }
    }

    #[task(binds = OTG_FS, priority = 2, resources = [usb_side])]
    fn usb(cx: usb::Context) {
        let _ = cx.resources.usb_side.poll();
    }

    #[task(binds = EXTI0, priority = 3, resources = [button])]
    fn button(cx: button::Context) {
        // Clear the interrupt so this does not trigger again
        cx.resources.button.clear_interrupt_pending_bit();

        if cx.resources.button.is_low().unwrap() {
            let _ = transition_state(Some(AppState::WaitUp), AppState::Processing);
        }
    }

    #[idle(resources = [app_side])]
    fn idle(cx: idle::Context) -> ! {
        loop {
            if !cx.resources.app_side.poll() {
                cortex_m::asm::wfi();
            }
        }
    }
};

pub fn handle_timer(
    blue_led: &mut gpio::gpioc::PC13<Output<PushPull>>,
    led_state: &mut LedState,
) -> usize {
    // This interrupt is called every 1 ms. Increase the counter
    let now = time_tick();

    let state = get_state();
    let switch = now > led_state.since + led_state.switch_in || led_state.last_state != state;

    if switch {
        match (state, led_state.on) {
            (AppState::Idle, true) => (led_state.switch_in = 2800),
            (AppState::Idle, false) => (led_state.switch_in = 200),

            // In processing, we want to have the led on constantly
            (AppState::Processing, true) => (led_state.switch_in = 0),
            (AppState::Processing, false) => (led_state.switch_in = 1000),

            (AppState::WaitUp, true) => (led_state.switch_in = 150),
            (AppState::WaitUp, false) => (led_state.switch_in = 150),

            (AppState::Winking, true) => (led_state.switch_in = 150),
            (AppState::Winking, false) => (led_state.switch_in = 150),

            // In canceling, the LED is just of and we wait.
            (AppState::Canceling, true) => (led_state.switch_in = 1000),
            (AppState::Canceling, false) => (led_state.switch_in = 0),
        }

        if state != led_state.last_state || state != AppState::Winking {
            led_state.blinked = 0;
        } else {
            led_state.blinked += 1;
        }

        // Remember when we switched
        led_state.last_state = state;
        led_state.on = !led_state.on;
        led_state.since = now;

        // Switch the led
        if led_state.on {
            let _ = blue_led.set_low();
        } else {
            let _ = blue_led.set_high();
        }

        if led_state.blinked > 8 {
            let _ = transition_state(Some(AppState::Winking), AppState::Idle);
        }
    }

    now
}

const COUNTER_POOL_SIZE: usize = 4;

const CERT: &[u8; 760] = include_bytes!("../../ctap2-tests/certs/intercert.der");
const ATTESTATION_KEY: [u8; 32] = [
    0x32, 0xc9, 0xf7, 0x5d, 0x73, 0xb5, 0xc5, 0xce, 0x89, 0x0d, 0xaa, 0x57, 0x5a, 0xd5, 0x08, 0x42,
    0x9a, 0xcd, 0x95, 0xd5, 0xd3, 0xd7, 0x77, 0x3f, 0xf2, 0xd0, 0x9c, 0xd0, 0xa5, 0xf3, 0xd6, 0xcd,
];

#[derive(Debug)]
pub struct UsbKey {
    device_secret: [u8; 32],
    up_timer: usize,
    timer: usize,
    counter_pool: [u32; COUNTER_POOL_SIZE],

    #[cfg(feature = "ct-sign")]
    // NOTE: This needs to handled differently in deployment (See probe documentation).
    random_data: [u8; 32],
}

impl UsbKey {
    pub(crate) fn new() -> Self {
        let now = get_time();

        Self {
            // TODO: Generate an actual device secret
            device_secret: [
                1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4,
                5, 6, 7, 8,
            ],
            up_timer: now,
            timer: now,
            // TODO: Generate random counter pool
            counter_pool: [0, 0, 0, 0],

            #[cfg(feature = "ct-sign")]
            random_data: [0x01; 32],
        }
    }
}

impl HmacKeyPlatform for UsbKey {
    const AAGUID: [u8; 16] = [
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24,
        0x25,
    ];
    const CERTIFICATE: &'static [u8] = CERT;
    const MAX_MSG_LENGTH: u16 = 2048;
    const ATTESTATION_KEY: [u8; 32] = ATTESTATION_KEY;

    fn reset(&mut self) -> Result<(), ()> {
        // TODO: Implement a reset check with its own blink sequence and 2 sec pushing
        Ok(())
    }

    fn generate_nonce(&mut self, _nonce: &mut [u8]) {
        // TODO: Generate an actual nonce once we can
    }

    fn fetch_counter(&mut self, counter_id: u32) -> u32 {
        self.counter_pool[counter_id as usize % COUNTER_POOL_SIZE]
    }

    fn bump_counter(&mut self, counter_id: u32) {
        self.counter_pool[counter_id as usize % COUNTER_POOL_SIZE] += 1;
    }

    fn get_device_secret(&self) -> [u8; 32] {
        self.device_secret
    }

    fn user_check_prompt(&mut self, _make_credential: bool) {
        self.up_timer = get_time();
        transition_state(Some(AppState::Processing), AppState::WaitUp).unwrap();
    }

    fn check_user_presence(&mut self) -> bool {
        loop {
            match get_state() {
                AppState::WaitUp => {
                    // Check if the test has timed out
                    if get_time() - self.up_timer > 10_000 {
                        break false;
                    } else {
                        // If nothing is happening, we will simply wait for an interrupt and then
                        // recheck
                        cortex_m::asm::wfi();
                    }
                }
                AppState::Canceling => break false,
                _ => break true,
            }
        }
    }

    fn start_timeout(&mut self) {
        self.timer = get_time();
    }

    fn has_timed_out(&mut self, time: Duration) -> bool {
        let now = get_time();

        now - self.timer > time.as_millis().try_into().unwrap()
    }

    #[cfg(not(feature = "ct-sign"))]
    fn probe(&mut self, probe: &mut [u8], length: u16) -> u16 {
        use nisty::Keypair;

        // Check that length and command bit are fitting
        // FIXME: The max length checking should not be necessary but it is.
        // There is a bug somewhere in ctap2-authenticators probe code
        if length < 33 || length >= Self::MAX_MSG_LENGTH {
            return 0;
        }

        // Get the seed from the message and generate the key
        let seed: [u8; 32] = probe[1..33].try_into().unwrap();
        let key = Keypair::generate_patiently(seed);

        // Count the instructions while counting instructions
        let tick = cortex_m::peripheral::DWT::get_cycle_count();
        let _ = key.sign(&probe[33..length as usize - 33]);
        let tock = cortex_m::peripheral::DWT::get_cycle_count();

        // Return the instruction difference
        probe[0] = 0x00;
        probe[1..5].copy_from_slice(&(tock - tick).to_le_bytes());
        5
    }

    #[cfg(feature = "ct-sign")]
    fn probe(&mut self, probe: &mut [u8], length: u16) -> u16 {
        // NOTE: In order to decouple the execution time from the input, a masking scalar needs to
        // be used. This masking scalar needs to be random and inaccessible to potential side
        // channel attackers.
        // The way we generate our masking scalar here is not at all random and should not be used
        // in deployment. We implemented it that way for our tests because it does not need
        // additional hardware.


        use p256::Scalar;
        use ctap2_extra::ct_sign::PrivateKey;

        if length < 33 || length >= Self::MAX_MSG_LENGTH {
            return 0;
        }

        // Get the seed from the message and generate the key
        let seed: [u8; 32] = probe[1..33].try_into().unwrap();

        let masking_scalar = Scalar::from_bytes(&self.random_data)
            .unwrap_or(Scalar::one());

        let key = PrivateKey::from_bytes(&seed)
            // TODO: Use default instead?
            .unwrap_or(PrivateKey::one());

        // Generate the signature
        let tick = cortex_m::peripheral::DWT::get_cycle_count();
        let maybe_sig = key.try_sign_deterministic(&probe[33..length as usize - 33], Some(&masking_scalar));
        let tock = cortex_m::peripheral::DWT::get_cycle_count();

        // Check if signature generation was succesfull. Discard invalid signatures
        let time = match maybe_sig {
            Ok(_) => tock - tick,
            Err(_) => 0,
        };

        // Update the random data for the next run
        let mut tmp: [u8; 31] = [0x00; 31];
        tmp.copy_from_slice(&self.random_data[0..31]);
        self.random_data[1..32].copy_from_slice(&tmp);
        self.random_data[0] = (tock - tick) as u8;

        // Return the instruction difference
        probe[0] = 0x00;
        probe[1..5].copy_from_slice(&time.to_le_bytes());
        5
    }
}
